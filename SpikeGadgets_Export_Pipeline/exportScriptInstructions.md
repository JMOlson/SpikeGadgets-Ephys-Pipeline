Open Plexon
Open a cut .plx file of one tetrode

%% We will first export unit ID and spike times 

Open exportSpikeTimes.ofb
Change Dir to where you final cut .plx files are

File > Export per-waveform data 

Format: Text
Delimiter: Comma
All Channels
All Units
Chosen: Unit, Timestamp
Write to: One file

Export!

Check it looks good with two columns of numbers (unit ID (int), spike times (floats))
Then delete the file. We just did this to make the settings

File > Export Batch Command File
Run exportSpikeTimes

Check the first TT as script runs to ensure still good

Takes a few minutes to run through all tetrodes

This should go in the *.SpikesFinal folder. Make sure all your tetrodes are there and accurate.

%% Now we export unit stats

Open exportWFStats.ofb
Change Dir to where you final cut .plx files are


File > Export per-unit stats
Format: Text
Delimiter: Comma
Check Export a Row of Column Headers
All Channels

Chosen: 
Channel, 
Unit Number,
Number of Waveforms,
Peak-Valley of Unit Template,
Valley FWHM of Unit Template,
Peak to Valley Ticks of Unit Template,
Valley to Peak Ticks of Unit Template

Check append average waveform

Write to: One file

Export!
check that there are headers. Should have a row per sorted unit
Delete the file

File > Export Batch 
Run exportWFStats.ofb

Check the first TT as script runs to ensure still good

Takes a few minutes to run through all tetrodes

This should go in the *.ExportedUnitStatsfolder. Make sure all your tetrodes are there and accurate.





