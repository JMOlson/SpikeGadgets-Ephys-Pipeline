function exportTrodesData(dataTypeToExport, recFileString, outputDirectory, outputName, varargin)

%

%% Additional input parameters may be passed by name/value pairs or as a struct with field names
% and values.

%% Settings that can't be set from here, and must be correct in your config file:
% Reference trode and specific wire for referencing. (Same wire is 
% selected for spike/lfp/raw, btw).
% Type of referencing, if you want common average referencing I guess. Not
% totally sure CAR works on the export. EDIT - CAR does not work Trodes
% 2.0, still true as of 2.3.2

% Add necessary libraries to paths.
[origPath, origLibPath] = addTrodesToPaths('','');

%% ToDo: add exportofflinesorter


%% Parse Inputs - ToDo: Set up validation
% Default parameter settings are consistent with Jadhav lab consensus -
% August 2022.
p = inputParser;
p.StructExpand = true;
validStr = @(x) isstring(x) || ischar(x);
addRequired(p,'dataTypeToExport',validStr);
addRequired(p,'recFileString',validStr);
addRequired(p,'outputDirectory',validStr);
addRequired(p,'outputName',validStr);

addParameter(p,'reconfig','',validStr);
addParameter(p,'interpDist',3000); % SpikeGadgets default:-1  packets to interp over. -1=infinite
addParameter(p,'sortingMode',1); % SpikeGadgets default:1   0 - all wires together; 1 - by ntrode; 2 - by sorting group
addParameter(p,'abortBadData',false); % not sure of SpikeGadgets default
addParameter(p,'paddingBytes',[]); % not sure of SpikeGadgets default, or why you would use.

% LFP data band parameters
addParameter(p,'lfpLowPass',400);
addParameter(p,'lfpOutputRate',1500); % Spikegadgets default 1500
addParameter(p,'useLfpRefs',false);
addParameter(p,'useLfpFilters',true);

% spike data band parameters
addParameter(p,'spikeHighPass',600);
addParameter(p,'spikeLowPass',6000);
addParameter(p,'invert',false);
addParameter(p,'useSpikeRefs',true);
addParameter(p,'useSpikeFilters',true);

% raw data band parameters
addParameter(p,'useRawRefs',true); % Mountainsort export uses this - I want 

% spike event parameters
addParameter(p,'thresh',60);

parse(p,dataTypeToExport, recFileString, outputDirectory, outputName, varargin{:});

% Simplify var naming once inputs are processed.
dataTypeToExport = p.Results.dataTypeToExport;
recFileString = p.Results.recFileString;
outputDir = p.Results.outputDirectory;
outputName = p.Results.outputName;

configFile = p.Results.reconfig;
sortMode = p.Results.sortingMode;
abortBadData = p.Results.abortBadData;
interpDist = p.Results.interpDist;
paddingBytes = p.Results.paddingBytes;

lfpLowPass = p.Results.lfpLowPass;
lfpOutputRate = p.Results.lfpOutputRate;
useLfpRefs = p.Results.useLfpRefs;
useLfpFilters = p.Results.useLfpFilters;

spikeHighPass = p.Results.spikeHighPass;
spikeLowPass = p.Results.spikeLowPass;
invert = p.Results.invert;
useSpikeRefs = p.Results.useSpikeRefs;
useSpikeFilters = p.Results.useSpikeFilters;

useRawRefs = p.Results.useRawRefs;
threshMV = p.Results.thresh;


%% Trodes folder has to be on the path.
%% Find trodes exportFn
trodesPath = which('trodes_path_placeholder.m');
trodesPath = fileparts(trodesPath);
exportFn = fullfile(trodesPath, 'trodesexport');

%% Choose correct settings for the export type and build the settings string.
switch dataTypeToExport
    case 'spikes'
        processingFlag = '-spikes';
        disp('No default settings set yet for the spike export.');
        spikeSettings = {'-thresh', threshMV}; % -thresh <int>
        specificSettings = buildSettings(spikeSettings);
    case 'lfp'
        processingFlag = '-lfp';
        lfpSettings = {'-lfplowpass', lfpLowPass, '-lfpoutputrate', lfpOutputRate,...
            '-uselfprefs', useLfpRefs, '-uselfpfilters', useLfpFilters};
        % Use lfpchan=# in your workspace file to set the correct lfp channel for each tetrode.
        specificSettings = buildSettings(lfpSettings);
    case 'spikeband'
        processingFlag = '-spikeband';
        spikebandSettings = {'-spikehighpass', spikeHighPass, '-spikelowpass', spikeLowPass,...
            '-invert', invert, '-usespikerefs', useSpikeRefs, '-usespikefilters', useSpikeFilters};
        specificSettings = buildSettings(spikebandSettings);
    case 'raw'
        processingFlag = '-raw';
        rawSettings = {'-userawrefs', useRawRefs}; %  <T/F>
        specificSettings = buildSettings(rawSettings);
    case 'stim'
        processingFlag = '-stim';
        disp('No default settings set yet for the spike export.');
        stimSettings = ' ';
        specificSettings = buildSettings(stimSettings);
    case 'dio'
        processingFlag = '-dio';
        specificSettings = ' ';
    case 'aio'
        processingFlag = '-analogio';
        specificSettings = ' ';
    case 'mountainsort'
        processingFlag = '-mountainsort';
        mtnSrtSettings = {'-sortingmode ', sortMode, '-useRawRefs', useRawRefs};
        %mtnSrtSettings = {'-useRawRefs', useRawRefs};
        specificSettings = buildSettings(mtnSrtSettings);
    case 'kilosort'
        processingFlag = '-kilosort';
        kiloSrtSettings = {'-sortingmode ', sortMode, '-useRawRefs', useRawRefs};
        specificSettings = buildSettings(kiloSrtSettings);
    case 'time'
        processingFlag = '';
        specificSettings = ' ';
        interpDist = 500;
        exportFn = fullfile(trodesPath, 'exporttime');
    otherwise
        disp('I did not understand your data type to export. Exiting.');
        return;
end

%% Build settings string used for all export types.
standardSettings = [recFileString, buildSettings({'-outputdirectory', outputDir,...
    '-output', outputName, '-abortbaddata', abortBadData,...
    '-interp', interpDist, '-reconfig', configFile, '-paddingbytes', paddingBytes})];

%% Send export command to run in system command shell.
command = strjoin({exportFn, processingFlag, standardSettings, specificSettings});
disp(['Running: ', command]);
system(command);
% eval(['! ', command]);

% Reset paths to prior state.
addTrodesToPaths(origPath, origLibPath);
end

function settingsString = buildSettings(potentialSettings)
%% Build settings string, ignoring empty values, so that defaults are used.
settingsString = '';
nSettings = numel(potentialSettings)/2;
for iSetting = 1:nSettings
    settingName = potentialSettings{iSetting*2-1};
    settingVal = potentialSettings{iSetting*2};
    if ~isempty(settingVal)
        if islogical(settingVal) || isnumeric(settingVal)
            settingVal = num2str(settingVal);
        end
        if isempty(settingsString)
            settingsString = strjoin({' ', settingName, settingVal});
        else
            settingsString = strjoin({' ', settingsString, settingName, settingVal});
        end
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF CODE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
