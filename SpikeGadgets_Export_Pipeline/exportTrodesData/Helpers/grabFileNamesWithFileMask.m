function recFileString = grabFileNamesWithFileMask(fileNameMask, extension, folderToSearch)
%%
% Adpated this from export****BinaryFiles in the Trodes TrodesToMatlab code folder to be recursive.
% Uses a fileMask to recursively grab all files that start with that name and end with a given file type.
% Then sorts by write date, and exports names as a string.

recFileString = [];
recFileTimes = [];

startDir = pwd;
cd(folderToSearch);

if ispc % Windows
    [~,recFileList] = system(['where /r . ', fileNameMask,'*',extension]);
else % Unix
    [~, recFileList] = system(['find . -name ', fileNameMask,'*',extension]);
end

RecFiles = strsplit(strtrim(recFileList));
RecFiles = RecFiles(cellfun(@(x) ~isempty(x),RecFiles));
RecFiles = cellfun(@(x) [folderToSearch,x(2:end)],RecFiles,'UniformOutput',false);
for iRec = 1:length(RecFiles)
    recName = RecFiles{iRec};
    if ispc % Windows
        [~,rawDateOut] = system(['for %a in (', recName, ') do set FileDate=%~ta']);
        rawDateOut = strsplit(strtrim(rawDateOut),'=');
        recFileTimes(iRec) = datenum(datetime(rawDateOut{2},'InputFormat','MM/dd/yyyy hh:mm a'));
    else % Unix
        [~, timeStr] = system(['date -r', recName, ' +%s']);
        recFileTimes(iRec) = str2double(timeStr);
    end  
    
end

[~,fileIndSorted] = sort(recFileTimes);
for i=1:length(fileIndSorted)
    recFileString = [recFileString ' -rec ' RecFiles{fileIndSorted(i)}];
end

cd(startDir);
end
