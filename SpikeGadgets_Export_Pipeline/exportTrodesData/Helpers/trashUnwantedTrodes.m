function trashUnwantedTrodes(trodeList,folderPrefix)
% helper for the preprocessing script. pretty hardcoded. beware.

lfpFolder = [folderPrefix,'.LFP'];
trodeFolder = [folderPrefix,'.mountainsort'];

for iTrode = 1:numel(trodeList)
    % trash extra LFP
    lfpToDel = dir([lfpFolder,filesep, '*nt',num2str(trodeList(iTrode)),'ch*']);
    if numel(lfpToDel) == 1
        disp(['deleting: ',lfpToDel.name]);
        delete([lfpToDel.folder,filesep,lfpToDel.name]);
    else
        disp('oops found multiple - or none');
    end

    % trash extra trode .mda
    trodeToDel = dir([trodeFolder,filesep, '*.nt',num2str(trodeList(iTrode)),'.mda']);
    if numel(trodeToDel) == 1
        disp(['deleting: ',trodeToDel.name]);
        delete([trodeToDel.folder,filesep,trodeToDel.name]);
    else
        disp('oops found multiple - or none');
    end
    % trash extra trode .dat
    trodeToDel = dir([trodeFolder,filesep, '*.nt',num2str(trodeList(iTrode)),'.*.dat']);
    if numel(trodeToDel) == 1
        disp(['deleting: ',trodeToDel.name]);
        delete([trodeToDel.folder,filesep,trodeToDel.name]);
    else
        disp('oops found multiple - or none');
    end
end

end