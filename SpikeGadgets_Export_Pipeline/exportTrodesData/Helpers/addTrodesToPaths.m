function [origPath, origLibPath] = addTrodesToPaths(trodesPath,libPath)

%% Not tested on windows yet

DEFAULT_WINDOWS_PATH = "B:\Trodes_2-3-2_Windows64";
DEFAULT_WINDOWS_QT_LIB = 'B:\Trodes_2-3-2_Windows64\QT\lib';

%DEFAULT_LINUX_PATH = "/home/jmolson/Desktop/Trodes_2-1-1_Ubuntu1804";
DEFAULT_LINUX_PATH = "/opt/Trodes_2-3-2_Ubuntu2004";
%DEFAULT_LINUX_QT_LIB = '/home/jmolson/Desktop/Trodes_2-1-1_Ubuntu1804/QT/lib';
DEFAULT_LINUX_QT_LIB = '/opt/Trodes_2-3-2_Ubuntu2004/QT/lib';


if isempty(trodesPath)
    % Defaults!
    if ispc
        origPath = addpath(DEFAULT_WINDOWS_PATH);
    else
        disp(['Using export tools from: ', DEFAULT_LINUX_PATH']);
        disp('Mismatched recording and export versions may not successfully export');
        origPath = addpath(DEFAULT_LINUX_PATH);
    end
else
    origPath = addpath(trodesPath);
end
if isempty(libPath)
    % Defaults!
    if ispc
        origLibPath = getenv("LD_LIBRARY_PATH");
        setenv("LD_LIBRARY_PATH", DEFAULT_WINDOWS_QT_LIB);
    else
        origLibPath = getenv("LD_LIBRARY_PATH");
        setenv("LD_LIBRARY_PATH", DEFAULT_LINUX_QT_LIB);
    end
else
    origLibPath = getenv("LD_LIBRARY_PATH");
    setenv("LD_LIBRARY_PATH", libPath);
end

end