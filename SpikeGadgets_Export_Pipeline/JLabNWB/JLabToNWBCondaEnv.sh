ENVNAME=JLabToNWB
conda create --name $ENVNAME python=3.8
conda activate $ENVNAME
pip install jupyter matplotlib ndx-pose pynapple

# NWBHandler needs:
pip install pynwb lxml xmldiff
conda develop /mnt/4TBData/Code_Repos/jlab-settings
conda develop /mnt/4TBData/Code_Repos/JLab-Analysis-Suite/Analysis
conda develop /mnt/4TBData/Code_Repos/JLab-Analysis-Suite/SpikeGadgets_Export_Pipeline/JLabNWB

## Previous instructions that may be of use:
# gedit ~/conda/envs/jlabToNwb/lib/python3.8/site-packages/conda.pth
# ```
#
# Can also just use conda develop /PATH/TO/YOUR/MODULE
#
# Add add the path to your pynapple and trodes reader files to that file
# and save, a la:
# /mnt/4TBData/Code Repos/trodes/TrodesToPython
# /mnt/4TBData/Code Repos/pynapple

# Now add rec_to_nwb to the environment, using:
# '''
# pip install -e YOUR/PATH/TO/RECTONWB
# pip install ndx_franklab_novela
# pip install mountainlab_pytools
# pip install xmldiff
# pip install xmlschema
# '''
# a la: 
# pip install -e "/mnt/4TBData/Code Repos/rec_to_nwb"

