#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Converting DIOs to behavioral events

License:
BSD-3-Clause
"""

# Built-in/Generic Imports
# import itertools
# import operator
# import time
import re
import warnings

# Libs
import numpy as np
import pandas as pd
# Own modules
# from StateScriptRecData import StateScriptRec as SSRec
import pynapple as nap
from pynapple import Tsd, TsGroup

# import matplotlib.pyplot as plt


class DIO(Tsd):
    '''
    Class for single DIO from a JLab NWB file
    DIO is a binary time series (Pynapple Tsd object)
    '''

    def __init__(self, *args, **kwargs):
        '''
        Initialize DIO object
        '''
        if 'd' in kwargs and kwargs['d'].dtype == 'uint8':
            kwargs['d'] = kwargs['d'].astype(np.int16)

        super().__init__(*args, **kwargs)
        # Test to make sure it is a binary time series
        if self is not None:
            self.test_binary()

    # Helpers
    def test_binary(self):
        '''
        Test to make sure it is a binary time series.
        Should be data with times where it changes from 0 to 1 and back.
        '''
        # Session max is a proxy for max sessions in a day.
        # We assume no one will do more than this.
        # Session edges can cause non_flips that would be okay.
        # If there are more  non_flips than this, we throw
        # an error on import assuming it isnt a DIO.
        SESSION_MAX = 20
        if np.any(self.data()):
            if len(np.unique(self.data())) > 2:
                raise ValueError('Time series must be binary')
            # check that values always change by 1
            data = self.data()
            non_flips = np.where(np.abs(np.diff(data)) != 1)[0]
            if (np.any(np.abs(np.diff(data)) < 0) or
                    np.any(np.abs(np.diff(data)) > 1)):
                raise ValueError(
                    'Time series doesn\'t oscillate between two values.')
            if len(non_flips) > SESSION_MAX:
                raise ValueError(
                    'Time series has %i consecutive equal values. '
                    + 'DIOs should not.', len(non_flips))
            elif len(non_flips) > 0:
                warnings.warn((
                    '{0} consecutive values found. Okay at session borders. '
                    + 'Otherwise indicates a problem').format(len(non_flips)))
            return

    @ staticmethod
    def make_DIO(tsd):
        '''
        Make a DIO object from a tsd
        I don't know if you should do this this way, but it works
        '''
        tsd.__class__ = DIO
        tsd.test_binary()
        return tsd

    def create_events_df(self, start_state=1):
        '''
        Create a pandas dataframe of events from a binary time series
        Want start, stop, and duration of each event

        Parameters
        ----------
        start_state : int
            Value of binary_tsd that indicates the start of an event
        '''
        data = self.data()
        times = self.times()
        # Repeats are where a change is repeated - which should not be possible.
        # Excessive repeats cause an error during creation.
        # Here, if it occurs, we assume it is due an edge case between two
        # sessions and remove the repeat, either before or after, depending
        # if it a start (remove 1st) or stop (remove 2nd) repeat.
        repeats = np.where(np.abs(np.diff(data)) != 1)[0]
        if len(repeats):
            for i_rep in repeats:
                if data[i_rep] == start_state:
                    data = np.delete(data, i_rep)
                    times = np.delete(times, i_rep)
                else:
                    data = np.delete(data, i_rep+1)
                    times = np.delete(times, i_rep+1)
        event_starts = times[np.where(data == start_state)]
        event_stops = times[np.where(data != start_state)]

        # Drop first event if it is a stop event
        if event_stops[0] < event_starts[0]:
            event_stops = event_stops[1:]
        if event_stops[-1] < event_starts[-1]:
            event_starts = event_starts[:-1]
        event_duration = event_stops - event_starts

        # create pandas dataframe
        return pd.DataFrame({'start_time': event_starts, 'stop_time': event_stops,
                             'duration': event_duration})

    def get_events(self, start_state=1):
        '''
        Return TimeSeries of start times
        '''
        return self[np.where(self == start_state)]


class DIOs(TsGroup):
    '''
    TsGroup object (Pynapple) of DIOs from a JLab NWB file
    TsGroup is essentially a dict, but also has _metadata and other features.
    '''

    def __init__(self, *args, **kwargs):
        '''
        Initialize DIO object
        '''
        super().__init__(*args, **kwargs)
        return

    def add_DIO_types(self, dio_metadata):
        '''
        Read id from exp metadata, create type and type index columns, add to
        dio metadata
        '''
        type_dict = {this_dio['id']: this_dio['description']
                     for this_dio in dio_metadata['behavioral_events']}
        # Replace of Nones (if ppl left blanks in settings) with empty strings
        type_dict = {key: val for key, val in type_dict.items() if val}
        type_regexp_str = '|'.join(type_dict.keys())
        type_re = r'(?P<type>{})'.format(type_regexp_str)
        # get DIO ids - should be equivalent to type_dict.keys(), but we
        # dont require that they are the same - the user could have defined
        # more or fewer in the metadata than are in the DIOs
        dio_ids = self.get_ids()

        type = [re.sub(r"\d+", "", iDIO) for iDIO in dio_ids]
        # strip all but numbers from string
        type_index = [re.search(r"\d+", iDIO).group()
                      for iDIO in dio_ids]
        if type_re is None:  # no DIOs in metadata
            self.set_info(**{'io_type': np.array(type),
                             'io_type_index': np.array(type_index),
                             'use_type': np.repeat('', len(dio_ids)),
                             'use_type_id': np.repeat('', len(dio_ids))})
        else:
            # match DIO ids to metadata
            dio_types = [re.compile(type_re).fullmatch(iDIO)
                         for iDIO in dio_ids]
            # Parse type and type index from DIO ids using regular expressions
            desc_types = [type_dict[iDIO['type']]
                          if iDIO is not None else '' for iDIO in dio_types]
            use_id = [re.search(r"\S+$", i_type).group()
                      if i_type else '' for i_type in desc_types]
            desc_types = [re.sub(r"\s\S+$", "", i_type)
                          for i_type in desc_types]
            desc_types = ['Not Labelled' if i_type ==
                          '' else i_type for i_type in desc_types]
            self.set_info(**{'io_type': np.array(type),
                             'io_type_index': np.array(type_index),
                             'use_type': np.array(desc_types),
                             'use_type_id': np.array(use_id)})

        return

    def get_ids(self):
        '''
        Get the name of each DIO, returns a list
        '''
        return [self._metadata['id'][iDIO]
                for indDIO, iDIO in enumerate(self)]

    # Helpers
    @ staticmethod
    def make_DIOs(tsg):
        '''
        Make a DIOs object from a tsGroup of DIOs
        I don't know if you should do this this way, but it works
        '''
        tsg.__class__ = DIOs
        return tsg

    def restrict(self, *args, **kwargs):
        '''
        Restrict DIOs to a time span
        '''
        return DIOs.make_DIOs(super().restrict(*args, **kwargs))

    def __getitem__(self, key):
        '''
        Trying to deal with keeping my type for the inherited methods.
        Again, I don't know if you should do this this way, but it works
        Appears to not be necessary anymore for tsd?... hmmm...
        '''
        item = super().__getitem__(key)
        # if isinstance(item, Tsd) and not isinstance(item, DIO):
        #     return DIO.make_DIO(item)
        if isinstance(item, TsGroup) and not isinstance(item, DIOs):
            return DIOs.make_DIOs(item)
        # else:
        return item

    @ staticmethod
    def remove_empty_DIOs(dios_tsg):
        '''
        Remove empty DIOs
        TsGroup is immutable, so this returns a new DIOs object.
        '''
        has_events = [iCount > 0 for iCount in dios_tsg.count(
            ep=dios_tsg.time_support.time_span()).data()[0]]

        new_dio_dict = dict((pair[0], pair[1]) for pair, has_event
                            in zip(dios_tsg.items(), has_events)
                            if has_event)

        metadata = {col: dios_tsg.get_info(col)[has_events]
                    for col in dios_tsg.metadata_columns}
        # Don't need "rate" column - it is calculated from the data
        metadata.pop('rate')
        return DIOs(data=new_dio_dict, **metadata)

    def get_events(self, start_state=1):
        '''
        Return TimeSeries of start times for all events combined. The data
        is the type_index from the metadata.
        '''
        # if type_index repeats for any DIO, raise an error
        if (len(self._metadata['io_type_index']) !=
                len(np.unique(self._metadata['io_type_index']))):
            raise ValueError('type_index must be unique for each DIO. If not, '
                             + 'multiple DIOs will be assigned the same '
                             + 'type_index.')
        event_dict = {iDIO: self[iDIO].get_events(
            start_state) for iDIO in self}
        events = nap.TsGroup(event_dict, time_support=self.time_support,
                             **{key: self.get_info(key)
                                for key in self._metadata.keys()
                                if key != 'rate'})
        return events.to_tsd('io_type_index')

    def create_events_df(self, start_state=1):
        '''
        Create a dataframe of events from DIOs

        Parameters
        ----------
        start_state : int
            Value of binary_tsd that indicates the start of an event
        '''
        events_dict = {self.get_info('use_type_id').iloc[indDIO]:
                       self[iDIO].create_events_df(start_state)
                       for indDIO, iDIO in enumerate(self)}
        a_key = next(iter(events_dict))
        types = self.get_info('use_type')
        # Add type and type_index to dataframe
        [events_dict[iDIO].insert(0, 'use_type', types.iloc[indDIO])
            for indDIO, iDIO in enumerate(events_dict)]

        # add index names to dataframe
        columns = events_dict[a_key].columns
        events_df = pd.concat(events_dict, names=['use_type_id'])
        events_df.columns = columns
        events_df.sort_values(by='start_time', inplace=True)
        return events_df.reset_index(level='use_type_id')

    # def get_events(self, start_state=1):
    #     '''
    #     Get events from DIOs
    #     Parameters
    #     ----------
    #     dios_tsg : Pynapple TimeSeriesGroup Object
    #         Time series of binary values
    #     start_state : int
    #         Value of binary_tsd that indicates the start of an event
    #     '''
    #     dios_events = {indDIO: DIO.get_events_to_df(self[iDIO], start_state)
    #                 if self._metadata['rate'][iDIO] != 0 else None
    #                 for indDIO, iDIO in enumerate(self)}
    #     return dios_events
