import write_jlab_nwb_settings.import_settings as jlab_to_nwb_settings
from jlab_nwb import JLabNWBHandler



# Settings are determined by import_settings.py's IMPORT_SETTINGS_FILE
dayRec = JLabNWBHandler(all_paths=jlab_to_nwb_settings.get_paths(),
                        settings=jlab_to_nwb_settings.get_settings())

# A couple examples of alternate ways to process your data.
# See the init method of JLabNWBHandler for more details.

# PATH_FILE = '/mnt/4TBData/Code_Repos/jlab-settings/example_export_path_file.yml'

# NWB_FILE = 'Day34.nwb'
# NWB_FILE = 'SL18_D19.nwb'

# Linux paths
# NWB_PATH = '/mnt/Fast4TB/Processing Prototyping'
# data_dir = '/mnt/16TBData/Data/SubLearning/SL18/SL18_D19'

# Windows paths
# NWB_PATH = r'G:\bp_inference\Transitive\data\TH405\inferenceTraining\Day34\NWB'
# data_dir = r'G:\bp_inference\Transitive\data\TH405\inferenceTraining\Day34'


# dayRec = JLabNWBHandler(all_paths=PATH_FILE, filename=NWB_FILE,
#                         nwb_path=NWB_PATH, data_path=data_dir)
# dayRec = JLabNWBHandler(all_paths=PATH_FILE,
#                         nwb_path=NWB_PATH)



# """
# This script writes NWB files for both the subject and the partner in the social W task.

# The NWB files contain various data types, including:
#     - Spikes (neural spiking activity)
#     - DIOs (digital input/output events)
#     - LFPs (local field potentials)
#     - DLC tracking (tracking data from DeepLabCut)
#     - Metadata (contextual information about the experiment)

# The purpose of this script is to analyze cooperative behavior in the social W task by organizing and storing the data 
# from the subject and partner into structured NWB files for further analysis.

# Parameters:
#     subject_id (str): The identifier for the subject.
#     partner_id (str): The identifier for the partner.
#     day (int): The day of the experiment being analyzed.
    
# The script uses the JLabNWBHandler to handle data extraction, processing, and writing to the NWB format.
# """

# # Define subject, partner, and day as variables

# subject_id = "FXM110"
# partner_id = "FXM101"
# day = 16

# # Get paths for the subject
# subject_paths = jlab_to_nwb_settings.get_paths()
# subject_paths['NWB_name'] = f'{subject_id}_day{day}'  # Use formatted string for NWB name
# subject_paths['DLC_path'] = f'G:\\Edward\\{subject_id}\\{subject_id}_day{day}\\'  # Format the DLC path dynamically

# # Copy paths for the partner and adjust accordingly
# partner_paths = subject_paths.copy()
# partner_paths['NWB_name'] = f'{partner_id}_day{day}'  # Use formatted string for partner's NWB name
# partner_paths['DLC_path'] = f'G:\\Edward\\{subject_id}\\{subject_id}_day{day}\\{partner_id}_day{day}'  # Use formatted path for partner

# # Get settings for the subject
# subject_settings = jlab_to_nwb_settings.get_settings()
# subject_settings['ignore_data_types'] = ['']  # Set ignored data types for subject

# # Copy settings for the partner and adjust accordingly
# partner_settings = subject_settings.copy()
# partner_settings['DLC_search_term'] = partner_id  # Search term based on partner ID
# partner_settings['ignore_data_types'] = ['ephys']  # Ignore ephys data for the partner

# # Create handlers for subject and partner to write NWB files
# subject_dayRec = JLabNWBHandler(all_paths=subject_paths, settings=subject_settings)

# partner_dayRec = JLabNWBHandler(all_paths=partner_paths, settings=partner_settings)
