#! /usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
import pytz
import itertools
import os
import re
import warnings
from pathlib import Path

import numpy as np
import pandas as pd
import pynapple as nap
import TrodesToPython.readCameraModuleTimeStamps as trodesCamReader
import TrodesToPython.readTrodesExtractedDataFile3 as trodesReader
import yaml
from dios import DIO, DIOs
from lxml import etree as ET
from ndx_pose import PoseEstimation, PoseEstimationSeries, Skeleton
from pynwb import NWBHDF5IO, NWBFile, TimeSeries
from pynwb.behavior import BehavioralEvents, BehavioralTimeSeries
from pynwb.ecephys import LFP, ElectricalSeries
from pynwb.file import Subject
from pynwb.image import ImageSeries

# from xmldiff import formatting, main

TRIM_LIMIT = 15  #  frames. typically 15 frames is 0.5 seconds

class JLabNWBHandler:
    """
    Class for handling Jadhav Lab NWB files. This class is used to write or
    read from a NWB file built with this object. This object is used to write
    data from the Jadhav Lab data format, which is based on the Trodes
    recording software, into NWB.
    """

    def __init__(self, all_paths=None, nwb_name=None, nwb_path=None,
                 data_basename=None, data_path=None, settings=None):
        """
        Initialize the JadhavLabNWB object. Read the NWB file if it exists.
        Write the NWB file if it does not exist.

        Parameters
        ----------
        all_paths : str  (optional)
            Path to the yaml file that contains all the paths needed for the
            export. Paths specified by the other input arguments will
            override the paths in this file.
                Paths:
                NWB_name : str (optional) - name of the nwb file
                NWB_path : str (optional)
                data_path : str (optional)
                video_path : str (optional, required for video files)
                    # Video files folder - searches recursively, so can be a
                    # parent folder of the data folder.
                DLC_path : str (optional, required for DLC tracking files)
                    # DeepLabCut tracking data folder - searches recursively,
                    # so can be a parent folder of the data folder.
        nwb_name : str  (optional)
            Name of the nwb file. Can be a full path, name with extension, or
            just the name. If path is not given here or with the path argument,
            the path will be the current working directory. If nwb_name is not
            given here, the nwb_name will default to the data_path.
        nwb_path : str  (optional)
            Path to the nwb file. If not given here or as part of the nwb_name,
            the path will be the current working directory. It is assumed that
            the data are stored in the same folder as the nwb file. This
            argument overrides the path given in the nwb_name argument.
        data_basename : str  (optional)
            Base name of the data files. If not given here, the base name will
            default to the nwb_name. This is used to find the data files.
        data_path : str  (optional)
            Path to the pre-nwb data. If not given here, the path will be the
            nwb_path. It is assumed that the data are stored in the same folder
            as the nwb file.
        settings : str or dictionary (optional)
            Path to the settings file. If not given here, the file named
            'jlab_default_export_settings.yml' in the examples folder a level
            below this code will be used. This file contains the settings for
            the export.
        """

        # Import constants and settings.
        # code_dir = os.path.dirname(os.path.realpath(__file__))
        # all_paths
        if all_paths is None:
            self.paths = dict()
        elif isinstance(all_paths, dict):
            self.paths = all_paths
        else:
            with open(all_paths, 'r') as stream:
                self.paths = yaml.safe_load(stream)

        # Input argument handling.
        # nwb path
        if nwb_path is not None:
            self._nwb_path = nwb_path
        # check if key exists paths
        elif 'NWB_path' in self.paths:
            self._nwb_path = self.paths['NWB_path']
        elif nwb_name is not None:
            if os.path.dirname(nwb_name):
                self._nwb_path = os.path.dirname(nwb_name)
            else:
                self._nwb_path = os.getcwd()
        else:
            self._nwb_path = os.getcwd()
        # data path
        if data_path is not None:
            self._data_path = data_path
        elif 'data_path' in self.paths:
            self._data_path = self.paths['data_path']
        else:
            self._data_path = self._nwb_path

        # nwb_name
        if nwb_name is not None:
            self._nwb_basename = os.path.basename(nwb_name)
        elif 'NWB_name' in self.paths:
            self._nwb_basename = self.paths['NWB_name']
        else:
            self._nwb_basename = os.path.basename(self._data_path)

        # Add .nwb extension if not present.
        if not os.path.splitext(self._nwb_basename)[1]:
            self._nwb_basename = self._nwb_basename + '.nwb'
        # Set the full path and basename of the nwb file.
        self._nwb_basename_stem = os.path.splitext(self._nwb_basename)[0]
        self._nwb_filename = os.path.join(self._nwb_path, self._nwb_basename)

        # data_base_name
        if data_basename is not None:
            self._data_basename = data_basename
        elif 'data_basename' in self.paths:
            self._data_basename = self.paths['data_basename']
        else:
            self._data_basename = self._nwb_basename_stem

        # Check if folder and nwb file exists and is a nwb file
        self._nwb_exists = False
        if not os.path.exists(self._nwb_path):
            print('Folder {} does not exist. Creating folder.'.format(
                self._nwb_path))
            os.makedirs(self._nwb_path)
        # Settings are necessary for write: this is only ok for reading the NWB
        if settings is None and os.path.isfile(self._nwb_filename):
            # NWB found, continue on.
            self._nwb_exists = True
        elif settings is None:
            raise ValueError('No nwb found and no settings file given.' +
                             ' Cannot write NWB.')
            # import settings
        elif isinstance(settings, dict):
            self.settings = settings
        else:
            self.import_settings(settings)
        # Add nested folder of nwb basename stem to path if nest_nwb is True
        if (hasattr(self, 'settings') and 'nest_nwb' in self.settings
                and self.settings['nest_nwb']):
            self._nwb_path = os.path.join(self._nwb_path,
                                          self._nwb_basename_stem)
            self._nwb_filename = os.path.join(self._nwb_path,
                                              self._nwb_basename)
            if not os.path.exists(self._nwb_path):
                print('Folder {} does not exist. Creating folder.'.format(
                    self._nwb_path))
                os.makedirs(self._nwb_path)
            # If case for an nwb read with a nested path
            if os.path.isfile(self._nwb_filename):
                self._nwb_exists = True
        # It's a file, but not an nwb
        if not self._nwb_filename.endswith('.nwb'):
            raise RuntimeError(
                'File {} is not a nwb file.'.format(self._nwb_filename))

        # Load the nwb file if it exists. If not, write the nwb file.
        if self._nwb_exists:
            try:
                self.read_nwb()
                success = True
            except Exception as e:
                raise e
            if success:
                self._nwb_exists = True
                # Print out that the file exists and what is the name.
                print('NWB file already exists: {}'.format(self._nwb_filename))
        else:
            # Print out that the file does not exist and what is the name.
            print('NWB file {} not found. Creating.'.format(
                self._nwb_filename))
            if not os.path.exists(self._data_path):
                raise RuntimeError('Data folders {} does not exist. No \
                                   existing valid data folder.'.format(
                    self._data_path))
            self.import_data_for_nwb()
            self._nwb_exists = self.write_data_to_nwb()
            print('NWB file created: {}'.format(self._nwb_filename))

    # Methods for loading JLab raw data to JLabNWB - step before writing NWB
    def import_settings(self, settings_file):
        with open(settings_file, 'r') as stream:
            self.settings = yaml.safe_load(stream)
        return

    def import_data_for_nwb(self):
        """
        This loads the raw data

        Parameters
        ----------
        path : str
            Path to the session

        We have the following data streams to get from our recordings
        into the NWB:
        - Experimental Metadata (Hand entered, or markdown?)
        - Time Epochs (Start/stop of each recording)
        - Tracking Data (Either DeepLabCut or Trodes tracker)
        - Behavioral Data (DIO & ECU log)
        - Sorted Spikes (Spiketimes, Unit IDs, Metadata on the process (which
            is extract, mountainsort, offline sorter, and all parameters used),
            Waveforms?)
        - LFPs (Downsampled broadband, and associated metadata)

        Main loader loads tracking, DIOs, and time epochs for storage into NWB.
        Needed:
            - LFPs
            - Spikes (sorted)
            - DIOs
            - ECU Log (Statescript Logs)
            - Any User MetaData
            - Video Files
            - MountainSort Settings
        """
        # Check if settings dict has a ignore_data_types key
        if 'ignore_data_types' not in self.settings:
            self.settings['ignore_data_types'] = []
        self.import_user_metadata()
        self.import_timestamp_offsets()
        self.import_epochs()
        if ('LFPs' not in self.settings['ignore_data_types'] and
            'ephys' not in self.settings['ignore_data_types']):
            self.import_LFPs()
        if 'DIOs' not in self.settings['ignore_data_types']:
            self.import_DIOs()
        if ('spikes' not in self.settings['ignore_data_types'] and
            'ephys' not in self.settings['ignore_data_types']):
            self.import_spikes()
            self.import_msort_settings()
        self.import_videos()
        self.import_dlc_csv()
        self.import_ecu_logs()
        return None

    def import_user_metadata(self):
        '''
        Grab header of first rec in the group as well as the user defined yml
        '''
        endReadFlag = self.settings['recfile_xml_root'].encode()
        # Not a recursive search for yml - only looks in the data folder.
        user_metadata_file = sorted(Path(self._data_path).glob(
            '*' + self._nwb_basename_stem + '*.yml'))
        if not user_metadata_file:
            raise ValueError('No user metadata (yml) file',
                             self._nwb_basename_stem,
                             ' found in data',
                             self._data_path)
        elif len(user_metadata_file) > 1:
            raise ValueError(
                'More than one user metadata (yml) file found in data %s',
                self._data_path)
        else:  # one file found - strip it out of the list
            user_metadata_file = user_metadata_file[0]

        # Stashing whole exp metadata file as a string in data_collection field
        with open(user_metadata_file, 'r') as stream:
            self.metadata = yaml.safe_load(stream)
            self.metadata_all_string = yaml.dump(self.metadata)

        # Find all recs, then grab all headers
        recCount = 0
        headerXML = []
        # Recursive search - needed if recs were multisession)
        recFiles = sorted(Path(self._data_path).rglob(
            '*' + self._data_basename + '*.rec'))

        # recFilesLists = [
        #     [os.path.join(self._data_path, dir, f)
        #      for f in os.listdir(os.path.join(self._data_path, dir))
        #      if f.endswith(self.settings['rec_suffix'])]
        #     for dir in os.listdir(self._data_path)
        #     if os.path.isdir(os.path.join(self._data_path, dir))]
        # # Add recs in the main folder
        # recFilesLists.append(
        #     [os.path.join(self._data_path, f)
        #      for f in os.listdir(self._data_path)
        #      if f.endswith(self.settings['rec_suffix'])])
        # # flatten the list of recFiles
        # recFiles = [recFile for iList in recFilesLists for recFile in iList]
        if not recFiles:
            warnings.warn('No rec files found in data {} or subfolders'.format(self._data_path))
        for recFile in recFiles:
            if recFile:
                with open(recFile, 'rb') as recStream:
                    headerFile = ''
                    for line in recStream:
                        headerFile += line.decode()
                        if line.find(endReadFlag) != -1:
                            break
                headerXML.append(ET.fromstring(headerFile))
                recCount += 1

        if recCount != 0:
            # Compare all headers to validate, get first rec time.
            diffOpt = {"ignored_attrs": self.settings['ignore_attribute_list']}
            # diffResults = [main.diff_trees(xmlPair[0], xmlPair[1],
            #                diff_options=diffOpt)
            #                for xmlPair in list(combinations(headerXML, 2))]
            # TODO: Learn how to make and throw exceptions.
            # if any(diffResults)
            #     throwException = ("things are different in the headers ",
            #                       "that shouldn't be")
            timeStamps = [datetime.datetime.fromtimestamp(int(xml.find(
                './GlobalConfiguration').get('systemTimeAtCreation'))/1000)
                for xml in headerXML]
            self.recTime = min(timeStamps)
            
            self.headerXML = headerXML[timeStamps.index(self.recTime)]
            self.clock_rate = float(self.headerXML.find(
                './HardwareConfiguration').
                get('samplingRate'))
            # differences = main.diff_trees(
            #   headerXML1, headerXML2, diff_options=diffOpt)
            # datetime.datetime.fromtimestamp(headerXML1.find(
            #   './GlobalConfiguration').attrib['systemTimeAtCreation']/1000)
        return

    def import_timestamp_offsets(self):
        if 'timestampoffset_folder' not in self.settings:
            print('No timestamp offset file setting found. If more than one ',
                  'rec file is being imported, check the search term and ',
                  'folder in the settings file.')
            self.timestamp_offsets = None
            return

        timestamps_path = sorted(Path(self._data_path).rglob(
            '*' + self.settings['timestampoffset_folder']))
        if not timestamps_path:
            print('No timestamp offset folder found. If more than one rec ',
                  'file is being imported, check the search term and folder ',
                  'in the settings file.')
            self.timestamp_offsets = None
            return
        elif len(timestamps_path) > 1:
            raise ValueError(
                'Found more than one timestamp offset folder.\n'
                + str(timestamps_path) + '\n'
                + 'Check the data folder and search in the settings file.')
        else:  # one file found - strip it out of the list
            timestamps_path = timestamps_path[0]

        trodes_offset_files = sorted(Path(timestamps_path).rglob(
            '*' + self._data_basename + '*' +
            self.settings['timestampoffset_search_term']))
        if not trodes_offset_files:
            print('No timestamp offset files found. If more than one rec ',
                  'file is being imported, check the search term and folder ',
                  'in the settings file.')
            self.timestamp_offsets = None
            return

        # Get the timestamp offsets
        timestamp_offset = []
        for toFile in trodes_offset_files:
            # Assuming it's one line - one number.
            # I sure think it always is, because its created by spikegadgets
            # export.
            with open(toFile, 'r', encoding='utf-8') as fid:
                timestamp_offset.append(int(fid.read().strip()))
        self.timestamp_offsets = timestamp_offset
        return

    def import_filename_epoch_info(self):
        # This code reads the file names and creates a dataframe with the
        # epoch information.
        video_time_files = sorted([path for path in Path(
            self._data_path).rglob(
                '*' + self._data_basename + '*' +
                self.settings['video_timestamps_search_term'])])
        regex_stripped = (
            r''.join(line.strip() for line in
                     self.settings['trodes_filename_reg_exp'].splitlines()))
        trodes_filename_reg_exp = re.compile(
            regex_stripped.replace(' ', ''))
        if not video_time_files:
            raise ValueError(
                'No video timestamp files found in {}'.format(self._data_path)
                + 'Check the search term in the settings file.')
        columns = (list(re.match(trodes_filename_reg_exp,
                   video_time_files[0].name).groupdict().keys())
                   + ['filename'])
        file_info_lists = []
        for this_time_file in video_time_files:
            this_filename_data_row = (list(re.match(
                trodes_filename_reg_exp,
                this_time_file.name).groupdict().values())
                + [this_time_file.name])
            file_info_lists.append(this_filename_data_row)
        self.filename_info = pd.DataFrame(
            data=file_info_lists, columns=columns)
        self.filename_info['n_columns'] = len(columns)
        self.filename_info['unique_epoch_ID'] = ['_'.join(
            [self.filename_info.at[file_ind, word]
             for word in self.settings['unique_epoch_words']])
            for file_ind in range(len(video_time_files))]

        return

    def import_epochs(self):
        '''
            Using video timestamps files as the base epochs - the assumption is
            that any data without video is not useful.

            At this point, we are completely ignoring trodes comments files.

            Timestamps are converted to seconds.
        '''
        self.import_filename_epoch_info()

        video_time_files = sorted([path for path in Path(
            self._data_path).rglob(
                '*' + self._data_basename + '*' +
                self.settings['video_timestamps_search_term'])])
        self.video_timestamps = {}
        vidTs = []
        starting_frame_ind = [0]
        start_times = []
        stop_times = []
        prev_epoch = ''
        first_ep_ind = []
        for iVid, vidTsFile in enumerate(video_time_files):
            timestamps, clock_rate = (
                trodesCamReader.readCameraModuleTimeStamps(vidTsFile))
            # check if clock rate var exists in self
            if hasattr(self, 'clock_rate'):
                if clock_rate != self.clock_rate:
                    raise ValueError('Clock rate mismatch between ',
                                     + 'video timestamps and rec.')
            else:
                self.clock_rate = clock_rate
            vidTs.append(timestamps)
            starting_frame_ind.append(starting_frame_ind[-1]+len(vidTs[iVid]))
            curr_epoch = self.filename_info['unique_epoch_ID'][iVid]
            if curr_epoch == prev_epoch:
                stop_times[-1] = vidTs[iVid][-1]
            else:
                start_times.append(vidTs[iVid][0])
                stop_times.append(vidTs[iVid][-1])
                prev_epoch = curr_epoch
                first_ep_ind.append(iVid)
        if self.timestamp_offsets is None:
            start_times = np.array(start_times)
            stop_times = np.array(stop_times)
        else:
            vidTs = [ep_ts + offset/self.clock_rate for ep_ts,
                     offset in zip(vidTs, self.timestamp_offsets)]
            start_times = (
                np.array(start_times)
                + (np.array(self.timestamp_offsets)/self.clock_rate))
            stop_times = (
                np.array(stop_times)
                + (np.array(self.timestamp_offsets)/self.clock_rate))
        self.filename_info['first_ep_ind'] = first_ep_ind

        self.epochs = nap.IntervalSet(
            start=start_times, end=stop_times, time_units='s')
        if len(start_times) != len(self.metadata['epochs']):
            raise ValueError('Number of epochs in metadata does not match ',
                             'number of epochs determined by the timestamps ',
                             'files.')
        if not hasattr(self, 'recTime'):
            # Default time - set arbirtrairly - obviously not correct.
            # Required for NWB file.
            self.recTime = datetime.datetime(
                2001, 1, 1, 0, 0, 0, 0, pytz.UTC)
            print('Warning: No recTime found. Using default time.')

        # Save the video timestamps and starting frame indices for the video
        # portion
        self.videos = {}
        self.videos['starting_frame'] = starting_frame_ind[:-1]
        self.videos['timestamps'] = np.concatenate(vidTs)
        return

    def import_LFPs(self):
        print('Importing LFPs...')
        LFP_FOLDER_SUFFIX = self.settings['LFP_folder_suffix']
        LFP_SEARCH_TERM = self.settings['LFP_search_term']
        TIMESTAMPS_SEARCH_TERM = self.settings['LFP_timestamps_search_term']

        lfpFolder = self._data_basename + LFP_FOLDER_SUFFIX
        if not os.path.exists(os.path.join(self._data_path, lfpFolder)):
            print('No LFP folder found.')
            return

        timestampFile = [f for f in os.listdir(os.path.join(
            self._data_path, lfpFolder))
            if TIMESTAMPS_SEARCH_TERM in f][0]
        lfpFiles = [f for f in os.listdir(os.path.join(
            self._data_path, lfpFolder))
            if LFP_SEARCH_TERM in f]
        channelList = [int(re.findall(r'\d+', f)[2]) for f in lfpFiles]

        timestampFile = [f for f in os.listdir(
            os.path.join(self._data_path, lfpFolder))
            if TIMESTAMPS_SEARCH_TERM in f][0]
        lfpFiles = [f for f in os.listdir(
            os.path.join(self._data_path, lfpFolder))
            if LFP_SEARCH_TERM in f]
        channelList = [int(re.findall(r'\d+', f)[2]) for f in lfpFiles]

        timestampFileContents = trodesReader.readTrodesExtractedDataFile(
            os.path.join(self._data_path, lfpFolder, timestampFile))
        lfpTimestamps_secs = (timestampFileContents['data']['time']
                              / self.clock_rate).flatten() # must be 1D

        lfpFileContents = [trodesReader.readTrodesExtractedDataFile(
            os.path.join(self._data_path, lfpFolder, f))
            for _, f in sorted(zip(channelList, lfpFiles))]
        # flatten added to force 1D instead of default X by 1 2D array
        lfpArray = (np.array([lF['data']['voltage'].flatten()
                              for lF in lfpFileContents])
                    * float(lfpFileContents[0]['voltage_scaling'])).T
        trodeID = np.array([lF['ntrode_id'] for lF in lfpFileContents])
        self.lfps = nap.TsdFrame(t=lfpTimestamps_secs, d=lfpArray,
                                 time_units='s', columns=trodeID)
        print('LFPs imported.')
        return

    def import_spikes(self):
        print('Importing spikes...')
        if 'spike_data_format' not in self.settings:
            self.settings['spike_data_format'] = 'ofs'
        if self.settings['spike_data_format'] == 'ofs':
            self.import_spikes_ofs()
        elif self.settings['spike_data_format'] == 'phy':
            self.import_spikes_phy()
        else:
            raise ValueError('Spike data format not recognized.')
        return
    
    def import_spikes_ofs(self):
        '''
        Import spikes from offline sorter format.
        '''
        spike_sported_reg_exp = re.compile(
            self.settings['spike_sorted_channel_reg_exp'])
        spike_sorted_folder = (self._data_basename + self.settings[
            'spike_sorted_folder_suffix'])
        if not os.path.exists(
            os.path.join(self._data_path, spike_sorted_folder)):
            print('No spike folder found.')
            return
        spikeFiles = sorted([f for f in os.listdir(
            os.path.join(self._data_path, spike_sorted_folder))
            if self.settings['spike_sorted_search_term'] in f])

        unitCount = 0
        spikes = {}
        nTrodeIDs = pd.Series(dtype=np.int32)
        allUnitIDs = pd.Series(dtype=np.int32)
        regions = pd.Series(dtype=str)
        for spikeFile in spikeFiles:
            chInd = int(re.match(spike_sported_reg_exp,
                                 spikeFile).group('nTrode'))
            spikeData = pd.read_csv(
                os.path.join(self._data_path, spike_sorted_folder, spikeFile),
                names=self.settings['spike_sorted_data_columns'])
            unitIDs = np.unique(spikeData.Unit)
            for iUnit, thisUnit in enumerate(unitIDs):
                spikes[unitCount + iUnit] = nap.Ts(
                    t=spikeData.where(spikeData.Unit ==
                                      thisUnit).dropna().Timestamp.values,
                    time_units='s'
                )
            nTrodeIDs = pd.concat([nTrodeIDs, pd.Series(itertools.repeat(
                chInd, len(unitIDs)))], ignore_index=True)
            allUnitIDs = pd.concat([allUnitIDs,
                                    pd.Series(unitIDs)], ignore_index=True)
            unitCount += len(unitIDs)

            locInd = list(map(lambda x: chInd in x['nTrodes'],
                              self.metadata['implant_locations'])).index(True)
            region = self.metadata['implant_locations'][locInd]['location']
            regions = pd.concat([regions, pd.Series(itertools.repeat(
                region, len(unitIDs)))], ignore_index=True)

        # Check that unit_stats and actual spikes agree (use wf counts)
        n_waveforms = [len(val) for key, val in spikes.items()]
        unit_stats = self.import_unit_stats()
        if isinstance(unit_stats, pd.DataFrame):
            self.unit_stats = unit_stats
        if hasattr(self, 'unit_stats'):
            if not sum(self.unit_stats['Number of Waveforms'] == n_waveforms):
                print('Unit stats and actual spikes do not agree.')
                raise ValueError(
                    "Mismatch between unit_stats and actual spikes.")

        self.spikes = nap.TsGroup(spikes, time_units="s", nTrode=nTrodeIDs,
                                  unitInd=allUnitIDs, region=regions
                                  )
        return
        
    def import_spikes_phy(self):
        '''
        Import spikes from phy format.
        
        '''
        # Get path from settings
        if 'spike_sorted_folder_suffix' not in self.settings:
            raise ValueError('No spike sorted folder suffix found in settings.')
        elif 'spike_sorted_subpath' not in self.settings:
            spike_sorted_folder = os.path.join(
            (self._data_basename + self.settings['spike_sorted_folder_suffix']))
        else:    
            spike_sorted_folder = os.path.join(
                self._data_path, (self._data_basename
                 + self.settings['spike_sorted_folder_suffix']),
                self.settings['spike_sorted_subpath'])
        
        if not os.path.exists(os.path.join(self._data_path, spike_sorted_folder)):
            print('No spike folder found.')
            return
        # Get list of files in folder:
        spikeFiles = os.listdir(spike_sorted_folder)
        
        is_in_folder = lambda x: x in spikeFiles
        if not all(map(is_in_folder,
                       ['spike_clusters.npy',
                        'spike_times.npy',
                        'cluster_group.tsv'])):
            print('Not all required files found in spike folder. '
                  + 'No spikes imported.')
            return
        
        # phy .npy outputs:
        spike_clusters = np.load(os.path.join(
            spike_sorted_folder, 'spike_clusters.npy'))
        spike_sample_ids = np.load(os.path.join(
            spike_sorted_folder, 'spike_times.npy'))
        cluster_groups = pd.read_csv(os.path.join(
            spike_sorted_folder, 'cluster_group.tsv'), sep='\t')
        unit_inds = cluster_groups.cluster_id[
            cluster_groups.group == 'good'].values
        
        nUnits = len(unit_inds)
        unitCount = 0
        spikes = {}
        allUnitIDs = pd.Series(dtype=np.int32)
        ch_inds = [1384] * nUnits # ToDo: Ask connor where to get this from.
        for iUnit, this_unit in enumerate(unit_inds):
            spike_times = (spike_sample_ids[spike_clusters == this_unit]
                           / self.clock_rate)
            spikes[unitCount + iUnit] = nap.Ts(t=spike_times, time_units='s')
        loc_ind = []
        for i_ch in ch_inds:
            loc_ind.append(list(map(lambda x: i_ch in x['nTrodes'],
                                   self.metadata['implant_locations'])).index(True))
        regions = [self.metadata['implant_locations'][i_loc]['location']
                   for i_loc in loc_ind]
        self.spikes = nap.TsGroup(spikes, time_units="s", nTrode=ch_inds,
                                  unitInd=unit_inds, region=regions
                                  )
        return

    def import_unit_stats(self):

        UNIT_STATS_FOLDER_SUFFIX = self.settings[
            'unit_stats_folder_suffix']
        spike_sported_reg_exp = re.compile(
            self.settings['spike_sorted_channel_reg_exp'])
        UNIT_STATS_SEARCH_TERM = self.settings['unit_stats_search_term']

        stats_folder = os.path.join(self._data_path,
                                    (self._data_basename +
                                     UNIT_STATS_FOLDER_SUFFIX))
        if not os.path.exists(stats_folder):
            print('No waveform stats found.')
            return
        unit_files = sorted([f for f in os.listdir(stats_folder)
                             if UNIT_STATS_SEARCH_TERM in f])

        # Get header names. This should be the same for all files.
        unit_file = unit_files[0]
        chInd = int(re.match(spike_sported_reg_exp,
                             unit_file).group('nTrode'))
        get_names = pd.read_csv(os.path.join(stats_folder, unit_file))
        names = get_names.columns
        if self.settings['unit_stats_has_wf_template']:
            names = names.append(pd.Index(['Waveform Template']))

        # Read all files
        all_unit_stats = pd.DataFrame(columns=names)
        for unit_file in unit_files:
            chInd = int(re.match(spike_sported_reg_exp,
                                 unit_file).group('nTrode'))

            unit_data_one_ch = pd.read_csv(
                os.path.join(stats_folder, unit_file),
                skiprows=[0], header=None)
            # store the waveform template in a list in a single column
            waveform_templates = unit_data_one_ch.iloc[:, len(names)-1:].apply(
                lambda x: x.tolist(), axis=1)
            # remove waveform template from the data
            unit_data_one_ch = unit_data_one_ch.iloc[:, :len(names)]
            # give columns names
            unit_data_one_ch.columns = names
            # add waveform template back to the data as a single column
            unit_data_one_ch['Waveform Template'] = waveform_templates
            unit_data_one_ch['Channel'] = chInd

            all_unit_stats = pd.concat([all_unit_stats, unit_data_one_ch])
        return all_unit_stats

    def import_msort_settings(self):

        return

    def import_DIOs(self):
        print('Importing DIOs...')
        dioFolder = self._data_basename + \
            self.settings['DIO_folder_suffix']
        if not os.path.exists(os.path.join(self._data_path, dioFolder)):
            print('No DIO folder found.')
            return

        dioFiles = [f for f in os.listdir(os.path.join(
            self._data_path, dioFolder))
            if self.settings['DIO_search_term'] in f]
        channelList = [int(re.findall(r'\d+', f)[-1]) for f in dioFiles]
        description = [re.findall(r'\w+', f)[-2].replace('dio_', '')
                       for f in dioFiles]

        dioFileContents = [trodesReader.readTrodesExtractedDataFile(
            os.path.join(self._data_path, dioFolder, f))
            for f, _ in sorted(zip(dioFiles, channelList))]
        dios = {}
        direction = pd.Series(dtype=str, index=range(len(dioFileContents)))
        id = pd.Series(dtype=str, index=range(len(dioFileContents)))

        for iFile, dioFile in enumerate(dioFileContents):
            dioTimestamps_secs = (dioFile['data']['time']
                                  / self.clock_rate)
            dios[iFile] = nap.Tsd(
                t=dioTimestamps_secs,
                d=dioFile['data']['state'],
                time_units='s', time_support=self.epochs,
            )
            direction[iFile] = dioFile['direction']
            id[iFile] = dioFile['id']

        self.dios = nap.TsGroup(
            dios, time_units="s", id=id, direction=direction,
            description=np.array(description)
        )
        '''
        To access your metadata:
        self.dios._metadata is the data frame with metadata, e.g.
        self.dios._metadata.columns
        self.dios._metadata['id']
        '''
        # find the id in the metadata that matches the id in the dio
        print('DIOs imported.')
        return

    def import_videos(self):
        '''
        Load videos from the data folder. Point as if they are in a folder
        local to the nwb file. Will be the case when we upload the NWB file
        to DANDI. Locally, the videos may be in a different folder.

        Video timestamps are used as the core epoch times and are already
        imported.
        '''

        video_search_term = self.settings['video_search_term']
        if 'video_path' in self.paths:
            video_folder = self.paths['video_path']
        else:
            video_folder = self._data_path

        video_file_type = self.settings['video_file_type']

        if video_search_term and video_search_term == 'default':
            video_search_term_full = (self._data_basename +
                                      '*' + video_file_type)
        elif video_search_term:
            video_search_term_full = (video_search_term +
                                      '*' + video_file_type)
        else:
            video_search_term_full = '*' + video_file_type
        # Code to copy videos to a new folder
        # if not os.path.exists(vidFolderWithPath):
        #     videoFiles = sorted([path for path in
        #                          Path(self._data_path).rglob(videoSearchTerm)])
        #     os.makedirs(vidFolderWithPath)
        #     list(shutil.copy2(vid, vidFolderWithPath) for vid in videoFiles)

        video_files = sorted([path for path in
                              Path(video_folder).rglob(
                                  video_search_term_full)])
        if not video_files:
            video_search_term_full = '*' + video_search_term_full
            video_files = sorted([path for path in
                                  Path(video_folder).rglob(
                                      video_search_term_full)])
        if not video_files:
            print('No video files found in {} or subfolders.'.format(
                video_folder))
            return
        self.videos['external_file'] = [
            os.path.join(self._nwb_path, 'videos', video_file)
            for video_file in video_files]

        return

    def import_dlc_csv(self):
        """
        Load tracking data exported with DeepLabCut

        I bet it doesn't support bridging across multiple files yet (in the
        case of fragments for a session).
        """
        DLC_SEARCH_TERM = self.settings['DLC_search_term']
        if 'DLC_path' in self.paths:
            DLC_FOLDER = self.paths['DLC_path']
        else:
            print('No DLC folder given. Skipping DLC import.')
            return
        if not DLC_FOLDER:
            DLC_FOLDER = self._data_path
        DLC_FILE_TYPE = self.settings['DLC_file_type']
        if DLC_SEARCH_TERM and DLC_SEARCH_TERM == 'default':
            DLC_search_term_full = (self._data_basename
                                    + '*' + DLC_FILE_TYPE)
        elif DLC_SEARCH_TERM:
            DLC_search_term_full = DLC_SEARCH_TERM + '*' + DLC_FILE_TYPE
        else:
            DLC_search_term_full = '*' + DLC_FILE_TYPE
        dlcResultFiles = sorted([path for path in
                                 Path(DLC_FOLDER).rglob(DLC_search_term_full)])
        # Search again with leading wildcard if no files found
        if not dlcResultFiles:
            DLC_search_term_full = '*' + DLC_search_term_full
            dlcResultFiles = sorted(
                [path for path in
                 Path(DLC_FOLDER).rglob(DLC_search_term_full)])
        new_order = np.argsort([str(f)[str.find(str(f),
                                                self._data_basename):]
                                for f in dlcResultFiles])
        dlcResultFiles = [dlcResultFiles[i] for i in new_order]
        # Example code that shows how to append to a pandas dataframe in a loop
        # https://stackoverflow.com/questions/20906474/import-multiple-csv-files-into-pandas-and-concatenate-into-one-dataframe
        self.DLC_tracking = pd.concat((
            pd.read_csv(f, header=[1, 2], index_col=0)
            for f in dlcResultFiles), ignore_index=True)
        return

    def import_ecu_logs(self):
        '''
        TODO: Should combine Fragment statescript files into a single log file.
        '''
        # Find files with .stateScriptLog extension recursively in the path
        LOG_SEARCH_TERM = self.settings['log_search_term']
        logFiles = sorted(
            [path for path in Path(self._data_path).rglob(
                '*' + self._data_basename + '*' + LOG_SEARCH_TERM)])

        self.statescript_logs = []
        self.statescript_logs_file = []
        # Read in log files that are text files, store as list of lists
        for iLog, logFile in enumerate(logFiles):
            with open(logFile, 'r') as f:
                # Read by lines
                self.statescript_logs.append(
                    ''.join([line for line in f.readlines()]))
                self.statescript_logs_file.append(logFile.stem)
        return

    # Methods for writing to NWB
    def write_data_to_nwb(self):
        """
            Save the raw data to NWB

            Parameters
            ----------
            self : object
        """
        # I don't think this object can handle if a partial NWB file exists.
        # TODO: Handle and appending to existing NWB files.
        if self._nwb_exists:
            with NWBHDF5IO(self._nwb_filename, 'r+') as io:
                self.nwbfile = io.read()
        else:
            self.nwbfile = self.init_nwb_file()

        '''
        Save data in NWB here
        '''
        self.behavior_module = self.nwbfile.create_processing_module(
            name="behavior", description="Processed behavioral data"
        )

        if hasattr(self, 'epochs'):
            self.write_epochs_to_nwb()
        if hasattr(self, 'metadata') & (hasattr(self, 'lfps')
                                        | hasattr(self, 'spikes')):
            self.create_nwb_electrodes()
        if hasattr(self, 'lfps'):
            self.write_LFPs_to_nwb()
        if hasattr(self, 'spikes'):
            self.write_spikes_to_nwb()
        if hasattr(self, 'dios'):
            self.write_DIOs_to_nwb()
        if hasattr(self, 'videos'):
            self.write_videos_to_nwb()
        if hasattr(self, 'DLC_tracking'):
            self.write_tracking_data_to_nwb()
        if hasattr(self, 'statescript_logs'):
            self.write_ecu_logs_to_nwb()

        self.write_msort_settings_to_nwb()

        with NWBHDF5IO(self._nwb_filename, 'w') as io:
            io.write(self.nwbfile)

        return True

    def init_nwb_file(self):
        self.nwbfile = NWBFile(
            session_start_time=self.recTime,
            identifier=self.metadata["session_id"],
            session_description=self.metadata["session_description"],
            experiment_description=self.metadata["experiment_description"],
            experimenter=self.metadata["experimenter_name"],
            lab=self.metadata["lab"],
            institution=self.metadata["institution"],
            subject=Subject(**self.metadata["subject"]),
            data_collection=self.metadata_all_string,
        )
        # Stashing whole exp metadata file as a strimg in data_collection field
        return self.nwbfile

    def write_epochs_to_nwb(self):
        '''
        Save the epochs from the Trodes comments files to the NWB file. The
        epochs are saved as intervals in the NWB file.
        Epochs in the metadata
        '''

        # Epochs
        self.nwbfile.add_epoch_column(
            name='sesID',
            description='within day file unique session ID')
        self.nwbfile.add_epoch_column(
            name='epoch_info',
            description='all epoch info from file naming convention')
        self.nwbfile.add_epoch_column(
            name='epoch_info_columns',
            description='all session info columns from file naming convention')

        columns = self.filename_info['n_columns'][0]
        for iEp in self.epochs.index:
            epoch_info_ind = self.filename_info[
                self.filename_info['first_ep_ind'] <= iEp].iloc[-1].at[
                    'first_ep_ind']
            tag = '_'.join([self.filename_info.at[epoch_info_ind, word]
                            for word in self.settings['epoch_tag_words']])
            self.nwbfile.add_epoch(
                start_time=self.epochs.loc[iEp, "start"],
                stop_time=self.epochs.loc[iEp, "end"],
                tags=[tag],
                sesID=self.filename_info.at[epoch_info_ind, 'unique_epoch_ID'],
                epoch_info=' '.join(
                    self.filename_info.iloc[epoch_info_ind, :columns]),
                epoch_info_columns=' '.join(
                    self.filename_info.columns[:columns])
            )
        # self.nwbfile.epochs.to_dataframe()
        return

    def create_nwb_electrodes(self):
        # Default settings to save in markup
        devices = []
        for iDevice in self.metadata['data_acq_device']:
            devices.append(self.nwbfile.create_device(
                name=iDevice["system"],
                description=iDevice["description"],
                manufacturer=iDevice["manufacturer"],
            ))
            if iDevice["system"] == "MCU":
                ephysDevice = devices[-1]

        self.nwbfile.add_electrode_column(
            name="chID", description="electrode id string")
        self.nwbfile.add_electrode_column(
            name="hasLFP",
            description="boolean indicator if lfp was collected")
        nTrode_counter = 0
        electrode_counter = 0
        lfpElec = dict()
        for iTrode in self.headerXML.findall(
                './SpikeConfiguration/SpikeNTrode'):
            nTrodeID = iTrode.attrib['id']
            if self.settings['spike_data_format'] == 'phy':
                location = 'PROBE PLACEHOLDER'
            else:
                loc_ind = list(map(lambda x: int(nTrodeID) in x['nTrodes'],
                                self.metadata['implant_locations'])).index(True)
                location = self.metadata['implant_locations'][loc_ind]['location']
            electrode_group = self.nwbfile.create_electrode_group(
                name="nTrode{}".format(nTrodeID),
                description="electrode group for nTrode {}".format(nTrodeID),
                device=ephysDevice,
                location=location
            )
            lfpElec[int(nTrodeID)] = iTrode.attrib['LFPChan']
            nTrode_counter += 1

            iElec = 1
            for thisElec in iTrode.findall('./'):
                if int(iTrode.attrib['LFPChan']) == iElec:
                    hasLFP = True
                else:
                    hasLFP = False
                self.nwbfile.add_electrode(
                    group=electrode_group,
                    id=int(thisElec.attrib['hwChan']),
                    chID="nTrode{}_elec{}".format(nTrodeID, iElec),
                    location=location,
                    hasLFP=hasLFP
                )
                iElec += 1
                electrode_counter += 1
        return

    def write_LFPs_to_nwb(self):
        self.ecephys_module = self.nwbfile.create_processing_module(
            name="ecephys",
            description="Processed extracellular electrophysiology data",
        )
        # lfp_elecs = [ind for ind, iElec in enumerate(
        #     self.nwbfile.electrodes.hasLFP) if iElec]

        # Index the correct electrode number if you did not keep all electrodes
        # during recording
        all_LFP_elecs = [(ind, val.replace('nTrode', ''))
                         for ind, val in enumerate(
                             self.nwbfile.electrodes.group_name[:])
                         if self.nwbfile.electrodes.hasLFP[ind]]
        inds = [i[0] for i in all_LFP_elecs]
        vals = [i[1] for i in all_LFP_elecs]
        lfp_elecs = [inds[[i for i, this_val in enumerate(vals)
                           if each_lfp == this_val][0]]
                     for each_lfp in self.lfps.columns.values]

        lfp_table_region = self.nwbfile.create_electrode_table_region(
            region=lfp_elecs, description="lfp electrodes"
        )
        lfp_electrical_series = ElectricalSeries(
            name="ElectricalSeries",
            data=self.lfps.data(),
            electrodes=lfp_table_region,
            timestamps=self.lfps.times(),
            conversion=0.000001  # Data stored in uV currently
        )
        lfp = LFP(electrical_series=lfp_electrical_series)
        self.ecephys_module.add(lfp)
        return

    def write_spikes_to_nwb(self):
        # Adding units
        self.nwbfile.add_unit_column(
            name="nTrode", description="electrode id integer")
        self.nwbfile.add_unit_column(
            name="unitInd",
            description="integer id each unit within each nTrode")
        self.nwbfile.add_unit_column(
            name="region",
            description="string region where the ntrode is located")
        if hasattr(self, 'unit_stats'):
            self.nwbfile.add_unit_column(
                name="nWaveforms",
                description="integer number of waveforms for each unit")
            self.nwbfile.add_unit_column(
                name="waveformFWHM",
                description="float full width half max of the waveform")
            self.nwbfile.add_unit_column(
                name="waveformPeakMinusTrough",
                description="float peak minus trough of the waveform")
        # id is int
        for u in self.spikes.keys():
            if hasattr(self, 'unit_stats'):
                self.nwbfile.add_unit(
                    id=u,
                    nTrode=self.spikes.get_info('nTrode')[u],
                    unitInd=self.spikes.get_info('unitInd')[u],
                    region=self.spikes.get_info('region')[u],
                    nWaveforms=self.unit_stats['Number of Waveforms'].iloc[u],
                    waveformFWHM=self.unit_stats[
                        'Valley FWHM of Unit Template'].iloc[u],
                    waveformPeakMinusTrough=(self.unit_stats[
                        'Peak to Valley Ticks of Unit Template'].iloc[u]
                        / self.clock_rate),
                    spike_times=self.spikes[u].as_units("s").index.values,
                )
            else:
                self.nwbfile.add_unit(
                    id=u,
                    nTrode=self.spikes.get_info('nTrode')[u],
                    unitInd=self.spikes.get_info('unitInd')[u],
                    region=self.spikes.get_info('region')[u],
                    spike_times=self.spikes[u].as_units("s").index.values,
                )
        # self.nwbfile.units.to_dataframe() # to see results (while debugging)
        return

    def write_msort_settings_to_nwb(self):

        return

    def write_DIOs_to_nwb(self):
        time_series = []
        for iDio in self.dios.keys():
            time_series.append(TimeSeries(
                name=self.dios._metadata.at[iDio, 'id'],
                data=self.dios[iDio].data(),
                timestamps=self.dios[iDio].times(),
                description=self.dios._metadata.at[iDio, 'description'],
                unit='binary',
            ))

        dios = BehavioralTimeSeries(
            time_series=time_series,
            name="DIOs",
        )
        self.nwbfile.add_acquisition(dios)

        return

    def write_videos_to_nwb(self):
        behavior_external_file = ImageSeries(
            name="Behavioral Tracking Videos",
            description="Behavior video of animal moving in environment.",
            unit="n.a.",
            external_file=self.videos['external_file'],
            format="external",
            # frame lengths of the videos
            starting_frame=self.videos['starting_frame'],
            timestamps=self.videos['timestamps'],
        )

        self.nwbfile.add_acquisition(behavior_external_file)
        return

    def write_tracking_data_to_nwb(self):
        timestamps = self.videos['timestamps']
        bodyparts = self.DLC_tracking.columns.levels[0]
        bodypart_data = [
            self.DLC_tracking.loc[:, (iPart, ['x', 'y'])].to_numpy()
            for iPart in self.DLC_tracking.columns.levels[0]]
        bodypart_likelihood = [
            self.DLC_tracking.loc[:, (iPart, ['likelihood'])].to_numpy()
            for iPart in self.DLC_tracking.columns.levels[0]]

        if bodypart_data[0].shape[0] > len(timestamps):
            trim_size = bodypart_data[0].shape[0] - len(timestamps)
            print(f"Trimming {trim_size} lines from the end of the dlc data.",
                  "This is due to a mismatch between the number of video",
                  "timestamps and the number of frames in the video/DLC",
                  "tracking data.")
        elif bodypart_data[0].shape[0] < len(timestamps):
            trim_size = len(timestamps) - bodypart_data[0].shape[0]
            Warning.warn(f"Buffering the dlc data with {trim_size} frames of ",
                         "zeros to match the number of video timestamps.")
            # 30 frames is typically 1 second for us - so
            if trim_size > TRIM_LIMIT: 
                raise ValueError("The DLC data is missing too many frames to ",
                                 "be useful. Please check the DLC data.")
            # Buffer the data with zeros
            for iPart in range(len(bodypart_data)):
                bodypart_data[iPart] = np.vstack(
                    (bodypart_data[iPart], np.zeros((trim_size, 2)))
                )
                bodypart_likelihood[iPart] = np.vstack(
                    (bodypart_likelihood[iPart], np.zeros((trim_size, 1)))
                )
        # create a skeleton that define the relationship between the markers. 
        # also link this skeleton to the subject.
        skeleton = Skeleton(
            name="skeleton",
            subject=self.nwbfile.subject,
            nodes=list(bodyparts),)
            # define edges between nodes using the indices of the nodes in the node list.
            # edges=np.array([[0, 1], [1, 2]], dtype="uint8"),
        # )

        # List comprehension to create a list of PoseEstimationSeries objects
        # from the bodyparts
        pose_estimation_series = [PoseEstimationSeries(
            name=partName,
            description='',
            data=bodypart_data[iPart][:len(timestamps), :],
            unit='pixels',
            reference_frame='(0, 0) corresponds to bottom left of the image',
            timestamps=timestamps,
            confidence=bodypart_likelihood[iPart].reshape(-1),
            confidence_definition='Softmax output of the deep neural network.',
        )
            for iPart, partName in enumerate(bodyparts)]

        pe = PoseEstimation(
            pose_estimation_series=pose_estimation_series,
            description='Estimated body part positions using DeepLabCut.',
            skeleton=skeleton,
        )
        self.behavior_module.add(skeleton)
        self.behavior_module.add(pe)
        return

    def write_ecu_logs_to_nwb(self):
        statescript_log = TimeSeries(
            name="Statescript Logs",
            data=self.statescript_logs,
            starting_time=1.0,
            rate=1.0,
            description="Behavior and programming logs from ECU", unit="n.a.",
        )
        statescript_log_file = TimeSeries(
            name="Statescript Log File Names",
            data=self.statescript_logs_file,
            starting_time=1.0,
            rate=1.0,
            description="Corresponding file names for logs", unit="n.a.",
        )
        statescript_log_beh_events = BehavioralEvents(
            time_series=statescript_log, name="Statescript Logs"
        )
        statescript_log_beh_events.add_timeseries(statescript_log_file)
        self.nwbfile.add_acquisition(statescript_log_beh_events)

        return

    def read_nwb(self):
        """
        This loads an existing Jadhav Lab formatted nwb file.
        """
        self.get_epochs_as_dataframe()
        self.epochs_int_set = self.get_epochs_for_pynapple()
        return

    def get_exp_metadata(self):
        if not hasattr(self, 'metadata'):
            with NWBHDF5IO(self._nwb_filename, 'r') as io:
                nwbfile = io.read()
            self.metadata = yaml.safe_load(nwbfile.data_collection)
        return self.metadata

    def get_epochs_as_dataframe(self):
        if not hasattr(self, 'epochs'):
            with NWBHDF5IO(self._nwb_filename, 'r') as io:
                self.nwbfile = io.read()
                self.epochs = self.nwbfile.intervals['epochs'].to_dataframe()
        return self.epochs

    def get_epochs_for_pynapple(self):
        if not hasattr(self, 'epochs_int_set'):
            self.epochs_int_set = nap.IntervalSet(
                start=self.epochs['start_time'],
                end=self.epochs['stop_time'],
                time_units='s')
        return self.epochs_int_set

    def get_electrode_metadata(self):
        if not hasattr(self, 'electrode_meta'):
            with NWBHDF5IO(self._nwb_filename, 'r') as io:
                nwbfile = io.read()
                self.electrode_meta = nwbfile.electrodes.to_dataframe()
                self.electrode_meta['trodeID'] = [
                    int(re.findall(r'\d+', iCh)[0])
                    for iCh in self.electrode_meta['group_name']]
        return self.electrode_meta

    def get_LFPs(self, lfpInd=None, epoch=None):
        """
        Read lfps from nwb and return as pynapple TsdFrame

        Parameters
        ----------
        lfpInd : int, optional
            Pass in the slice of the lfps to be returned.
            If None, all lfps are returned.
        """
        lfps = False
        if not hasattr(self, 'lfp_meta'):
            self.get_LFP_metadata()
        with NWBHDF5IO(self._nwb_filename, 'r') as io:
            nwbfile = io.read()

            lfpElecSeries = (nwbfile.processing['ecephys']['LFP']
                             .get_electrical_series())
            trodeId = self.lfp_meta['trodeID'].astype('str')
            if lfpInd is None:
                data = lfpElecSeries.data[:]
                columns = trodeId
            else:
                data = lfpElecSeries.data[:, lfpInd]
                columns = trodeId.iloc[lfpInd]
            lfps = nap.TsdFrame(
                t=lfpElecSeries.timestamps[:], d=data,
                time_support=self.epochs_int_set, time_units='s',
                columns=columns.astype('str'))
        return lfps

    def get_LFP_metadata(self):
        """
            Returns dataframe of LFP IDs
        """
        if not hasattr(self, 'lfp_meta'):
            with NWBHDF5IO(self._nwb_filename, 'r') as io:
                nwbfile = io.read()
                self.lfp_meta = (nwbfile.processing['ecephys']['LFP']
                                 .get_electrical_series()
                                 .electrodes.to_dataframe())
            # Add trodeID column - an integer representation of the trode name
            self.lfp_meta['trodeID'] = [int(re.findall(r'\d+', iCh)[0])
                                        for iCh in self.lfp_meta['group_name']]
        return self.lfp_meta

    def get_DIOs(self, remove_empty=True):
        """
        Read dios from nwb and return as pynapple TsGroup
        """
        if hasattr(self, 'dios'):
            return self.dios
        self.dios = False
        with NWBHDF5IO(self._nwb_filename, 'r') as io:
            nwbfile = io.read()

            if 'DIOs' not in nwbfile.acquisition:
                print()
                return self.dios
            info_dict = {
                'id': np.array(list(
                    nwbfile.acquisition['DIOs'].time_series.keys())),
                # 'desc': np.array(
                #     [nwbfile.acquisition['DIOs'][iDio].description
                #         for iDio in
                #         nwbfile.acquisition['DIOs'].time_series.keys()]),
            }
            dios = DIOs({ind_dio: DIO(
                t=nwbfile.acquisition['DIOs'].time_series[i_dio].timestamps[:],
                d=nwbfile.acquisition['DIOs'].time_series[i_dio].data[:],
                time_units='s', time_support=self.epochs_int_set)
                for ind_dio, i_dio in enumerate(
                    nwbfile.acquisition['DIOs'].time_series.keys())},
                **info_dict)
        exp_metadata = self.get_exp_metadata()
        dios.add_DIO_types(exp_metadata)
        if remove_empty:
            dios = DIOs.remove_empty_DIOs(dios)
        self.dios = dios
        return self.dios

    def get_tracking(self):
        '''
        Read tracking data from nwb and return as Pandas MultiIndex DataFrame
        '''
        if hasattr(self, 'tracking'):
            return self.tracking
        with NWBHDF5IO(self._nwb_filename, 'r') as io:
            nwbfile = io.read()

            bodyparts = list(nwbfile.processing['behavior']['PoseEstimation'].
                             fields['pose_estimation_series'].keys())
            col_names = pd.MultiIndex.from_product(
                [bodyparts, ['xs', 'ys', 'confidence']],
                names=['bodypart', 'data'])
            t = (nwbfile.processing['behavior']['PoseEstimation']
                 [bodyparts[0]].timestamps[:])
            # Concatenate all bodyparts into one dataframe - first concatenate
            # xs, ys, and confidence, then concatenate all bodyparts.
            self.tracking = (pd.DataFrame(np.concatenate([np.concatenate((
                nwbfile.processing['behavior']['PoseEstimation'][iBP].data[:],
                nwbfile.processing['behavior']['PoseEstimation'][iBP]
                .confidence[:][:, None]), axis=1)
                for iBP in bodyparts], axis=1), index=t, columns=col_names))
        return self.tracking

    def get_spikes(self):
        spikes = False
        with NWBHDF5IO(self._nwb_filename, 'r') as io:
            nwbfile = io.read()
            if 'units' in nwbfile.fields:
                spikes = nwbfile.units.to_dataframe()
        return spikes

    def get_statescript_logs(self):
        """
        Get the statescript logs from the nwb file.

        Returns:
        --------
        statescript_logs : list
            List of strings with the contents of the statescript logs
        """
        if hasattr(self, 'statescript'):
            return self.statescript['logs']

        statescript = dict()
        with NWBHDF5IO(self._nwb_filename, 'r') as io:
            nwbfile = io.read()
            if 'Statescript Logs' in nwbfile.acquisition:
                statescript['logs'] = (nwbfile.acquisition['Statescript Logs'].
                                       time_series['Statescript Logs'].data[:])
                statescript['file_names'] = (
                    nwbfile.acquisition['Statescript Logs'].
                    time_series['Statescript Log File Names'].data[:])
            else:
                statescript = None
        self.statescript = statescript
        return self.statescript['logs']

    def get_statescript_filenames(self):
        """
        Get the filenames of the statescript logs from the nwb file.

        Returns:
        --------
        statescript_filenames : list
            List of strings with the filenames of the statescript logs
        """
        if not hasattr(self, 'statescript'):
            self.get_statescript_logs()
        return self.statescript['logs']


if __name__ == '__main__':
    # Default usage for JLab to write NWB files.
    import write_jlab_nwb_settings.import_settings as jlab_to_nwb_settings
    JLabNWBHandler(all_paths=jlab_to_nwb_settings.get_paths(),
                   settings=jlab_to_nwb_settings.get_settings())