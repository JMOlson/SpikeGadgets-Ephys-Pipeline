# -*- coding: utf-8 -*-
"""
Extracts Trodes camera module timestamps from exported .videoTimeStamps files

Requires Numpy to be installed.
@author: Chris@spikeGadgets.com
@editor: jolson1129@gmail.com
Assumes Python 3.9
"""
import re

import numpy as np

CLOCK_STRING = "Clock rate: "
HEADER_END_STRING = "End settings"


def readCameraModuleTimeStamps(filename):
    '''
    The below function reads the header in order to get the clock rate, then
    reads the rest of the file as uint32s and divides by the clock rate to get
    the timestamps in seconds.

    The header length switches, so reading lines seems more reliable..
    Encoding appears to be latin-1, not UTF-8.
    '''
    with open(filename, "r", encoding='latin-1') as fid:
        while True:
            header_text = fid.readline()
            # find substring "clock rate: " in header_text
            if header_text.find(CLOCK_STRING) != -1:
                clock_rate = int(re.search(r'\d+', header_text)[0])
            elif header_text.find(HEADER_END_STRING) != -1:
                break
        timestamps = np.fromfile(fid, dtype=np.uint32) / clock_rate
    return timestamps, clock_rate


# Example import and call
# import readCameraModuleTimeStamps as readCMTS
# timestampData = readCMTS.readCameraModuleTimeStamps(r"C:\Users\<YOURPATH>\<YOURFILENAME>.videoTimeStamps")
