from JLabNWB import JLabNWBHandler


def test_init_variable_handling():
    """
    Test that the init function handles variables correctly.
    """
    # No Filename, No Path, in a folder w/ data
    
    # No Filename, No Path, in a folder w/out data
    
    # No Filename, path with data
    
    # No Filename, path w/out data
    
    # Filename without extension, No Path, in a folder w/ data 
    
    # Filename without extension, No Path, in a folder w/out data
    
    # Filename without extension, global path with data
    
    # Filename without extension, global path w/out data
    
    # Filename without extension, local path with data
    
    # Filename without extension, local path w/out data
    
    # Filename with extension, path w/ data
    
    # Filename with extension, path w/out data
     
    # Filename with included global path w/ data
    
    # Filename with included global path w/out data
    
    # Filename with included local path w/ data
    
    # Filename with included local path w/out data
    
    
    
    # Global data path w/ data
    
    # Global data path w/out data
    
    # Local data path w/ data
    
    # Local data path w/out data
        
    # Load valid path with no filename

    # Valid data folder, nwb path given but doesn't exist
    
    # etc.
    # ...
    
    # assert it is what it should be
    
