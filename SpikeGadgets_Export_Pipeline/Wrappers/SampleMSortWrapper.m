%% Data Preprocessing Script
addpath('~/conda/envs/mlab/lib/node_modules/mountainlab/utilities/matlab/mdaio/');
msortOutSuffix = '_msortOut';

%% Trodes I care about and don't, and how I will cluster them.
nTrodes = 64;
mSortTrodeList = [2:3,6:8,9,11,21:25,27,33,36,38,40:43,45:47,49,52,54:56,58:59,61:64];
manSortTrodeList = [4,18,26];
lfpOnlyList = [5];
cellLists = union(mSortTrodeList,manSortTrodeList);
lfpTrodeList = union(cellLists,lfpOnlyList);
trashTrodeList = setxor(1:nTrodes,lfpTrodeList);

%% SL18_D19 Details
dataPath = '/media/jmolson/16TBData/Data/SubLearning/SL18/';
dayFolder = 'SL18_D19';

recFolder = [dataPath, filesep, dayFolder];

%% Auto Clustering script for clustering experiment.

% All sessions together
recMask = dayFolder;
mdaDir = [recFolder, filesep, recMask,'.mountainsort'];
msortOutFolder = [recFolder, filesep, recMask, msortOutSuffix];

msortRec(mdaDir, msortOutFolder, 'ConfigStruct', Cfg,...
    'trodeList', mSortTrodeList);
convertForPLXManualCut(mdaDir, msortOutFolder, 'ConfigStruct', Cfg,...
    'trodeList', manSortTrodeList);








