function msortMinimalWrapper(recFolder, recMask, mSortTrodeList, manSortTrodeList)
%% MSort Minimal - Works with my export wrappers.

addpath('~/conda/envs/mlab/lib/node_modules/mountainlab/utilities/matlab/mdaio/');
MSORT_OUT_SUFFIX = '_msortOut';

%% Auto Clustering script for clustering experiment.
mdaDir = [recFolder, filesep, recMask,'.mountainsort'];
msortOutFolder = [recFolder, filesep, recMask, MSORT_OUT_SUFFIX];
msortRec(mdaDir, msortOutFolder, 'trodeList', mSortTrodeList);
if ~isempty(manSortTrodeList)
    convertForPLXManualCut(mdaDir, msortOutFolder,...
        'trodeList', manSortTrodeList);
end

end