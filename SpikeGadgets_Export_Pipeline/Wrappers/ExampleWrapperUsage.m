%% Sample Ephys usage script. - linux only
dataPath = '/media/jmolson/SpeedData/DataProcessing';
dayFolder = 'SL06_D23';

recFolder = [dataPath, filesep, dayFolder];
recMask = 'SL06_D23_S02';

%% Most basic usage - uses all default settings. Open exportTrodesData.m to see
% all possible settings that can be changed.

% Export one session.
exportTrodesRecsAsOne(recFolder, recMask, {});

%% Most basic usage. Export all recs as one day.
exportTrodesRecsAsOne(recFolder, dayFolder, {});

%% Use an external trodesconf file.
recTrodesConfig = [recFolder, filesep, 'SL06_D23.trodesconf'];
exportTrodesRecsAsOne(recFolder, recMask,{},'reconfig',recTrodesConfig);

%% Grab only LFPs, and change a setting on them.
exportTrodesRecsAsOne(recFolder, recMask, {'lfp'}, 'lfpLowPass', 500);
