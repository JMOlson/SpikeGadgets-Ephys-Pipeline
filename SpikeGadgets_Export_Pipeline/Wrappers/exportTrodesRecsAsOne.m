function exportTrodesRecsAsOne(recFolder, fileNameMask, exportDataTypes, varargin)
% Wrapper for exportTrodesData. % Run to export a single spikegadgets
% recording for input into processing software (matlab, python, etc.).
%
% exportTrodesRecsAsOne(recFolder, fileNameMask, exportDataTypes, varargin)
% exportDataTypes is a cell array of strings that are data types desired to be exported. Valid values are: 'spikes' 'lfp' 'spikeband' 'raw' 'stim' 'dio' 'aio' 'mountainsort' 'kilosort' 'time'
%   Defaults are lfp, dio, and mountainsort
%
% All exportTrodesData settings may be passed into this wrapper, in one of two
% ways. exportDataTypes is optional in both methods.
% Method 1:
% exportTrodesRecsAsOne(recFolder, fileNameMask, exportDataTypes, Settings)
%       Settings is a struct. Each field in Settings is a parameter, and its
%       corresponding value is the desired value.
% Method 2:
% exportTrodesRecsAsOne(recFolder, fileNameMask, exportDataTypes, parameter, value, p,v,p,v,...)
%        Pass parameter/value pairs in to the function. For example:
%       exportTrodesRecsAsOne(recFolder, fileNameMask, 'lfpLowPass', 600, 'useLfpRefs', true);
%
%

%%  Create Trodes Comments File - do here?
% RN_createTrodesComments(sortedRecs);
% exportDataTypes = {'mda','raw','time'};

%% Parse Inputs
p = inputParser;
p.StructExpand = false;
p.KeepUnmatched = true;
validStr = @(x) isstring(x) || ischar(x);
validExportDataType = @(x) iscell(x);
addRequired(p,'recFolder',validStr);
addRequired(p,'fileNameMask',validStr);
addRequired(p,'exportDataTypes',validExportDataType);
addParameter(p,'outputDir',recFolder,validStr);
addParameter(p,'outputName',fileNameMask,validStr);

parse(p,recFolder, fileNameMask, exportDataTypes, varargin{:});

recFolder = p.Results.recFolder;
fileNameMask = p.Results.fileNameMask;
exportDataTypes = p.Results.exportDataTypes;
outputDir = p.Results.outputDir;
outputName = p.Results.outputName;

if isempty(exportDataTypes)
    exportDataTypes = {'lfp','dio','mountainsort'};
end
% Any unmatched parameters are Settings parameters, or the Settings struct.
if numel(fieldnames(p.Unmatched)) == 0
    Settings = [];
elseif isfield(p.Unmatched,'Settings')
    Settings = p.Unmatched.Settings;
else
    Settings = p.Unmatched;
end

%% Find the recording files.
recFileString = grabFileNamesWithFileMask(fileNameMask, 'rec', recFolder);

%% Export each data type requested.
nExport = numel(exportDataTypes);
for iExport=1:nExport
    if iExport == 1
        % Setup log
        mkdir([outputDir, filesep, 'Logs']);
        logFile = [outputDir, filesep, 'Logs', filesep, fileNameMask, '_ExtractionLog.log'];
        diary(logFile);
    end


    if ~isempty(Settings)
        exportTrodesData(exportDataTypes{iExport}, recFileString, outputDir, outputName, Settings);
    else
        exportTrodesData(exportDataTypes{iExport}, recFileString, outputDir, outputName);
    end
end

diary OFF;
end
