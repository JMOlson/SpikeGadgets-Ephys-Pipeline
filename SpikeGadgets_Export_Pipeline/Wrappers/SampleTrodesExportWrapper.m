%% Trodes Export Example Script

%% Trodes I care about and don't, and how I will cluster them.
nTrodes = 64;
mSortTrodeList = [2:3,6:8,9,11,21:25,27,33,36,38,40:43,45:47,49,52,54:56,58:59,61:64];
manSortTrodeList = [4,18,26];
lfpOnlyList = [5];
cellLists = union(mSortTrodeList,manSortTrodeList);
lfpTrodeList = union(cellLists,lfpOnlyList);
trashTrodeList = setxor(1:nTrodes,lfpTrodeList);

%% SL18_D19 Export
dataPath = '/media/jmolson/16TBData/Data/SubLearning/SL18/';
baseName = 'SL18_D19';
recFolder = [dataPath, filesep, baseName];

%% All sessions together
recMask = baseName;
exportTrodesRecsAsOne(recFolder, recMask, {});
% trashUnwantedTrodes(trashTrodeList,[recFolder,filesep,recMask]);







