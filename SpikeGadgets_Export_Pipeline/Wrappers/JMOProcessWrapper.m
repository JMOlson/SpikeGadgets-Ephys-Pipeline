%% Data Preprocessing Script

%% Export Parameters
dataPath = '/mnt/SoFast/DataProcessing';
dayFolder = 'SL06_D23';
recMask = dayFolder;

recFolder = [dataPath, filesep, dayFolder];

%% Trodes I care about and don't, and how I will cluster them.
nTrodes = 64;
mSortTrodeList = [12,30,31,32,44,48,49,51,54,55,57,58,60,61,62];
manSortTrodeList = [1,4,9,10,11,14,15,18,19,21,22,23,26,28,33,38,42,45,50,52,56,63,64];
lfpOnlyList = [2,3,5,6,7,8,13,20,24,25,27,29,34,35,36,37,39,40,41,43,46,47,59];
cellLists = union(mSortTrodeList,manSortTrodeList);
lfpTrodeList = union(cellLists,lfpOnlyList);
trashTrodeList = [16,17,53];

%% All sessions together
if ~exist([recFolder, filesep, '.LFP'], 'dir')
    exportTrodesRecsAsOne(recFolder, recMask, {});
    trashUnwantedTrodes(trashTrodeList,[recFolder,filesep,recMask]);
end

%% MountainSort AutoClustering
% If it errors with something about the process_cache.json, just delete
% that file and try again.

% All sessions together
recMask = dayFolder;
msortMinimalWrapper(recFolder, recMask, mSortTrodeList, manSortTrodeList)







