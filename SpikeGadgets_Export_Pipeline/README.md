# SpikeGadgets Ephys Pipeline - JMO 
This file contains usage instructions for the SpikeGadgets Ephys Pipeline by Jake Olson, circa 2022.

## Usage: 
This library contains code to export data from Spikegadget's Trodes, run semi-automated clustering using mountainsort, create files for sorting curation with OfflineSorter, and bringing data into MATLAB in the filter framework format (Frank Lab format).

## Terminology:
Rec - A single recording file and what it entails
Session - A (series of) recording file(s) (typically one, but if spikegadgets crashes during recording, there will be additional.
- The base unit of analysis - i.e. you never analyze a portion of a session.
- e.g. - a day may have 3 sessions - sleep, behav, sleep
- I recommend exporting 1 session at a time - this is how this pipeline is configured for - by default.
Day - All sessions that may be desired to process spikes together (typically all in 1 day). Some people export each day together - that won't work for the OfflineSorter clustering protocol (as of 10/2022).


## Steps to process a session:
1. Verify all necessary properties for the recordings
    are correct. Save as separate trodescomment file in that folder if a change is needed.
        - Reference Trode/Wire for each trode
        - LFP wire selected for each trode
        - While verifying, create lists of trodes for:
            - many cells - pass to mountinsort
            - single cells - cluster by hand
            - no cells, good lfp - if you want any lfp only wires
            - the rest (trash)

2. Copy desired session to fast drive.
3. Export all relevant data from session.
    In matlab, write a script to specify the folder and settings for export and run exportTrodesRecsAsOne.
    See example - sampleWrapperScript.m
    **Note: Must be done in Linux**
4. Run mountainsort on the desired tetrodes.
    - Example script: TODO
    - Requires the mountainsort folder to be on the path. TODO auto do this. 
    **Note: Must be done in Linux**
5. Run mountainsort preprocessing and export continuous data for manual clustering on the single cell trodes.
    - Included in example script: TODO
    **Note: Must be done in Linux**
6. Manual cluster.
    - Acquire offline sorter access through usb redirect (instructions @ TODO)
    - Open OfflineSorter
    - File -> Import -> Continuous Data
    **Note: Must be done in Windows**
7. Curate autoclustered data.
    - Open OfflineSorter
    - File -> Import -> Spike Data
    **Note: Must be done in Windows**



## Common errors
File "D:\pythonCode\JLab-Analysis-Suite\SpikeGadgets_Export_Pipeline\JLabNWB\jlab_nwb.py", line 1008, in write_videos_to_nwb
    behavior_external_file = ImageSeries(
                             ^^^^^^^^^^^^
  File "c:\Users\BlakePorter\anaconda3\envs\SL_Analysis\Lib\site-packages\hdmf\utils.py", line 668, in func_call
    return func(args[0], **pargs)
           ^^^^^^^^^^^^^^^^^^^^^^
  File "c:\Users\BlakePorter\anaconda3\envs\SL_Analysis\Lib\site-packages\pynwb\image.py", line 112, in __init__
    self._error_on_new_warn_on_construct(
  File "c:\Users\BlakePorter\anaconda3\envs\SL_Analysis\Lib\site-packages\pynwb\core.py", line 46, in _error_on_new_warn_on_construct
    raise ValueError(error_msg)
ValueError: ImageSeries 'Behavioral Tracking Videos': The number of frame indices in 'starting_frame' should have the same length as 'external_file'.

This issue occurs when your settings file (e.g. import_settings_fullRecs.yml) does not have the the correct video_file_type resulting in a mismatch of .videoTimeStamps to video files
