#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Report Behavior Stats from Statescript Output

StateScript Output Template:
    
For Events - 3 numbers, space separated:
    "time (uSecs)" "input status (binary)" "output status (binary)" 
For Messages - 1 number, then message:
    "time (uSecs)" "message"
For Code - code line - so not a number first.

License:
BSD-3-Clause
"""

# Futures

# Built-in/Generic Imports
# import itertools
# import operator

# Libs
import matplotlib.pyplot as plt
import numpy as np

# import pandas as pd

# Own modules
# from StateScriptRecData import StateScriptRec as SSRec

__author__ = 'Jacob M Olson'
__copyright__ = 'Copyright 2021, Jacob M Olson'
__credits__ = ['Jacob M Olson']
__license__ = 'BSD-3-Clause'
__version__ = '0.0.1'
__maintainer__ = 'Jacob M Olson'
__email__ = 'jolson1129@gmail.com'
__status__ = 'dev'


# ChatGPT transalation from my MATLAB code, then i fixed it
# Usage example:
# Assuming behGroup is a list of dictionaries containing 'beh' and 'id' keys
# where 'beh' is another dictionary containing 's', 'nRewards',
# 'inbound_performance', and 'outbound_performance' as keys.
# fig = plotInboundOutboundPerformance(behGroup)
def plotInboundOutboundPerformance(behGroup):
    rewardPlotMax = 1
    nBehSessionMax = 1

    fig, axs = plt.subplots(3, 1, figsize=(10, 10))

    for iRat in behGroup:
        nBehSessionMax = max(nBehSessionMax, iRat['beh']['nSessions'])
        rewardPlotMax = max(rewardPlotMax,
                            np.ceil(max(iRat['beh']['nRewards'])/10)*10)
        axs[0].plot(iRat['beh']['nRewards'])

        axs[1].plot(iRat['beh']['inbound_performance'])
        axs[2].plot(iRat['beh']['outbound_performance'])

    axs[0].set_xlim(0, nBehSessionMax)
    axs[0].set_ylim(0, rewardPlotMax)
    axs[0].set_title('# Rewards')

    axs[1].set_xlim(0, nBehSessionMax)
    axs[1].set_ylim(0, 1)
    axs[1].set_title('Inbound Performance')

    axs[2].set_xlim(0, nBehSessionMax)
    axs[2].set_ylim(0, 1)
    axs[2].set_title('Outbound Performance')
    axs[0].legend([str(rat['id']) for rat in behGroup])

    fig, ax = plt.subplots(figsize=(8, 5))

    inbound_performance = np.full((len(behGroup), nBehSessionMax), np.nan)
    outbound_performance = np.full((len(behGroup), nBehSessionMax), np.nan)

    ind = 0
    for iRat in behGroup:
        ind += 1
        inbound_performance[ind-1, :nBehSessionMax] = (
            iRat['beh']['inbound_performance'])
        outbound_performance[ind-1, :nBehSessionMax] = (
            iRat['beh']['outbound_performance'])

    ax.plot(np.nanmean(inbound_performance, axis=0))
    ax.plot(np.nanmean(outbound_performance, axis=0))
    ax.set_xlim(0, nBehSessionMax)
    ax.set_ylim(0, 1)
    ax.legend(['Inbound Performance', 'Outbound Performance'])

    plt.show()
    return fig


# Main Fn
if __name__ == '__main__':
    # Unit Test goes here, will run if run as script.
    pass
