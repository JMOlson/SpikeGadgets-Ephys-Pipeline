#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Built-in/Generic Imports
import os
import argparse

import analyze_one_nwb_settings.import_settings as analyze1_settings
import Beh.Tracking.tracking_preprocessor as tr_pre
import beh_core_settings.import_settings as beh_settings_importer
import Utils.epoch_tools as ep_tools
from jlab_nwb import JLabNWBHandler


def build_movement_dfs(nwb_file=None, pc=None, proj=None):
    # Settings Import
    # All hard coded values are loaded or defined here
    paths, rec_settings = analyze1_settings.init_analysis(nwb_file, pc, proj)
    tracking_settings = beh_settings_importer.import_tracking_settings()

    movement_df_file = os.path.join(paths['beh_path'], paths['nwb_file_stem']
                                    + rec_settings['fname_tracking_df'])
    movement_smooth_df_file = os.path.join(
        paths['beh_path'], paths['nwb_file_stem']
        + rec_settings['fname_tracking_smooth_df'])

    if (os.path.exists(movement_df_file) and
            os.path.exists(movement_smooth_df_file)):
        print('Movement dataframes already exist. Skipping creation.')
        return

    # Open NWB
    day_rec = JLabNWBHandler(nwb_name=paths['nwb_file'],
                             nwb_path=paths['nwb_path'])

    # Open epochs
    ep_file = os.path.join(paths['nwb_path'], paths['nwb_file_stem']
                           + rec_settings['fname_base_epochs'])
    epochs_intSet, epochs_df = ep_tools.load_base_epochs(ep_file)

    tracking = day_rec.get_tracking()
    tracking = tr_pre.fix_tracking_timestamp_duplicates(
        tracking, show_plot=False)

    # Get tracked position
    (position_cm, head_direction,
     position_smooth_cm, head_direction_smooth) = tr_pre.get_position(
        tracking, epochs_intSet, epochs_df,
        conf_thresh=tracking_settings['confidence_threshold'],
        max_gap_secs=tracking_settings['max_gap_secs'],
        merge_method=tracking_settings['position_merge_method']
    )
    head_direction.name = 'hd_in_rad'
    head_direction_smooth.name = 'hd_in_rad'

    # Create movement dataframe with all relevant variables.
    tr_sample_deltas = position_cm.index.to_series().diff()
    movement_df = tr_pre.create_movement_df(
        position_cm, head_direction, tr_sample_deltas,
        tracking_settings['track_moving_threshold_cmPerSecs'],
        tracking_settings['sleep_moving_threshold_cmPerSecs'])
    movement_smooth_df = tr_pre.create_movement_df(
        position_smooth_cm, head_direction_smooth, tr_sample_deltas,
        tracking_settings['track_moving_threshold_cmPerSecs'],
        tracking_settings['sleep_moving_threshold_cmPerSecs'])

    # Save movement dataframes for easy access.
    movement_df.to_pickle(
        os.path.join(paths['beh_path'], paths['nwb_file_stem']
                     + rec_settings['fname_tracking_df'])
    )
    movement_smooth_df.to_pickle(
        os.path.join(paths['beh_path'], paths['nwb_file_stem']
                     + rec_settings['fname_tracking_smooth_df'])
    )
    print(f"Movement df without linearization saved to {paths['beh_path']}")


def main():
    '''
    Main function to create track maps and trajectory track maps.
    Parses the input arguments and calls the necessary functions.
    '''
    input_args = parse_args()
    build_movement_dfs(input_args.nwb_file, input_args.pc, input_args.proj)
    return


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--nwb_file', type=str, default=None,
        help=['str: Variable to pass to analyze_one_nwb_settings as "nwb_file"'
              + '. Otherwise, defaults to the value in that file.'])
    parser.add_argument(
        '--pc', type=str, default=None,
        help=['str: Variable to pass to analyze_one_nwb_settings as "pc".'
              + 'Otherwise, defaults to the value in that file.'])
    parser.add_argument(
        '--proj', type=str, default=None,
        help=['str: Variable to pass to analyze_one_nwb_settings as "proj". '
              + 'Otherwise, defaults to the value in that file.'])
    input_args = parser.parse_args()
    return input_args


if __name__ == '__main__':
    main()
