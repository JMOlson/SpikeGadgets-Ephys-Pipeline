#!/usr/bin/python
# -*- coding: utf-8 -*-
"""

Using statescript messages to read well visits and rewards
Here as a sanity check on the io events method and DIO methods of
reading well visits and rewards. This module is essentially methods that
should be in the methods of StateScriptRec, but I don't want to add methods
that are specific to my experiment structure to that object. I should probably
make a derived class of StateScriptRec that has these methods.

License:
BSD-3-Clause
"""

# Built-in/Generic Imports

# Libs
import numpy as np
import pandas as pd

# Own modules


def get_visits_from_messages(message_df, rec_settings):
    '''
    Extracts well visits from a DataFrame of StateScript messages.

    Parameters:
    message_df (pandas.DataFrame): A DataFrame containing Statescript messages.

    Returns:
    dict: A dictionary containing the times, line indices, and well IDs of the
    well visits.
    '''
    whole_lines = message_df[0].to_list()
    is_visit_line = [rec_settings['ss_well_visit_message']
                     in i_line for i_line in whole_lines]

    visits = dict()
    visits['times'] = message_df.index[is_visit_line].to_numpy()
    visits['line_ind'] = [ind for ind, val in enumerate(is_visit_line) if val]
    visits['well_id'] = [i_line.split(' ')[-1] for i_line in [
        whole_lines[ind+1] for ind, val in enumerate(is_visit_line) if val]]

    is_well_id_line = get_well_id_lines_from_messages(
        message_df, rec_settings['ss_well_id_message'])
    if not all([is_well_id_line[ind+1]
                for ind, val in enumerate(is_visit_line) if val]):
        raise ValueError('Well ID line not found after visit line')

    return visits


def get_rewards_from_messages(message_df, rec_settings):
    '''
    Extracts reward information from a DataFrame of StateScript messages.

    Args:
        message_df (pandas.DataFrame): A DataFrame containing messages.

    Returns:
        dict: A dictionary with the following keys:
            - 'times': A numpy array with the timestamps of the
            reward messages.
            - 'line_ind': A list with the indices of the reward messages
            in the original DataFrame.
            - 'well_id': A list with the well IDs associated with each reward
            message.

    Raises:
        ValueError: If the well ID line is not found before a reward line.
    '''
    whole_lines = message_df[0].to_list()
    is_reward_line = [rec_settings['ss_reward_message']
                      in i_line for i_line in whole_lines]

    reward = dict()
    reward['times'] = message_df.index[is_reward_line].to_numpy()
    reward['line_ind'] = [ind for ind, val in enumerate(is_reward_line) if val]
    reward['well_id'] = [i_line.split(' ')[-1]
                         for i_line in [whole_lines[ind-1]
                         for ind, val in enumerate(is_reward_line) if val]]

    is_well_id_line = get_well_id_lines_from_messages(
        message_df, rec_settings['ss_well_id_message'])
    if not all([is_well_id_line[ind-1]
                for ind, val in enumerate(is_reward_line) if val]):
        raise ValueError('Well ID line not found before reward line')

    return reward


def get_well_id_lines_from_messages(message_df, well_id_message):
    '''
    Get well ID lines from messages
    '''
    whole_lines = message_df[0].to_list()
    is_well_id_line = [well_id_message in i_line for i_line in whole_lines]
    return is_well_id_line


def get_reward_counts_from_messages(message_df, rec_settings):
    '''
    Get reward counts from well visits and rewards calculated from messages
    '''
    rewards = get_rewards_from_messages(message_df, rec_settings)
    visits = get_visits_from_messages(message_df, rec_settings)
    # Get which well visits are rewarded
    diff_matrix = np.subtract.outer(rewards['line_ind'], visits['line_ind'])
    # Workaround for nans not working in numpy int arrays
    ignoreVal = np.min(diff_matrix)
    diff_matrix[diff_matrix > 0] = ignoreVal

    # find first positive value in each row - this is first well visit
    # documented after the reward, which is the well visit that is rewarded.
    first_pos = np.argmax(diff_matrix, axis=1).tolist()
    if first_pos[-1] == 0:
        # last reward well isn't in the well visit list
        first_pos = first_pos[:-1]
        print('Last reward well not in well visit list')
    visit_is_rewarded = [True if i in first_pos else False
                         for i in range(len(visits['times']))]
    well_ids, rew_counts = np.unique(np.asarray(
        visits['well_id'])[visit_is_rewarded], return_counts=True)
    return rew_counts, well_ids


def get_trajectories_from_events(events_df, home_well_id=None):
    '''
    Extracts trajectories from a pandas DataFrame of behavioral events.

    Parameters:
    -----------
    events_df : pandas.DataFrame
        A DataFrame containing the behavioral events, with columns for
        'EventType', 'Port', 'StartTime_ms', and any other relevant
        information.
    home_well_id : int or None, optional
        The ID of the home well, if any. If provided, the function will
        identify inbound and outbound trajectories based on whether the
        start well is the home well or not. If None, all trajectories are
        considered outbound.

    Returns:
    --------
    pandas.DataFrame
        A DataFrame with columns for 'start wells', 'end wells', 'indices',
        'times', 'is_rewarded', 'is_inbound', and 'is_outbound', containing
        information about each trajectory extracted from the events.
    '''
    port_change = np.append(True, events_df.Port[:-1].values
                            != events_df.Port[1:].values)
    # prepend first port value to port_change
    current_port = events_df.Port[port_change]

    reward_well_ind = events_df.Port[events_df['EventType'] == 'output'].index

    traj = dict()
    start_wells = pd.concat([pd.Series(0), current_port[:-1]],
                            ignore_index=True)
    start_wells.reset_index(drop=True, inplace=True)
    current_port.reset_index(drop=True, inplace=True)
    traj['start wells'] = start_wells
    traj['end wells'] = current_port
    traj_start = [start_ind for start_ind in np.nonzero(port_change)[0]]
    # Include all the way to the last index
    # the animal may still be in the port
    traj_end = [end_ind for end_ind in np.nonzero(port_change)[0][1:]]
    traj_end.append(len(events_df)-1)
    traj['indices'] = list(zip(traj_start, traj_end))
    traj['times'] = [(start_time, end_time) for start_time, end_time in
                     zip(events_df.StartTime_ms.iloc[traj_start],
                         events_df.StartTime_ms.iloc[traj_end])]
    # sum shows if any double triggers, +1 to include end_ind
    traj['is_rewarded'] = [any(reward_well_ind.isin(
                           np.arange(start_ind, end_ind+1)))
                           for start_ind, end_ind in traj['indices']]
    traj['is_inbound'] = [True if start_well != home_well_id else False
                          for start_well in start_wells]
    traj['is_outbound'] = [True if start_well == home_well_id else False
                           for start_well in start_wells]
    # Turn trajectories dict into a dataframe
    trajectories_df = pd.DataFrame.from_dict(traj)

    return trajectories_df
