#! /usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
import os
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pycircstat2 as circ
import pynapple as nap
import scipy as sp
import track_linearization as track_lin
from mpl_point_clicker import clicker
from replay_trajectory_classification import (make_track_graph,
                                              plot_graph_as_1D,
                                              plot_track_graph)


def fix_tracking_timestamp_duplicates(df_tracking, show_plot=False):
    '''
    For some reason, my tracking data has repeating timestamps (with changing
    tracking data). The gap after the repeated timestamps that are repeated
    are the correct length to interpolate the times, and the tracking data
    appears to be correct (i.e. the rat is not teleporting). This function
    interpolates the timestamps over the gap to remove the duplicates.
    The show_plot option provides rudimentary plots to assess whether the gaps
    and data do seem to be correct.
    '''
    is_duplicated_timestamp = df_tracking.index.duplicated()
    fixed_index = df_tracking.index.values.copy()
    ts_index = np.cumsum(np.ones(len(fixed_index)))
    fixed_index[is_duplicated_timestamp] = sp.interpolate.pchip_interpolate(
        ts_index[~is_duplicated_timestamp],
        fixed_index[~is_duplicated_timestamp],
        ts_index[is_duplicated_timestamp]
    )
    df_tracking.set_index(fixed_index, inplace=True, drop=True)

    if show_plot:
        dup_edge = ts_index[np.diff(
            is_duplicated_timestamp, prepend=False)].astype(int)
        window = 30  # 30 frames ~= 1 second

        h_a = plt.figure()
        h_b = plt.figure()
        h_c = plt.figure()
        h_d = plt.figure()
        for i_gap, gap in enumerate(dup_edge):
            print(i_gap, gap)
            ts_fixed_win = fixed_index[
                gap-window:gap+window]
            n_edge = len(dup_edge)
            # plot to subplot i_gap of figure h_a
            plt.figure(h_a)
            plt.subplot(n_edge, 1, i_gap+1)
            plt.plot(ts_fixed_win)
            plt.figure(h_b)
            plt.subplot(n_edge, 1, i_gap+1)
            plt.plot(ts_fixed_win, df_tracking.iloc[gap-window:gap+window, 0])
            plt.figure(h_c)
            plt.subplot(n_edge, 1, i_gap+1)
            plt.plot(ts_fixed_win, df_tracking.iloc[gap-window:gap+window, 1])
            plt.figure(h_d)
            plt.subplot(n_edge, 1, i_gap+1)
            plt.scatter(df_tracking.iloc[gap-window:gap+window, 0],
                        df_tracking.iloc[gap-window:gap+window, 1],
                        c=ts_fixed_win)
        plt.show()
    return df_tracking


def interpolate_tracking_gaps(pos_cm, conf,
                              conf_thresh=0.95, max_gap_secs=0.5):
    '''

    '''
    timestamps = pos_cm.index.values

    # Filter position data
    is_good_tracking = (conf >= conf_thresh)
    pos_cm[~is_good_tracking] = np.nan

    interp_fn = sp.interpolate.PchipInterpolator(
        timestamps[is_good_tracking], pos_cm.loc[is_good_tracking, :])

    # get edges of sample gaps
    gap_edges = np.diff(is_good_tracking.values.astype(int),
                        prepend=is_good_tracking.values[0].astype(int))
    gap_start = np.where(gap_edges == -1)[0]
    gap_end = np.where(gap_edges == 1)[0]
    if gap_end[0] < gap_start[0]:
        gap_end = gap_end[1:]
    if gap_start[-1] > gap_end[-1]:
        gap_start = gap_start[:-1]

    for i_gap in range(len(gap_start)):
        gap = timestamps[gap_start[i_gap]:gap_end[i_gap]]
        gap_length = gap[-1] - gap[0]
        if gap_length <= max_gap_secs:
            # Fill in gaps
            pos_cm.loc[gap, :] = interp_fn(gap)
    return pos_cm


def resample_movement_1D(movement, sample_times, max_gap_secs=0.5):
    '''
    Resample 1D movement data to a new set of sample times. Gaps in the
    movement data are interpolated over, and if the gap is too long, the
    samples are set to NaN.

    Inputs:
        movement: pandas series with timestamps as index and
                    1D movement data as columns - 
                    or a Tsd or TsdFrame with the same structure
        sample_times: numpy array of timestamps to resample the movement data to
        max_gap_secs: maximum gap length to interpolate over in seconds

    Outputs:
        new_move: pandas series with timestamps as index and
                    1D movement data as columns
    '''
    if isinstance(movement, nap.TsdFrame):
        movement = movement.as_dataframe()
    elif isinstance(movement, nap.Tsd):
        movement = movement.as_series()
    movement = movement.dropna()

    timestamps_current = movement.index.values
    new_move = sp.interpolate.pchip_interpolate(
        timestamps_current, movement, sample_times, axis=0)

    # add the min of sample_times to the beginning of timestamps_current
    mod_timestamps = np.append(min(sample_times), timestamps_current)
    gap_sizes = np.diff(mod_timestamps)
    gap_starts = np.where(gap_sizes > max_gap_secs)[0]
    gap_ends = gap_starts+1

    gaps = nap.IntervalSet(mod_timestamps[gap_starts],
                           mod_timestamps[gap_ends])
    in_gaps = ~np.isnan(gaps.in_interval(nap.Ts(sample_times)))
    new_move[in_gaps] = np.nan

    return pd.DataFrame(new_move, index=sample_times, columns=movement.columns)


def upsample_movement_1D(movement, bin_size, epoch=None, max_gap_secs=0.5):
    '''
    Upsample 1D movement data to a new bin size. Gaps in the movement data are
    interpolated over, and if the gap is too long, the samples are set to NaN.
    It's just a wrapper to resample_movement_1D.

        Inputs:
            movement: pandas series with timestamps as index and
                        1D movement data as columns
            bin_size: new bin size in seconds
            epoch: nap.Ts or nap.IntervalSet to restrict the movement data to
                    before resampling. If None, the entire movement data is
                    used.
            max_gap_secs: maximum gap length to interpolate over in seconds

        Outputs:
            new_move: pandas series with timestamps as index and
                        1D movement data as columns
    '''
    if epoch is None:
        epoch = nap.IntervalSet(start=movement.index.values[0],
                                end=movement.index.values[-1])
    # create array size of total time using new bin size
    n_Bins = round(epoch.tot_length() / bin_size) - 1
    # fill in timestamps from start to stop of epoch
    sample_times = np.linspace(epoch.start[0]+(bin_size/2),
                               epoch.end[0]-(bin_size/2), num=n_Bins)
    return resample_movement_1D(movement, sample_times, max_gap_secs)


def get_position(tr, int_set, epochs_df, conf_thresh=0.95, max_gap_secs=0.5,
                 merge_method='2 light mean - either', smoothing_sigma=0.1):
    '''
    Tracking data is in unit pixels from DLC, and needs to be converted to
    cm. The tracking data is interpolated over gaps in tracking data, and
    merged if there are multiple lights. The position and head direction,
    along with smoothed versions of these, are returned
    as a tuple of pandas dataframes.
    '''
    position = []
    head_direction = []
    position_smooth = []
    head_direction_smooth = []

    for i_sess_ind in int_set.index:
        i_sess = int_set.loc[[i_sess_ind]]

        x_list = []
        y_list = []
        this_tracking_list = epochs_df['led_list'][i_sess_ind]
        this_scale_in_cm = (epochs_df['camera_meters_per_pixel'][i_sess_ind]
                            * 100)  # cm per pixel
        for tracked_point in this_tracking_list:
            # Get tracking point data
            tr_tsdf = nap.TsdFrame(tr[tracked_point]).restrict(i_sess)
            i_pos_cm = (tr_tsdf['xs', 'ys'] * this_scale_in_cm).as_dataframe()
            conf = tr_tsdf['confidence'].as_series()
            i_pos_cm = interpolate_tracking_gaps(i_pos_cm, conf,
                                                 conf_thresh, max_gap_secs)
            x_list.append(i_pos_cm['xs'])
            y_list.append(i_pos_cm['ys'])
        xs = pd.concat(x_list, axis=1)
        ys = pd.concat(y_list, axis=1)

        pos_sig_n_samples = round(
            smoothing_sigma / np.median(np.diff(xs.index.values)))
        xs_smooth = pd.DataFrame(sp.ndimage.gaussian_filter1d(
            xs, sigma=pos_sig_n_samples, axis=0), index=xs.index.values)
        ys_smooth = pd.DataFrame(sp.ndimage.gaussian_filter1d(
            ys, sigma=pos_sig_n_samples, axis=0), index=ys.index.values)

        if xs.shape[1] == 1:  # only one light
            position_ep = pd.concat([xs.mean(axis=1, skipna=False),
                                     ys.mean(axis=1, skipna=False)],
                                    axis='columns')
            position_ep.columns = ['x', 'y']
            pos_smooth_ep = pd.concat([xs_smooth.mean(axis=1, skipna=False),
                                       ys_smooth.mean(axis=1, skipna=False)],
                                      axis='columns')
            pos_smooth_ep.columns = ['x', 'y']
            # fill with nans, all samples
            head_direction_ep = xs.squeeze() * np.nan
            head_direction_ep.columns = 'hd_in_rad'
            head_direction_smooth_ep = head_direction_ep
            head_direction_smooth_ep.columns = 'hd_in_rad'
        elif merge_method == '2 light mean - either':
            position_ep = pd.concat([xs.mean(axis=1, skipna=False),
                                     ys.mean(axis=1, skipna=False)],
                                    axis='columns')
            position_ep.columns = ['x', 'y']
            pos_smooth_ep = pd.concat([xs_smooth.mean(axis=1, skipna=False),
                                       ys_smooth.mean(axis=1, skipna=False)],
                                      axis='columns')
            pos_smooth_ep.columns = ['x', 'y']
            head_direction_ep = np.arctan2(ys.iloc[:, 1] - ys.iloc[:, 0],
                                           xs.iloc[:, 1] - xs.iloc[:, 0])
            head_direction_ep.columns = 'hd_in_rad'
            head_direction_smooth_ep = (
                np.arctan2(ys_smooth.iloc[:, 1] - ys_smooth.iloc[:, 0],
                           xs_smooth.iloc[:, 1] - xs_smooth.iloc[:, 0]))
            head_direction_smooth_ep.columns = 'hd_in_rad'
        elif merge_method == '2 light mean - both':
            position_ep = pd.concat([xs.mean(axis=1, skipna=True),
                                     ys.mean(axis=1, skipna=True)],
                                    axis='columns')
            position_ep.columns = ['x', 'y']
            pos_smooth_ep = pd.concat([xs_smooth.mean(axis=1, skipna=True),
                                       ys_smooth.mean(axis=1, skipna=True)],
                                      axis='columns')
            pos_smooth_ep.columns = ['x', 'y']
            head_direction_ep = np.arctan2(ys.iloc[:, 1] - ys.iloc[:, 0],
                                           xs.iloc[:, 1] - xs.iloc[:, 0])
            head_direction_ep.columns = 'hd_in_rad'
            head_direction_smooth_ep = (
                np.arctan2(ys_smooth.iloc[:, 1] - ys_smooth.iloc[:, 0],
                           xs_smooth.iloc[:, 1] - xs_smooth.iloc[:, 0]))
            head_direction_smooth_ep.columns = 'hd_in_rad'
        else:
            raise ValueError(
                'Position_merge_method invalid or not implemented')

        position.append(position_ep)
        position_smooth.append(pos_smooth_ep)
        head_direction.append(head_direction_ep)
        head_direction_smooth.append(head_direction_smooth_ep)
    return (pd.concat(position, axis=0),
            pd.concat(head_direction, axis=0),
            pd.concat(position_smooth, axis=0),
            pd.concat(head_direction_smooth, axis=0)
            )


def get_ang_vel(hd, tr_sample_deltas):
    '''
    Takes a hd array and calculates change in direction between each point
    dividing by the time delta between points to calc angular velocity.
    '''
    angvel_rps = np.empty_like(tr_sample_deltas)
    angvel_rps[0] = np.nan
    for i_sample in range(len(hd)-1):
        c = circ.utils.angrange(hd.iloc[i_sample]
                                - hd.iloc[i_sample+1])
        d = 2 * np.pi - c
        e = np.argmin([c, d], axis=0)
        f = np.min([c, d], axis=0)
        if e == 0:
            f *= -1
        angvel_rps[i_sample+1] = f / tr_sample_deltas.iloc[i_sample+1]
    return pd.Series(angvel_rps, index=hd.index)


def create_movement_df(position_cm, hd, tr_sample_deltas,
                       speed_thresh, speed_thresh_sleep):
    '''

    '''
    xy_vel = position_cm.diff().div(tr_sample_deltas, axis=0)
    vel_cmps = np.sqrt(xy_vel['x']**2 + xy_vel['y']**2)
    is_moving = vel_cmps > speed_thresh
    is_moving_sleep = vel_cmps > speed_thresh_sleep
    heading = np.arctan2(xy_vel['y'], xy_vel['x'])

    xy_acc = xy_vel.diff().div(tr_sample_deltas, axis=0)
    acc_cmps2 = np.sqrt(xy_acc['x']**2 + xy_acc['y']**2)
    acc_heading = np.arctan2(xy_acc['y'], xy_acc['x'])

    angvel_rps = get_ang_vel(hd, tr_sample_deltas)

    movement_df = pd.concat([position_cm, hd, vel_cmps, heading, acc_cmps2,
                             acc_heading, angvel_rps,
                             is_moving, is_moving_sleep],
                            names=['in_cm_per_s'],
                            axis=1)
    movement_df.columns = ['x', 'y', 'hd', 'vel', 'heading', 'acc',
                           'acc_heading', 'angvel', 'is_moving',
                           'is_moving_sleep']
    return movement_df


def create_track_map_socialW(epochs_df, tracking_file, save_file, edges=None,
                             edge_spacing=None, overwrite=False,
                             save_to_file=True):
    if edges is None:
        print('No edges provided, try again.')
        return
    if edge_spacing is None:
        print('No edge spacing provided, try again.')
        return

    # Check that conditions are properly met:
    all_good_flag = (not save_to_file or overwrite or
                     not os.path.exists(save_file))
    if not all_good_flag:
        # Error: File exists
        print('File exists, set overwrite=True to overwrite or ' +
              'save_to_file=False')
        return
    movement_df = pd.read_pickle(tracking_file)
    mv_no_nan_df = movement_df.dropna()
    pos_tsdf = nap.TsdFrame(mv_no_nan_df[['x', 'y']])

    for ep in range(2):
        ep_id = epochs_df.sesID[ep]
        ep_save_file = save_file.replace("graph", "graph_" + ep_id)

        ep_start = epochs_df.start_time[ep]
        ep_stop = epochs_df.stop_time[ep]

        ep_pos_tsdf = pos_tsdf[(pos_tsdf.index > ep_start)
                               & (pos_tsdf.index < ep_stop)]

        # Label the track nodes on a plot of the tracking data
        ep_track_nodes = np.asarray(label_track_map(ep_pos_tsdf)['node'])
        print(ep_track_nodes)
        # Create the track graph
        ep_track_graph = make_track_graph(ep_track_nodes, edges)

        if not save_to_file:
            return ep_track_graph, edges, edge_spacing
        elif overwrite or not os.path.exists(ep_save_file):
            # Save the track graph
            with open(ep_save_file, 'wb') as f:
                pickle.dump([ep_track_graph, edges, edge_spacing], f)
    return


def make_traj_list(well_list):
    '''
    Make a list of unique trajectory IDs
    '''
    # make a list of all combos in well_list
    traj_list = list(itertools.combinations(well_list, 2))
    traj_list.extend([(i[1], i[0]) for i in traj_list])
    # add the homewell to each combo
    return traj_list


def create_traj_track_maps(traj_df, traj_list, tracking_file, save_file,
                           overwrite=False, save_to_file=True):
    '''
    We are looping thru the trajectories, making graphs
    edges and edge spacing should be list of lists.
    '''
    all_good_flag = (not save_to_file or overwrite or
                     not os.path.exists(save_file))
    if not all_good_flag:
        # Error: File exists
        print('File exists, set overwrite=True to overwrite or ' +
              'save_to_file=False')
        with open(save_file, 'rb') as f:
            track_graphs_df = pickle.load(f)
        return track_graphs_df

    traj_all_ind = 0
    edges = []
    track_graph = []
    traj_start = []
    traj_end = []
    path_ind_all = []
    movement_df = pd.read_pickle(tracking_file)
    mv_no_nan_df = movement_df.dropna()
    pos_tsdf = nap.TsdFrame(mv_no_nan_df[['x', 'y']])
    # loop thru edges, edge spacing and correct tracking subset.
    # call create_track_map
    for i_traj in traj_list:
        # get the subset of the tracking data
        this_traj_df = traj_df[(traj_df['start_well'] == i_traj[0]) &
                               (traj_df['end_well'] == i_traj[1])]
        traj_IntSet = nap.IntervalSet(start=this_traj_df['start_time'],
                                      end=this_traj_df['stop_time'])

        path_ind = 0
        while True:
            # Label the track nodes on a plot of the tracking data
            track_nodes = np.asarray(
                label_track_map(pos_tsdf.restrict(traj_IntSet),
                                traj_id=i_traj,
                                full_movement_df=pos_tsdf,
                                path_ind=path_ind)['node'],
            )
            if track_nodes.size == 0:
                break
            edges.append([[node, node+1]
                         for node in range(len(track_nodes)-1)])
            traj_start.append(i_traj[0])
            traj_end.append(i_traj[1])
            path_ind_all.append(path_ind)
            # Create the track graph
            track_graph.append(make_track_graph(
                track_nodes, edges[traj_all_ind]))

            path_ind += 1
            traj_all_ind += 1

    track_graphs_df = pd.DataFrame(
        {'traj_start': traj_start,
         'traj_end': traj_end,
         'route_ind': path_ind_all,
         'track_graph': track_graph,
         'edges': edges
         }
    )

    if not save_to_file:
        return track_graphs_df
    elif overwrite or not os.path.exists(save_file):
        # Save the track graph trajs
        with open(save_file, 'wb') as f:
            pickle.dump(track_graphs_df, f)
    return track_graphs_df


def map_traj_to_templates(traj_df, track_graph_traj_df, tracking_file):
    '''
    Sometimes there are multiple routes between two wells. This function takes
    the tracking data and maps the trajectories to the correct template.
    '''
    # maximum root mean sq. error to be considered a match - in cm
    # ERROR_MAX_THRESH = 20

    movement_df = pd.read_pickle(tracking_file)
    mv_no_nan_df = movement_df.dropna()
    pos_tsdf = nap.TsdFrame(mv_no_nan_df[['x', 'y']])

    # Get all unique combinations of start and end wells (trajectories)
    traj_types = [[i, j] for i in np.unique(traj_df['start_well'])
                  for j in np.unique(traj_df['end_well']) if i != j]

    traj_template = -1*np.ones((len(traj_df)), dtype=int)
    route_ind = -1*np.ones((len(traj_df)), dtype=int)
    error_val = np.nan*np.ones((len(traj_df)))
    # Loop through possible trajectories
    for i_traj_type in traj_types:
        # Grab the trajectories that match the type
        is_start = [i == i_traj_type[0] for i in traj_df['start_well']]
        is_end = [i == i_traj_type[1] for i in traj_df['end_well']]
        is_traj = [st and end for st, end in zip(is_start, is_end)]
        this_type_traj_df = traj_df[is_traj]
        traj_intsets = nap.IntervalSet(
            start=this_type_traj_df['start_time'],
            end=this_type_traj_df['stop_time'])

        # Grab the graph trajectories that match the type
        is_start = [i == i_traj_type[0]
                    for i in track_graph_traj_df['traj_start']]
        is_end = [i == i_traj_type[1] for i in track_graph_traj_df['traj_end']]
        is_traj = [st and end for st, end in zip(is_start, is_end)]
        graph_trajs = track_graph_traj_df[is_traj]
        # loop thru each traj template.
        rmse = np.zeros((len(this_type_traj_df), len(graph_trajs)))
        for ind_gr_traj, i_gr_traj in enumerate(graph_trajs.index):
            # project onto each template, get error from projection.
            this_pos_df = pos_tsdf.restrict(traj_intsets).as_dataframe()
            lin_pos_df = get_linearized_position(
                this_pos_df,
                graph_trajs.at[i_gr_traj, 'track_graph'],
                graph_trajs.at[i_gr_traj, 'edges'])
            this_pos_df = pd.concat([this_pos_df, lin_pos_df], axis=1)
            for ind_traj, i_traj in enumerate(this_type_traj_df.index):
                start = this_type_traj_df.at[i_traj, 'start_time']
                stop = this_type_traj_df.at[i_traj, 'stop_time']
                this_traj = this_pos_df.loc[start:stop]
                rmse[ind_traj, ind_gr_traj] = np.sqrt(np.nanmean(
                    (this_traj['x'] - this_traj['projected_x_position'])**2
                    + (this_traj['y'] - this_traj['projected_y_position'])**2))
            # sum error for each template, take min as correct template.
            # AND only take if below a max value
            traj_temp = graph_trajs.index.values[np.argmin(rmse, axis=1)]
            error_val[this_type_traj_df.index] = np.min(rmse, axis=1)
            traj_template[this_type_traj_df.index] = traj_temp
            route_ind[this_type_traj_df.index] = (
                graph_trajs.loc[traj_temp, 'route_ind'])

    traj_df['traj_template_ind'] = traj_template
    traj_df['traj_route_ind'] = route_ind
    traj_df['traj_rmse_cm'] = error_val
    return traj_df


def get_track_map(file, plot=False):
    # load graph object from file
    with open(file, 'rb') as f:
        [track_graph, edges, edge_spacing, env_tracked] = pickle.load(f)

    if plot:
        if isinstance(track_graph, list):
            for ind, i_tr_gr in enumerate(track_graph):
                # plot the track graph
                fig, ax = plt.subplots(figsize=(10, 10))
                plot_track_graph(i_tr_gr, ax=ax, draw_edge_labels=True)
                ax.tick_params(left=True, bottom=True,
                               labelleft=True, labelbottom=True)
                ax.set_xlabel("X-Position")
                ax.set_ylabel("Y-Position")
                ax.set_title(env_tracked[ind])
                ax.spines["top"].set_visible(False)
                ax.spines["right"].set_visible(False)
                ax.invert_yaxis()
                # Linearized track plot
                fig, ax = plt.subplots(figsize=(10, 1))
                if len(edges) == len(track_graph):
                    plot_graph_as_1D(i_tr_gr, edges[ind], edge_spacing[ind],
                                     ax=ax)
                else:
                    plot_graph_as_1D(i_tr_gr, edges, edge_spacing, ax=ax)
                plt.show()
        else:
            fig, ax = plt.subplots(figsize=(10, 10))
            plot_track_graph(track_graph, ax=ax, draw_edge_labels=True)
            ax.tick_params(left=True, bottom=True,
                           labelleft=True, labelbottom=True)
            ax.set_xlabel("X-Position")
            ax.set_ylabel("Y-Position")
            ax.set_title(env_tracked)
            ax.spines["top"].set_visible(False)
            ax.spines["right"].set_visible(False)
            ax.invert_yaxis()
            # Linearized track plot
            fig, ax = plt.subplots(figsize=(10, 1))
            plot_graph_as_1D(track_graph, edges, edge_spacing, ax=ax)
            plt.show()

    return track_graph, edges, edge_spacing, env_tracked


def get_linearized_position(pos_xy_df, track_graph, edge_order,
                            edge_spacing=0, use_HMM=False):

    lin_pos_df = track_lin.core.get_linearized_position(
        pos_xy_df.values, track_graph,
        edge_order=edge_order,
        edge_spacing=edge_spacing, use_HMM=use_HMM)
    lin_pos_df.index = pos_xy_df.index
    return lin_pos_df


def get_segment_positions(track_graph, edges, edge_spacing):
    start_node_linear_position = [0.0]
    end_node_linear_position = []
    for ind, edge in enumerate(edges):
        end_node_linear_position.append(
            start_node_linear_position[ind] +
            track_graph.edges[edge]["distance"]
        )
        try:
            start_node_linear_position.append(
                start_node_linear_position[ind] +
                track_graph.edges[edge]["distance"] + edge_spacing[ind]
            )
        except IndexError:
            pass
    edge_lengths = [node_end - node_st for node_st, node_end
                    in zip(start_node_linear_position,
                           end_node_linear_position)]

    # Add results into dataframe
    tr_segs_df = pd.DataFrame(list(zip(start_node_linear_position,
                                       end_node_linear_position,
                                       edge_lengths)),
                              columns=['start_node_linear_position',
                                       'end_node_linear_position',
                                       'edge_lengths'])
    return tr_segs_df

# class TrackingData:
#     def __init__(self, bodyparts=None, xs=None, ys=None, confidence=None,
#                  settings=None):
#         self.bodyparts = bodyparts
#         self.xs = xs
#         self.ys = ys
#         self.confidence = confidence

#         if settings is None:
#             code_path = os.path.abspath(__file__)
#             code_dir = os.path.split(code_path)[0]
#             settings_file = os.path.join(code_dir, 'SLDefaultSettings.yml')
#             self.import_settings(settings_file)
#         # Check if settings is a string (file name) or a dict
#         elif isinstance(settings, str):
#             # check if is a file name or a path
#             if os.path.isfile(settings):
#                 self.import_settings(settings)
#             else:
#                 code_path = os.path.abspath(__file__)
#                 code_dir = os.path.split(code_path)[0]
#                 settings_file = os.path.join(code_dir, settings)
#                 self.import_settings(settings_file)
#         elif isinstance(settings, dict):
#             self.settings = settings
#         else:
#             raise ValueError('settings must be a file or a dict')

#     def import_settings(self, settings_file):
#         with open(settings_file, 'r') as stream:
#             self.settings = yaml.safe_load(stream)
#         return
