#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys

import argparse
import matplotlib.pyplot as plt
import pandas as pd
import pynapple as nap
import numpy as np
import pickle
import track_linearization as track_lin
from mpl_point_clicker import clicker
from replay_trajectory_classification import (make_track_graph,
                                              plot_graph_as_1D,
                                              plot_track_graph)
# Own modules
import analyze_one_nwb_settings.import_settings as analyze1_settings
import Beh.Tracking.tracking_preprocessor as tr_pre
import beh_core_settings.import_settings as beh_settings_importer
from jlab_nwb import JLabNWBHandler
import Utils.epoch_tools as ep_tools


def map_tracks(do_traj_track_maps=False, nwb_file=None, pc=None, proj=None,
               supp_nwbs=[], plot=True):
    # Settings Import
    # All hard coded values are loaded or defined here
    paths, rec_settings = analyze1_settings.init_analysis(nwb_file, pc, proj)
    tr_map_settings = beh_settings_importer.import_track_map_settings()

    save_file = os.path.join(paths['beh_path'], paths['nwb_file_stem']
                             + rec_settings['fname_track_graph'])
    save_file_traj = os.path.join(paths['beh_path'], paths['nwb_file_stem']
                                  + rec_settings['fname_track_graph_trajs_df'])

    if 'is_social' in rec_settings and rec_settings['is_social']:
        supp_nwbs = [paths['sister_nwb_file']]

    # Get the environment-specific position time series dataframes
    pos_tsdfs = get_pos_tsdfs(nwb_file, supp_nwbs, pc, proj)

    # Create default full track map
    track_graphs, edges, edge_spacing, envs = create_full_track_map(
        pos_tsdfs=pos_tsdfs, save_file=save_file,
        edges=tr_map_settings['edges'],
        edge_spacing=tr_map_settings['edge_spacing'],
        overwrite=tr_map_settings['overwrite'])
    for nwb in supp_nwbs:
        add_maps_to_partners(save_file, nwb, pc, proj)

    # Plot the track maps
    if plot:
        tr_pre.get_track_map(file=save_file, plot=True)

    # Create trajectory track maps - BROKEN CURRENTLY
    if do_traj_track_maps:
        if 'is_social' in rec_settings and rec_settings['is_social']:
            ValueError('Social tracking not yet implemented for trajectory'
                       + 'track maps.')
        # Load the trajectory df
        traj_file = os.path.join(paths['beh_path'], paths['nwb_file_stem']
                                 + rec_settings['fname_trajectories_df'])
        if not os.path.exists(traj_file):
            ValueError('Trajectory dataframe not found at {0} .'.format(
                traj_file) + ' Please run build_basic_beh_dfs.py first.')
        traj_df = pd.read_pickle(traj_file)
        well_list = pd.unique(traj_df['start_well'].values)
        well_list.sort()
        traj_list = tr_pre.make_traj_list(well_list)
        track_graphs_df = tr_pre.create_traj_track_maps(
            traj_df=traj_df, traj_list=traj_list,
            tracking_file=pos_tsdfs, save_file=save_file_traj)
        # Update trajectory df with track graph information
        # Compare each trajectory to the barrier and short version - use the
        # linearization with the least error.
        traj_df = tr_pre.map_traj_to_templates(traj_df, track_graphs_df,
                                               tracking_file=pos_tsdfs)
        traj_df.to_pickle(traj_file)
        print('Trajectory track map creation complete. Trajectory dataframe'
              + ' updated and saved to %s' % traj_file)

    # Add linearized position to the movement dataframe
    if do_traj_track_maps:
        one_pos_tsdf = get_one_rec_pos_tsdfs(nwb_file, pc, proj)
        add_linearized_position(paths, rec_settings, one_pos_tsdf, traj_df)
        for nwb in supp_nwbs:
            paths_supp, rec_settings_supp = (
                analyze1_settings.init_analysis(nwb_file, pc, proj))
            one_pos_tsdf = get_one_rec_pos_tsdfs(nwb, pc, proj)
            add_linearized_position(paths_supp, rec_settings_supp,
                                    pos_tsdf=one_pos_tsdf)
    else:
        one_pos_tsdf = get_one_rec_pos_tsdfs(nwb_file, pc, proj)
        add_linearized_position(paths, rec_settings,
                                pos_tsdf=one_pos_tsdf)
        for nwb in supp_nwbs:
            paths_supp, rec_settings_supp = (
                analyze1_settings.init_analysis(nwb, pc, proj))
            one_pos_tsdf = get_one_rec_pos_tsdfs(nwb, pc, proj)
            add_linearized_position(paths_supp, rec_settings_supp,
                                    pos_tsdf=one_pos_tsdf)
    print('Map creation complete.')


def _overwrite_check(save_file, overwrite, save_to_file):
    # Check that conditions are properly met:
    all_good_flag = (not save_to_file or overwrite or
                     not os.path.exists(save_file))
    if not all_good_flag:
        # Error: File exists
        print('{0} exists, set '.format(save_file)
              + 'overwrite=True to overwrite or '
              + 'save_to_file=False')
        return False
    return True


def create_full_track_map(pos_tsdfs, edges, edge_spacing, save_file,
                          overwrite=False, save_to_file=True, plot=True):
    '''
    Can handle multiple tracking files, but only one set of edges and edge
    spacing.
    '''
    is_good = _overwrite_check(save_file, overwrite, save_to_file)
    if not is_good:
        return tr_pre.get_track_map(file=save_file)

    track_graphs = []
    env_tracked = []
    for this_env, this_pos_list in pos_tsdfs.items():
        # _overwrite_check(save_file.replace(
        #     'graph', 'graph_' + this_env), overwrite, save_to_file)
        # Label the track nodes on a plot of the tracking data
        track_nodes = np.asarray(label_track_map(this_pos_list)['node'])
        if len(track_nodes) > 0:
            # Create the track graph
            track_graphs.append(make_track_graph(track_nodes, edges))
            env_tracked.append(this_env)

    if not save_to_file:
        pass
    elif overwrite or not os.path.exists(save_file):
        # Save a track graph file for each environment with the env name
        # appended. If only one environment, save the track graph as is.
        if len(track_graphs) == 1:
            track_graph = track_graphs[0]  # unnest from list
            env_tracked = env_tracked[0]
            with open(save_file, 'wb') as f:
                pickle.dump([track_graph, edges, edge_spacing, env_tracked], f)
        else:
            with open(save_file, 'wb') as f:
                pickle.dump(
                    [track_graphs, edges, edge_spacing, env_tracked], f)
            # for i, i_graph in enumerate(track_graphs):
            #     with open(save_file.replace(
            #             'graph', 'graph_' + env_tracked[i]), 'wb') as f:
            #         pickle.dump([i_graph, edges, edge_spacing], f)
    return track_graphs, edges, edge_spacing, env_tracked


def get_pos_tsdfs(nwb_file, supp_nwbs, pc=None, proj=None):
    '''
    Return a dictionary of position time series dataframes segmented by
    environment.
        all_pos_tsdfs - dictionary of all pos_tsdfs - not nested by nwb
            keys: environment names
            values: lists of position time series dataframes for values.
    '''
    pos_tsdfs = [get_one_rec_pos_tsdfs(nwb_file, pc, proj)]
    for i_supp_nwb in supp_nwbs:
        pos_tsdfs.append(get_one_rec_pos_tsdfs(i_supp_nwb, pc, proj))
    return merge_one_rec_pos_tsdfs(pos_tsdfs)


def get_one_rec_pos_tsdfs(nwb_file, pc=None, proj=None):
    '''
    Load the tracking data of one nwb and returns a dictionary of
    position time series dataframes segmented by environment.
    '''
    paths, rec_settings = analyze1_settings.init_analysis(nwb_file, pc, proj)
    ep_file = os.path.join(paths['nwb_path'], paths['nwb_file_stem']
                           + rec_settings['fname_base_epochs'])
    epochs_intSet, epochs_df = ep_tools.load_base_epochs(ep_file)

    envs = np.unique(epochs_df['env_name'])
    pos_tsdfs = dict()
    for i_env in envs:
        env_eps = epochs_df[epochs_df['env_name'] == i_env].index

        # Access the tracking data
        tracking_file_to_use = rec_settings['fname_tracking_default']
        tracking_file = os.path.join(paths['beh_path'], paths['nwb_file_stem']
                                     + rec_settings[tracking_file_to_use])
        if not os.path.exists(tracking_file):
            ValueError('Tracking file not found at {0} .'.format(tracking_file)
                       + 'Please run build_movement_df.py first.')

        movement_df = pd.read_pickle(tracking_file)
        pos_no_nan_df = movement_df[['x', 'y']].dropna()
        pos_tsdf = nap.TsdFrame(pos_no_nan_df,
                                time_support=epochs_intSet[env_eps.values])
        pos_tsdfs[i_env] = pos_tsdf
    return pos_tsdfs


def merge_one_rec_pos_tsdfs(pos_tsdfs):
    '''
    Returns a dictionary with reorganized pos info.
    Args:
        pos_tsdfs - list of single nwb pos_tsdf_dicts
            keys: environment names
            values: position time series dataframes for values.
    Returns:
        all_pos_tsdfs - dictionary of all pos_tsdfs - not nested by nwb
            keys: environment names
            values: lists of position time series dataframes for values.
    '''
    env_pos_tsdfs = dict()
    for i_nwb in pos_tsdfs:
        for i_env, i_pos in i_nwb.items():
            if i_env in env_pos_tsdfs:
                env_pos_tsdfs[i_env].append(i_pos)
            else:
                env_pos_tsdfs[i_env] = [i_pos]

    return env_pos_tsdfs


def label_track_map(movement_df, traj_id=None, full_movement_df=None,
                    path_ind=None):
    '''
    Wrapper for the clicker class to label corners on a track map
    DOES NOT WORK IN JUPYTER NOTEBOOK OR DEBUGGER
    Supports taking in lists of movement dataframes for multi-animal tracking
    onto the same track map and plots each in different shades
    '''
    fig, ax = plt.subplots(figsize=(16, 10), constrained_layout=True)
    colors = ['purple', 'green', 'orange', 'blue', 'cyan', 'magenta']

    if full_movement_df is not None:
        if isinstance(full_movement_df, list):
            for ind, i_df in enumerate(full_movement_df):
                ax.scatter(i_df["x"], i_df["y"], color=colors[ind], marker='.')
        else:
            ax.scatter(full_movement_df["x"], full_movement_df["y"],
                       color='black', marker='.')
    ax.invert_yaxis()
    if isinstance(movement_df, list):
        for ind, i_df in enumerate(movement_df):
            ax.scatter(i_df["x"], i_df["y"], color=colors[ind], marker='.')
    else:
        ax.scatter(movement_df["x"], movement_df["y"],
                   color=colors[0], marker='.')
    if traj_id is not None:
        ax.set_title(f"Trajectory {traj_id} This traj counter: {path_ind}")
    klicker = clicker(ax, ["node"])
    plt.show()
    return klicker.get_positions()


def add_linearized_position(paths, rec_settings, pos_tsdf, trajectories=False):
    # Load movement dataframes
    movement_df = pd.read_pickle(
        os.path.join(paths['beh_path'], paths['nwb_file_stem']
                     + rec_settings['fname_tracking_df'])
    )
    movement_smooth_df = pd.read_pickle(
        os.path.join(paths['beh_path'], paths['nwb_file_stem']
                     + rec_settings['fname_tracking_smooth_df'])
    )

    # Add track map, traj track maps for linearization
    track_graph_file = os.path.join(paths['beh_path'], paths['nwb_file_stem']
                                    + rec_settings['fname_track_graph'])

    # Add linearized position in the entire track to the movement dataframes
    movement_df = _add_linearized_position(
        movement_df, track_graph_file, pos_tsdf=pos_tsdf)
    movement_smooth_df = _add_linearized_position(
        movement_smooth_df, track_graph_file, pos_tsdf=pos_tsdf)

    if isinstance(trajectories, pd.DataFrame):
        with open(
                os.path.join(paths['beh_path'], paths['nwb_file_stem']
                             + rec_settings['fname_track_graph_trajs_df']),
                'rb') as f:
            track_graph_traj_df = pickle.load(f)
        # Add linearized position in the trajectories to movement dataframes
        movement_df = _add_linearized_position(
            movement_df, track_graph_traj_df, traj=trajectories)
        movement_smooth_df = _add_linearized_position(
            movement_smooth_df, track_graph_traj_df, traj=trajectories)

    # Save movement dataframes for easy access.
    movement_df.to_pickle(
        os.path.join(paths['beh_path'], paths['nwb_file_stem']
                     + rec_settings['fname_tracking_df'])
    )
    movement_smooth_df.to_pickle(
        os.path.join(paths['beh_path'], paths['nwb_file_stem']
                     + rec_settings['fname_tracking_smooth_df'])
    )
    print(f"Movement dataframes saved to {paths['beh_path']}")
    return


def _add_linearized_position(movement_df, track_graph_file, pos_tsdf=None,
                             trajectories=False):
    '''
    Add linearized position to the movement dataframe.
    trajectories: Should be the trajectory dataframe if trajectories are being
                    used. If False, the entire track is used.
    '''
    lin_pos_df = pd.DataFrame()
    if pos_tsdf:
        [track_graph, edges, edge_spacing, env] = tr_pre.get_track_map(
            track_graph_file)
        for i_env, i_pos_tsdf in pos_tsdf.items():
            if not isinstance(track_graph, list) and env == i_env:
                tr_graph = track_graph
            else:
                # find the track graph for the corresponding environment
                tr_graph = [i_tg for i_tg, this_env in zip(track_graph, env)
                            if this_env == i_env]
                if len(tr_graph) == 0:
                    # No track graph for this environment
                    lin_pos_df = pd.concat([
                        lin_pos_df,
                        pd.DataFrame(i_env, columns=['env'],
                                     index=i_pos_tsdf.index)], axis=0)
                    continue
                else:
                    tr_graph = tr_graph[0]
            # TODO: edges and edge spacing assumed to be the same for all envs
            # Need to be handle if its different.
            lin_pos_one = tr_pre.get_linearized_position(
                i_pos_tsdf, tr_graph,
                edge_order=edges, edge_spacing=edge_spacing, use_HMM=False)
            lin_pos_one['env'] = i_env
            lin_pos_df = pd.concat([lin_pos_df, lin_pos_one], axis=0)
    if isinstance(trajectories, pd.DataFrame):
        # loop thru each traj, get the track graph for the corresponding track
        # template, get linearized position.
        for row_ind, traj in trajectories.iterrows():
            if traj['traj_template_ind'] == -1:
                # Some weird trajectories don't match any template
                continue
            temp = track_graph_file.loc[traj['traj_template_ind']]
            track_graph = temp['track_graph']
            edges = temp['edges']
            pos_traj = pos_tsdf.loc[traj['start_time']:traj['stop_time']]
            lin_pos_one = tr_pre.get_linearized_position(
                pos_traj, track_graph, edge_order=edges, use_HMM=False)
            lin_pos_df = pd.concat([lin_pos_df, lin_pos_one], axis=0)
        lin_pos_df = lin_pos_df.add_prefix('traj_')

    [movement_df.drop(columns=[new_col], inplace=True)
     for new_col in lin_pos_df.columns
     if new_col in movement_df.columns]
    return pd.concat([movement_df, lin_pos_df], axis=1)


def add_maps_to_partners(curr_maps_file, nwb_file, pc, proj):
    '''
    Add the track map(s) additional nwb's folders.
    Originally added for the socialW project.
    '''
    # find partner nwb folder
    paths, rec_settings = analyze1_settings.init_analysis(nwb_file, pc, proj)
    save_file = os.path.join(paths['beh_path'], paths['nwb_file_stem']
                             + rec_settings['fname_track_graph'])
    # copy to partner folder
    with open(curr_maps_file, 'rb') as f:
        curr_maps = pickle.load(f)
    with open(save_file, 'wb') as f:
        pickle.dump(curr_maps, f)
    print('Maps added to {0}'.format(save_file))
    return


def main():
    '''
    Main function to create track maps and trajectory track maps.
    Parses the input arguments and calls the necessary functions.
    '''
    input_args = parse_args()
    map_tracks(input_args.do_traj_track_maps, input_args.nwb_file,
               input_args.pc, input_args.proj)
    return


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--do_traj_track_maps', type=bool, default=False,
                        help=['boolean: True will create trajectory track'
                              + 'maps. False by default.'])
    parser.add_argument(
        '--nwb_file', type=str, default=None,
        help=['str: Variable to pass to analyze_one_nwb_settings as "nwb_file".'
              + 'Otherwise, defaults to the value in that file.'])
    parser.add_argument(
        '--pc', type=str, default=None,
        help=['str: Variable to pass to analyze_one_nwb_settings as "pc".'
              + 'Otherwise, defaults to the value in that file.'])
    parser.add_argument(
        '--proj', type=str, default=None,
        help=['str: Variable to pass to analyze_one_nwb_settings as "proj". '
              + 'Otherwise, defaults to the value in that file.'])
    input_args = parser.parse_args()
    return input_args


if __name__ == '__main__':
    main()
