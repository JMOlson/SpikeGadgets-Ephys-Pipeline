#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Built-in/Generic Imports
import os
import sys
import argparse
import pandas as pd

import analyze_one_nwb_settings.import_settings as analyze1_settings
import Beh.beh_utilities as beh_utils
import beh_core_settings.import_settings as beh_settings_importer
import Utils.epoch_tools as ep_tools
from jlab_nwb import JLabNWBHandler
from StateScriptRecData import StateScriptRec as SSRec


def build_beh_dfs(nwb_file=None, pc=None, proj=None):
    # Settings Import
    # All hard coded values are loaded or defined here
    paths, rec_settings = analyze1_settings.init_analysis(nwb_file, pc, proj)
    beh_settings = beh_settings_importer.import_beh_core_settings()

    # File Paths
    rew_df_file = os.path.join(paths["beh_path"], paths["nwb_file_stem"]
                               + rec_settings["fname_rewards_df"])
    visits_df_file = os.path.join(paths["beh_path"], paths["nwb_file_stem"]
                                  + rec_settings["fname_visits_df"])
    traj_df_file = os.path.join(paths["beh_path"], paths["nwb_file_stem"]
                                + rec_settings["fname_trajectories_df"])

    if (os.path.exists(rew_df_file) and os.path.exists(visits_df_file)
            and os.path.exists(traj_df_file)):
        print("Behavior dataframes already exist. Skipping creation.")
        return
    # Open NWB
    rec_nwb = JLabNWBHandler(nwb_name=paths["nwb_file"],
                             nwb_path=paths["nwb_path"])
    exp_metadata = rec_nwb.get_exp_metadata()
    ep_file = os.path.join(paths['nwb_path'], paths['nwb_file_stem']
                           + rec_settings['fname_base_epochs'])
    epochs_df = ep_tools.store_epoch_info(rec_nwb, ep_file, beh_settings,
                                          rec_settings["sleep_tag"])[1]

    if os.path.exists(visits_df_file):
        print("Loading existing visits dataframe.")
        visits_df = pd.read_pickle(visits_df_file)
    else:
        # import event data
        # First try DIOs - if they do not exist, read statescript logs.
        dios = rec_nwb.get_DIOs()
        if dios is False:
            print("No Dios Found - Using Statescript Logs")
            # Get events from logs.
            events_df = beh_utils.get_ss_events(rec_nwb)
            event_type_meta = beh_utils.get_ss_dio_types(exp_metadata)
            events_df = beh_utils.add_event_types_to_events(
                events_df, event_type_meta)

            # Get Rew & beambreaks Events
            rew_event_ids = list(
                event_type_meta[event_type_meta.use_type ==
                                beh_settings["reward_event_id"]].id
            )
            reward_events_df = SSRec.filter_events(events_df, rew_event_ids)

            beambreak_event_ids = list(
                event_type_meta[event_type_meta.use_type ==
                                beh_settings["well_id"]].id
            )
            beambreak_events_df = SSRec.filter_events(
                events_df, beambreak_event_ids)
        else:
            print("Using DIOs")
            DIO_groups = dios.getby_category("use_type")
            # Get Rew & beambreaks Events
            reward_events_df = DIO_groups[
                beh_settings["reward_event_id"]].create_events_df()
            beambreak_events_df = DIO_groups[
                beh_settings["well_id"]].create_events_df()

        reward_events_df = add_supplemental_event_info(
            reward_events_df, epochs_df, exp_metadata, beh_settings)
        beambreak_events_df = add_supplemental_event_info(
            beambreak_events_df, epochs_df, exp_metadata, beh_settings)
        if ("use_licks") in beh_settings and beh_settings["use_licks"]:
            beh_utils.add_licks_to_events(
                beambreak_events_df, epochs_df=epochs_df)

        # Making a visits dataframe.
        visits_df = beh_utils.create_visits_df(
            beambreak_events_df, reward_events_df, beh_settings)

    if not os.path.exists(traj_df_file):
        # Creating a trajectory dataframe
        traj_df = beh_utils.create_trajectory_df(visits_df)

    # Save dataframes to a pickle file
    reward_events_df.to_pickle(
        os.path.join(paths["beh_path"], paths["nwb_file_stem"]
                     + rec_settings["fname_rewards_df"]))
    visits_df.to_pickle(
        os.path.join(paths["beh_path"], paths["nwb_file_stem"]
                     + rec_settings["fname_visits_df"]))
    traj_df.to_pickle(
        os.path.join(paths["beh_path"], paths["nwb_file_stem"]
                     + rec_settings["fname_trajectories_df"]))
    print(f"Behavior dataframes saved to {paths['beh_path']}")
    return


def add_supplemental_event_info(events_df, epochs_df,
                                exp_metadata, beh_settings):
    beh_utils.add_epochs_to_events(events_df, epochs_df)
    events_df = beh_utils.remove_irrelevant_events(events_df, exp_metadata)
    if "home_well_id" in beh_settings:
        home_wells = epochs_df.home_well_ind
        beh_utils.add_ishomewell_to_events(events_df, home_wells)
    return events_df


def main():
    """
    Main function to create track maps and trajectory track maps.
    Parses the input arguments and calls the necessary functions.
    """
    input_args = parse_args()
    build_beh_dfs(input_args.nwb_file, input_args.pc, input_args.proj)
    return


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--nwb_file",
        type=str,
        default=None,
        help=[
            "str: Variable to pass to analyze_one_nwb_settings as "
            + '"nwb_file". Otherwise, defaults to the value in that file.'
        ],
    )
    parser.add_argument(
        "--pc",
        type=str,
        default=None,
        help=[
            'str: Variable to pass to analyze_one_nwb_settings as "pc".'
            + "Otherwise, defaults to the value in that file."
        ],
    )
    parser.add_argument(
        "--proj",
        type=str,
        default=None,
        help=[
            'str: Variable to pass to analyze_one_nwb_settings as "proj". '
            + "Otherwise, defaults to the value in that file."
        ],
    )
    input_args = parser.parse_args()
    return input_args


if __name__ == "__main__":
    main()
