#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Converting DIOs to behavioral events

License:
BSD-3-Clause
"""


# Built-in/Generic Imports
# import itertools
# import operator
# import time
import re
import warnings

# Libs
# import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pynapple as nap
# Own modules
# from jlab_nwb import JLabNWBHandler
from StateScriptRecData import StateScriptRec as SSRec

# Note: ECU times are in ms - so need to divide by 1000 to get seconds.


def get_reward_times(event_df):
    '''
    Get the reward times from an event dataframe
    '''
    return event_df[event_df['id'] == 'reward']['start_time']


def get_reward_counts(event_df):
    '''
    Get the reward counts from an event dataframe
    '''
    return len(get_reward_times(event_df))


'''
All functions below here are in support of build basic dfs. Handle
DIO and SS raw data to create events, and then produces rewards, visits,
and trajectories from those events.
'''


def get_ss_events(day_rec):
    # Get events from logs.
    ss_logs = day_rec.get_statescript_logs()
    events_df = pd.DataFrame()
    # events are built from statescript IO lines.
    for ind, ss_log in enumerate(ss_logs):
        event_log = SSRec.from_string(contents=ss_log)
        sess_events = event_log.get_events()
        sess_events['epoch_ind'] = ind
        if len(sess_events) > 0:
            events_df = pd.concat([events_df, sess_events],
                                  axis=0, ignore_index=True)
    events_df.io_type_id = events_df.io_type_id.astype(str)
    return events_df


def get_ss_dio_types(dio_metadata):
    '''
    Read id from exp metadata, create type and type index columns, add to
    dio metadata
    '''
    type_dict = {this_dio['id']: this_dio['description']
                 for this_dio in dio_metadata['behavioral_events']}
    type_dict = {key: val for key, val in type_dict.items() if val}
    type_regexp_str = '|'.join(type_dict.keys())
    type_re = r'(?P<type>{})'.format(type_regexp_str)
    dio_ids = type_dict.keys()

    io_type = [re.sub(r"\d+", "", iDIO) for iDIO in dio_ids]
    # strip all but numbers from string
    io_type_index = [re.search(r"\d+", iDIO).group()
                     for iDIO in dio_ids]
    # match DIO ids to metadata
    dio_types = [re.compile(type_re).fullmatch(iDIO)
                 for iDIO in dio_ids]
    # Parse type and type index from DIO ids using regular expressions
    use_type = [type_dict[iDIO['type']]
                if iDIO is not None else '' for iDIO in dio_types]
    use_type_id = [re.search(r"\S+$", i_type).group()
                   if i_type else '' for i_type in use_type]
    use_type = [re.sub(r"\s\S+$", "", i_type)
                for i_type in use_type]
    use_type = ['Not Labelled' if i_type ==
                '' else i_type for i_type in use_type]

    event_type_meta = pd.DataFrame({'id': np.array(dio_ids),
                                    'io_type': np.array(io_type),
                                    'io_type_index': np.array(io_type_index),
                                    'use_type': np.array(use_type),
                                    'use_type_id': np.array(use_type_id)})
    return event_type_meta


def add_event_types_to_events(events_df, event_type_meta):
    # Append correct event_type_meta to events_df.
    events_df['use_type'] = None
    events_df['use_type_id'] = None
    for i_id in event_type_meta['id']:
        these_events = events_df['id'] == i_id
        n_events = sum(these_events)
        this_ch = event_type_meta[event_type_meta['id'] == i_id]
        # Explicit broadcast - don't understand why i can't just broadcast it.
        events_df.loc[these_events, 'use_type'] = np.repeat(
            this_ch['use_type'], n_events).values
        events_df.loc[these_events, 'use_type_id'] = np.repeat(
            this_ch['use_type_id'], n_events).values
    return events_df


def add_epochs_to_events(events_df, epochs_df):
    events_df['epoch_ind'] = -1
    for ind in epochs_df.index:
        in_ep = ((epochs_df.start_time[ind] < events_df.start_time)
                 & (epochs_df.stop_time[ind] > events_df.stop_time))
        if sum(in_ep) == 0:
            continue  # no events in this epoch
        else:  # add the epoch index to the events in this epoch
            events_df.loc[in_ep.values, 'epoch_ind'] = ind
    return


def add_licks_to_events(events_df, epochs_df=None,
                        lick_dur_min_secs=None, lick_dur_max_secs=None):
    '''
    Determine if an event is a lick event. Add a column to the events_df for
    lick status. Also return a boolean array of lick status. If lick_dur_min
    and lick_dur_max are not provided, they will be taken from the epochs_df.

    Parameters
    ----------
    events_df : pandas.DataFrame
        DataFrame of events. Must have columns 'start_time' and 'duration'.
    epochs_df : pandas.DataFrame, optional
        DataFrame of epochs. Must have 'ss_vars' with lickDurationMin and
        lickDurationMax.
    lick_dur_min_secs : float, optional
        Minimum lick duration in seconds. The default is None.
    lick_dur_max_secs : float, optional
        Maximum lick duration in seconds. The default is None.
    '''
    # Handle inputs
    # if one of min and max are not none
    # XOR
    if bool(lick_dur_min_secs) ^ bool(lick_dur_max_secs):  # XOR
        raise ValueError('Must specify both min and max lick duration' +
                         ' or neither.')
    elif bool(lick_dur_min_secs) and (not epochs_df.empty):
        raise ValueError('Specify either epochs_df that contains ' +
                         'lick_durations, or the lick durations directly.')
    elif lick_dur_min_secs:
        # Lick duration is specified, so do nothing
        pass
    elif epochs_df is None:
        raise ValueError('Must specify epochs_df or ' +
                         'lick_dur_min and lick_dur_max')
    elif epochs_df is not None:
        lick_dur_min_secs = [int(
            epochs_df['ss_vars'][ind]['lickDurationMin'])/1000
            for ind in epochs_df.index]
        lick_dur_max_secs = [int(
            epochs_df['ss_vars'][ind]['lickDurationMax'])/1000
            for ind in epochs_df.index]
    else:
        # no cases should ever hit this... error if they do
        raise ValueError('Something went wrong with the input handling.')

    # Add a column to the events_df for lick status
    events_df['is_lick'] = False
    for ind in epochs_df.index:
        in_ep = ((epochs_df.start_time[ind] < events_df.start_time)
                 & (epochs_df.stop_time[ind] > events_df.stop_time))
        if sum(in_ep) == 0:
            continue
        events_df.loc[in_ep.values, 'is_lick'] = (
            (events_df.loc[in_ep.values, 'duration']
             <= lick_dur_max_secs[ind])
            & (events_df.loc[in_ep.values, 'duration']
               >= lick_dur_min_secs[ind]))
    return events_df['is_lick']


def create_visits_df(events_df, reward_events_df, beh_settings):
    '''
    Create a dataframe of visits from an event dataframe of beambreaks.
    '''
    events_df.sort_values(by='start_time', inplace=True)

    if 'use_licks_only' in beh_settings and beh_settings['use_licks_only']:
        events_to_use = events_df[events_df['is_lick']]
    else:
        events_to_use = events_df

    visit_diff = [a != b for a, b in zip(events_to_use['use_type_id'][:-1],
                                         events_to_use['use_type_id'][1:])]
    # buffered to include the first visit and move the diff to the first event
    # of the visit, instead of the last event of the previous visit
    visit_diff_starts = np.append([True], visit_diff)
    visit_diff_stops = np.append(visit_diff, [True])
    visit_start_events = events_to_use.iloc[visit_diff_starts.nonzero()[0]]
    visit_stop_events = events_to_use.iloc[visit_diff_stops.nonzero()[0]]

    visits_df = pd.DataFrame({'id': visit_start_events['use_type_id'].values,
                              'start_time': visit_start_events['start_time'].values,
                              'stop_time': visit_stop_events['stop_time'].values,
                              'epoch_ind':
                              visit_start_events['epoch_ind'].values,
                              })
    if 'is_home' in events_df.columns:
        visits_df['is_home'] = visit_start_events['is_home'].values
    visits_df['duration'] = visits_df['stop_time'] - visits_df['start_time']

    if 'min_visit_dur' in beh_settings and beh_settings['min_visit_dur']:
        min_visit_dur = beh_settings['min_visit_dur']
        visits_df = visits_df[visits_df['duration'] >= min_visit_dur]
    if 'is_lick' in events_df.columns:
        add_licks_info_to_visits(visits_df, events_df)
    add_rewards_to_visits(visits_df, reward_events_df)
    return visits_df


def remove_irrelevant_events(events_df, exp_metadata):
    '''
        This function removes events that are not in the event list for each
        epoch. For the SocialW task, this is used to remove events from the
        other environment(s) (and therefore animals).

        NOTE: Assuming epochs are 1-indexed in the settings yml file.
    '''
    # concatenate two columns of strings into 1
    full_use_strs = events_df['use_type'] + ' ' + events_df['use_type_id']

    to_keep = [False]*len(events_df)
    for epoch in exp_metadata['epochs']:
        env = epoch['env_name']
        for this_env in exp_metadata['environment']:
            if this_env['env_name'] == env:
                break  # found the environment
        event_list = this_env['event_list']
        if event_list is None:
            continue
        is_used_event = [
            i_f_u_s in event_list for i_f_u_s in full_use_strs]
        events_in_epoch = events_df['epoch_ind'] == (
            epoch['index']-1)  # 1-indexed
        to_keep = [a or (b and c) for a, b, c in zip(
            to_keep, is_used_event, events_in_epoch.values)]
    events_df = events_df[to_keep]  # toss ignored events

    return events_df


def add_licks_info_to_visits(visits_df, beambreak_events_df,
                             posthoc_licks=None, posthoc_nonlicks=None):
    '''
    Add columns to visits with lick information:
        StateScript-defined lick IntSet
        StateScript-defined non-licks IntSet
    If posthoc_licks are provieded, we will also add:
        Post-hoc defined licks
        Post-hoc defined non-licks

    Parameters
    ----------
    visits_df : pandas.DataFrame
        DataFrame of visits. Created by (ThisMod).create_visits_df.
    beambreak_events_df : pandas.DataFrame
        DataFrame of beambreak events. Created by DIOS.create_events_df
    posthoc_licks : pandas.DataFrame, optional
        Pynapple interval set of post-hoc defined licks. The default is None.
        If None, will not add post-hoc lick information.
    posthoc_nonlicks : pandas.DataFrame, optional
        Pynapple interval set of post-hoc defined non-licks. The default is
        None. If None, will not add post-hoc non-lick information.
    '''
    visit_intset = nap.IntervalSet(
        visits_df['start_time'], visits_df['stop_time'])

    is_lick = beambreak_events_df['is_lick']
    ss_licks = nap.IntervalSet(beambreak_events_df.start_time[is_lick],
                               beambreak_events_df.stop_time[is_lick])
    ss_nonlicks = nap.IntervalSet(beambreak_events_df.start_time[~is_lick],
                                  beambreak_events_df.stop_time[~is_lick])

    # intersect Interval Sets to get the events for each visit.
    visits_df['ss_licks'] = [visit_intset[visit_ind].intersect(ss_licks)
                             for visit_ind in visit_intset.index]
    visits_df['ss_nonlicks'] = [visit_intset[visit_ind].intersect(ss_nonlicks)
                                for visit_ind in visit_intset.index]
    if posthoc_licks is not None:
        visits_df['posthoc_licks'] = [
            visit_intset[visit_ind].intersect(posthoc_licks)
            for visit_ind in visit_intset.index]
    if posthoc_nonlicks is not None:
        visits_df['posthoc_licks_nonlicks'] = [
            visit_intset[visit_ind].intersect(posthoc_nonlicks)
            for visit_ind in visit_intset.index]
    return visits_df


def add_ishomewell_to_events(events_df, home_well_per_epoch):
    '''
    Add a column to events_df indicating whether the event is a home well
    event. This is based on the use_type_id of the event.
    '''
    home_well = [home_well_per_epoch[i] for i in events_df['epoch_ind']]
    events_df['is_home'] = (events_df['use_type_id'].values == home_well)

    return events_df


def add_rewards_to_visits(visits_df, reward_events_df):
    '''
    Add a column to visits_df indicating whether the visit was rewarded
    and a column with the reward time.
    Any reward event that occurs during the visit or before the next visit is
    considered to be a reward for that visit. The event must have the same
    index as the visit as a safety measure.
    '''
    reward_events = nap.Tsd(
        t=reward_events_df.start_time.values,
        d=reward_events_df.use_type_id.values)
    # max_time = np.maximum(visits_df['stop_time'].iloc[-1],
    #                       reward_events.index.values[-1])
    visit_intset = nap.IntervalSet(np.append(0, visits_df['stop_time'][:-1]),
                                   visits_df['stop_time'])
    reward_inds = visit_intset.in_interval(reward_events)
    if np.any(visits_df.iloc[reward_inds, visits_df.columns.get_loc('id')]
              != reward_events.values):  # removed integer type requirement here
        # give a warning
        warnings.warn('Reward events do not all have' +
                      ' the same well as the visit.')

    is_rew_arr = np.array([False]*len(visits_df))
    print(is_rew_arr.shape)
    is_rew_arr[reward_inds.astype(int)] = True
    visits_df['is_rewarded'] = is_rew_arr

    rew_arr = np.array([np.nan]*len(visits_df))
    rew_times = reward_events.index.values
    for i_reward in range(len(reward_inds)):
        this_visit_ind = reward_inds[i_reward].astype(int)
        if not np.isnan(this_visit_ind):
            if np.isnan(rew_arr[this_visit_ind]):
                rew_arr[this_visit_ind] = rew_times[i_reward]
            else:
                warnings.warn('You have two rewards in {ind} visit. We kept the first one'.format(
                    ind=reward_inds[i_reward]))
    visits_df['reward_times'] = rew_arr
    return visits_df


def create_trajectory_df(visits_df):
    '''
    Create a dataframe of trajectories from the well visits dataframe

    Start time is currently the stop time of the previous visit, and stop time
    is the start time of the next visit. This isn't when the animal starts
    moving, so it may not be what we want to use, or we can then filter by
    speed.

    I could imagine movement speed, theta power, and/or location all as
    possible choices for the start and stop times of a trajectory.
    '''
    # init a dataframe and ignore the current indexes of visits_df
    # make an array of length visits_df - 1 of zeros
    starts = visits_df.loc[visits_df.index[:-1], 'id'].values
    ends = visits_df.loc[visits_df.index[1:], 'id'].values
    traj_ids = [str(start) + '->' + str(end)
                for start, end in zip(starts, ends)]
    traj_df = pd.DataFrame({
        'traj_id': traj_ids,
        'epoch_ind': visits_df.loc[visits_df.index[:-1], 'epoch_ind'].values,
        'start_well': starts,
        'end_well': ends,
        'start_time': visits_df.loc[visits_df.index[:-1], 'stop_time'].values,
        'stop_time': visits_df.loc[visits_df.index[1:], 'start_time'].values,
        'is_rewarded': visits_df.loc[visits_df.index[1:],
                                     'is_rewarded'].values,
    })
    if 'is_home' in visits_df.columns:
        # add outboun and inbound columns
        traj_df['is_outbound'] = visits_df.loc[visits_df.index[:-1],
                                               'is_home'].values
        traj_df['is_inbound'] = visits_df.loc[visits_df.index[1:],
                                              'is_home'].values

    if 'match' in visits_df.columns:  # add match column
        traj_df['match'] = visits_df.loc[visits_df.index[1:],
                                         'match'].values

    return traj_df
