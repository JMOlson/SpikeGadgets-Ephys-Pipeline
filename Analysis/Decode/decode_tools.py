# -*- coding: utf-8 -*-
'''

'''
import os
from os.path import exists
from copy import deepcopy
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import replay_trajectory_classification as rtc
from track_linearization import get_linearized_position

import Beh.Tracking.tracking_preprocessor as tr_pre

# Constants
STATES = ['continuous', 'fragmented', 'stationary']
LOCAL_REMOTE_THRESH = 20  # cm away from animal
AT_LOC_THRESH = 10 # if animal is less than this distance away, they are 'at' the designated location
VEL_THRESH = 2  # cm/s
PLACE_BIN_SIZE = 2.0
MOVEMENT_VAR = 1 # EmilyAeryJones also considered 6 with a 2 state only model.
POSITION_STD = 6.0
ALGORITHM = 'spiking_likelihood_kde'
CONTINUOUS_TRANSITION_TYPES = [
    [rtc.RandomWalk(movement_var=MOVEMENT_VAR), rtc.Uniform(), rtc.Identity()],
    [rtc.Uniform(), rtc.Uniform(), rtc.Uniform()],
    [rtc.RandomWalk(movement_var=MOVEMENT_VAR), rtc.Uniform(), rtc.Identity()]]

MAX_GAP_SECS = 0.5

# Based closely off of Emily Aery Jones' code from her MEC 2024 paper.
class Classifier_Model:
    '''Build a model to fit to spiking and predict X from spiking

    Attributes:
        name (string):                         path to binned_data file
        classifier (SortedSpikesClassifier):   classifier & decoder model
        classifier_results (xarray):           predicted results       
        
    Methods:
        load
        save
        
    Functions:
        bin_data
        build_model
        build_nonlocal_model
        build_2D_model
        train_model
        train_2D_model
        train_nonlocal_model
        predict_from_model
        predict_from_nonlocal_model
        order_cells_by_field

    '''

    def __init__(self, name="", spikes=None, position=None, spikes_test=None,
                 position_test=None, cross_val=0, is_training=None,
                 classifier_type='decoder', model_parameters=dict(), plot=False):
        '''Loads or extracts sequences and relevant variables

        Parameters:
            name (string):                      optional, path to sequences file
            spikes (Spike object):              optional, spiketimes
            position (Position object):         optional, position of animal 
            spikes_test (Spikes object):        optional, spiketimes to decode, if different from training set
            position_test (Position object):    optional, position of animal, if different from training set
            cross_val (int)                     optional, whether to do n-fold cross-validation
            is_training (array of bool):        optional, which indices of binned data to train model on (otherwise use any time vel>2)
            classifier_type (String):           optional, whether to use the Denovellis 2021 linearized (decoder), (2D), or 2023 (nonlocal) classifier
            model_parameters (dict):            optional, parameters for the model
            
        '''

        self.name = name
        self.cv = cross_val
        self.classifier_type = classifier_type

        # if instance already defined, load. Otherwise, create.
        if exists(self.name):
            self.load()
        elif name:
            raise Exception(f"Cannot find file {self.name}")
        else:
            '''Initialize the classifier model'''
            if (spikes is None) or (position is None):
                raise Exception(
                    "Must specify Spikes object and Position object")
            if (spikes_test is not None) and cross_val:
                raise Exception("Set cross_val=False if training decoder ",
                                "on one dataset and testing on another (e.g. ",
                                "decoding track position from restbox spikes).")

            # specify classifier model params
            if classifier_type=='decoder':
                untrained_classifier = Classifier_Model.build_model(
                    params=model_parameters)
            elif classifier_type=='nonlocal':
                pass
                # untrained_classifier = Classifier_Model.build_nonlocal_model(position.arena)
            elif classifier_type=='2D':
                pass
                # untrained_classifier = Classifier_Model.build_2D_model()
            else:
                raise Exception(
                    "Classifier_type must be 'decoder', '2D', or 'nonlocal'")

            # bin training data - We handle before input
            # timestamps, spikes, position = bin_data(
            #     spikes, position, classifier_type)
                
            # split into cross-validation folds (if necessary)
            if cross_val > 1:
                self.n_folds = cross_val
                self.folds = np.array_split(
                    np.arange(position.shape[1]), self.n_folds)
            else:
                self.n_folds = 1
                self.folds = None
            
            # train model
            if classifier_type=='decoder':
                self.classifier = Classifier_Model.train_model(
                    spikes, position, untrained_classifier, self.n_folds,
                    self.folds, is_training, plot=plot)
            elif classifier_type=='nonlocal':
                pass
                # self.classifier = Classifier_Model.train_nonlocal_model(
                #     spikes, position, untrained_classifier,
                #     n_folds, folds, is_training)
            elif classifier_type=='2D':
                pass
                # self.classifier = Classifier_Model.train_2D_model(
                #     spikes, position, untrained_classifier, n_folds,
                #     folds, is_training)
                   
            # # bin test data
            # if (spikes_test is None):
            #     spikes_test = spikes
            # if (position_test is None):
            #     position_test = position
            # test_binned_timestamps, test_binned_spikes, test_binned_position = \
            #     bin_data(spikes_test, position_test, classifier_type)
                
            # # use model to decode test spikes
            # if classifier_type=='decoder' or classifier_type=='2D':
            #     self.classifier_results = predict_from_model(
            #         test_binned_timestamps, test_binned_spikes,
            #         self.classifier, n_folds, folds)
            # elif classifier_type=='nonlocal':
            #     self.classifier_results = predict_from_nonlocal_model(
            #         test_binned_timestamps, test_binned_spikes, 
            #         test_binned_position, self.classifier, n_folds, folds)

    def load(self):
        '''Load from files'''
        with open(self.name, 'rb') as file:
            self.classifier = pickle.load(file)
        
        # # read classifier results in chunks so pickle doesn't give Errno 22    
        # max_bytes = 2**31 - 1
        # input_size = getsize(self.name[:-4]+'_results.txt')
        # bytes_in = bytearray(0)
        # with open(self.name[:-4]+'_results.txt', 'rb') as file:
        #     for _ in range(0, input_size, max_bytes):
        #         bytes_in += file.read(max_bytes)
        # self.classifier_results = pickle.loads(bytes_in)

    def save(self):
        '''Save to file'''
        with open(self.name, 'wb') as file:
            pickle.dump(self.classifier, file)
            
        # # break classifier results into chunks so pickle doesn't give Errno 22
        # max_bytes = 2**31 - 1
        # bytes_out = pickle.dumps(self.classifier_results)
        # n_bytes = getsizeof(bytes_out)
        # with open(self.name[:-4]+'_results.txt', 'wb') as file:
        #     for idx in range(0, n_bytes, max_bytes):
        #         file.write(bytes_out[idx:idx+max_bytes])
             
    @staticmethod
    def bin_data(spikes, position, classifier_type):
        # bin spikes (2ms) & generate matching upsampled linearized position trace
        binned_spikes, binned_timestamps = sp.calc_binned_spikes(
            spikes=spikes.spikes, start=0, end=spikes.end-spikes.start)

        # upsample, then linearize, so that linearized position doesn't smooth between segments
        if classifier_type == '2D':
            resampled_pos = pos.upsample(df=position.position[['Timestamp', 'X', 'Y', 'Velocity']],
                                    new_timestamps=binned_timestamps)
        elif classifier_type=='decoder' or classifier_type=='nonlocal':
            resampled_pos = pos.linearize_xy_only(
                resampled_pos, position.arena)
        binned_position = resampled_pos.set_index('Timestamp')
        return binned_timestamps, binned_spikes, binned_position

    @staticmethod
    def build_model(params=dict()):
        '''
        
        Needed Parameters:
        place_bin_size (float): size of bins in cm
        track_graph (networkx graph): graph of track
        edge_order (list of tuples): order of edges
        edge_spacing (float): spacing between edges
        movement_var (float): variance of movement
        position_std (float): standard deviation of position

        '''
        if type(params) is not dict:
            raise Exception("Params must be a dictionary of model settings") 
        # check for keys in params, otherwise use defaults
        if 'track_graph_file' in params:
            track_graph, edges, edge_spacing = tr_pre.get_track_map(
                file=os.path.join(params['track_graph_file']), plot=False)
        elif 'track_graph_file' not in params and 'environment' not in params:
            raise Exception("Must provide a track graph file or an environment")
        
        # Use defaults if not provided
        if 'place_bin_size' not in params: 
            params['place_bin_size'] = PLACE_BIN_SIZE
        if 'movement_var' not in params:
            params['movement_var'] = MOVEMENT_VAR
        if 'position_std' not in params:
            params['position_std'] = POSITION_STD      
        if 'environment' not in params:
            params['environment'] = rtc.Environment(
                place_bin_size=params['place_bin_size'],
                track_graph=track_graph,
                edge_order=edges,
                edge_spacing=edge_spacing)
        if 'continuous_transition_types' not in params:
            params['continuous_transition_types'] = CONTINUOUS_TRANSITION_TYPES
        if 'algorithm' not in params:
            params['algorithm'] = ALGORITHM
        
        # Construct model
        # There is a decent chance that in order to support other algorithms,
        # we will need to pass in the parameters differently. Not sure there
        # are any other algorithms that are supported by the current version.
        classifier = rtc.SortedSpikesClassifier(
            environments=params['environment'],
            continuous_transition_types=params['continuous_transition_types'],
            sorted_spikes_algorithm=params['algorithm'],
            sorted_spikes_algorithm_params={
                'position_std':params['position_std']
                }
        )
        return classifier
    
    @staticmethod
    def build_nonlocal_model(arena):
        # build model from training data
        environment = rtc.Environment(place_bin_size=2.0,
                                    track_graph=arena.track_graph,
                                    edge_order=arena.edge_order,
                                    edge_spacing=arena.edge_spacing)
        classifier = nl.NonLocalSortedSpikesDetector(
            environments=environment,
            sorted_spikes_algorithm='sorted_spikes_kde',
            sorted_spikes_algorithm_params={'position_std': 6.0}
        )
        return classifier
    
    @staticmethod
    def build_2D_model():
        # build model from training data
        environment = rtc.Environment(place_bin_size=2.0)
        continuous_transition_types = [[rtc.RandomWalk(movement_var=1),
                                        rtc.Uniform(), rtc.Identity()],
                                    [rtc.Uniform(),
                                        rtc.Uniform(), rtc.Uniform()],
                                    [rtc.RandomWalk(movement_var=1),
                                        rtc.Uniform(), rtc.Identity()]]
        classifier = rtc.SortedSpikesClassifier(
            environments=environment,
            continuous_transition_types=continuous_transition_types,
            sorted_spikes_algorithm='spiking_likelihood_kde',
            sorted_spikes_algorithm_params={'position_std': 6.0}
        )
        return classifier
    
    @staticmethod
    def train_model(binned_spikes, position_upsampled, classifier, n_folds=5,
                    folds=None, is_training=None, plot=True):
        '''
        Parameters:
        - binned_spikes (pynapple TsdFrame):  binned spikes (time x units)
        - position_upsampled (pandas DataFrame): position matching the binned spikes sampling  (time x position)
        '''
        # fit model
        if n_folds > 1:
            classifiers = []
            for i in range(n_folds): 
                fold_classifier = deepcopy(classifier)
                held_in_inds = np.concatenate(folds[:i] + folds[i+1:])
                if is_training is None:
                    is_training = (
                        position_upsampled['vel'].iloc[held_in_inds]
                        > VEL_THRESH)
                if plot:
                    _plot_movement(position_upsampled[is_training])
                fold_classifier.fit(
                    position_upsampled.loc[held_in_inds,'linear'],
                    binned_spikes.as_array()[:, held_in_inds],
                    is_training=is_training)
                classifiers.append(fold_classifier)
            return classifiers
        else:
            # is_training is a boolean array of the same length as the data, 
            # where True indicates that the data should be used for training.
            # if is_training is None, use all data where velocity > X
            if is_training is None:
                is_training = position_upsampled['vel'] > VEL_THRESH
            if plot:
                _plot_movement(position_upsampled[is_training])
            classifier.fit(position_upsampled['linear_position'],
                           binned_spikes.as_array(), is_training=is_training)
            return classifier 
    
    @staticmethod
    def train_nonlocal_model(binned_timestamps, binned_spikes, binned_position, classifier, n_folds=5, folds=None, is_training=None):
        # fit model
        if folds is not None:
            classifiers = []
            for i in range(n_folds): 
                fold_classifier = deepcopy(classifier)
                held_in_inds = np.concatenate(folds[:i] + folds[i+1:])
                is_training = binned_position['Velocity'].iloc[held_in_inds] > 2
                fold_classifier.fit(position_time=binned_timestamps.iloc[held_out_inds], 
                    position=binned_position['Linear'].iloc[held_in_inds],
                    spike_times=binned_spikes[:, held_in_inds].T,
                    is_training=is_training)
                classifiers.append(fold_classifier)
            return classifiers
        else:
            if is_training is None:
                is_training = binned_position['Velocity'] > 2
            #time, position, spike_times, is_training
            classifier.fit(position_time=binned_timestamps, 
                position=binned_position['Linear'],
                spike_times=binned_spikes.T,
                is_training=is_training)
            return classifier

    @staticmethod        
    def train_2D_model(binned_spikes, binned_position, classifier, n_folds=5, folds=None, is_training=None):
        # fit model
        if folds is not None:
            classifiers = []
            for i in range(n_folds): 
                fold_classifier = deepcopy(classifier)
                held_in_inds = np.concatenate(folds[:i] + folds[i+1:])
                is_training = binned_position['Velocity'].iloc[held_in_inds] > 2
                fold_classifier.fit(binned_position[['X','Y']].iloc[held_in_inds], binned_spikes[:, held_in_inds].T, is_training=is_training)
                classifiers.append(fold_classifier)
            return classifiers
        else:
            if is_training is None:
                is_training = binned_position['Velocity'] > 2
            classifier.fit(binned_position[['X','Y']], binned_spikes.T, is_training=is_training)
            return classifier

    def predict(self, binned_spikes):
        '''
            Wrapper for predict_from_model
        '''
        self.classifier_results = Classifier_Model.predict_from_model(
            binned_spikes, self.classifier, self.n_folds, self.folds)
        return self.classifier_results
    
    @staticmethod
    def predict_from_model(binned_spikes, classifier, n_folds=5, folds=None):
        # predict decoded position and classification from test data
        if folds is not None:
            classifier_results = []
            for i in range(n_folds): 
                held_out_inds = folds[i]
                fold_classifier_results = classifier[i].predict(
                    binned_spikes.as_array()[:, held_out_inds],
                    time=binned_spikes.index.values[held_out_inds],
                    state_names=STATES, use_gpu=True)
                classifier_results.append(fold_classifier_results)
        else:
            classifier_results = classifier.predict(
                binned_spikes.as_array(), time=binned_spikes.index.values,
                state_names=STATES, use_gpu=True)
        return classifier_results
    
    @staticmethod
    def predict_from_nonlocal_model(binned_timestamps, binned_spikes, binned_position, classifier, n_folds=5, folds=None):
        # predict decoded position and classification from test data
        if folds is not None:
            classifier_results = []
            for i in range(n_folds): 
                held_out_inds = folds[i]
                fold_classifier_results = classifier[i].predict(binned_spikes[:, held_out_inds].T, time=binned_timestamps[held_out_inds], )
                fold_classifier_results = classifier.predict(
                    spike_times=binned_spikes[:, held_out_inds].T,
                    time=(binned_timestamps[held_out_inds[0]],binned_timestamps[held_out_inds[-1]]+1/500),
                    position=binned_position['Linear'].iloc[held_out_inds],
                    position_time=binned_timestamps.iloc[held_out_inds])
                classifier_results.append(fold_classifier_results)
        else:
            classifier_results = classifier.predict(spike_times=binned_spikes.T,
                time=(binned_timestamps[0],binned_timestamps[-1]+1/500),
                position=binned_position['Linear'],
                position_time=binned_timestamps)
        return classifier_results

    @staticmethod
    def order_cells_by_field(classifier):
        n_cells = classifier.place_fields_[('', 0)].shape[1]
        max_bin = np.zeros(n_cells)
        for u in range(n_cells):
            max_bin[u] = float(classifier.place_fields_[('', 0)].sel(neuron=u).idxmax().values)
        return np.argsort(max_bin), max_bin
    
def upsample_movement(tracking_df, dim, track_graph_file, bin_size,
                      time_support=None, max_gap_secs=MAX_GAP_SECS, plot=True):
    '''
    Upsample movement data to match the binned spikes.
    Must relinearize the position data after upsampling for 1D data.
    '''
    pos_upsample = tr_pre.upsample_movement_1D(
        tracking_df[['x','y','vel']], bin_size, time_support,
        max_gap_secs=MAX_GAP_SECS)
    if dim==1:
        # Load track graph to obtain track graph, edges, and edge spacing for model
        track_graph, edges, edge_spacing = tr_pre.get_track_map(
            file=os.path.join(track_graph_file), plot=False)

        # convert dataframe to array
        linearized_df = get_linearized_position(
            pos_upsample[['x','y']].to_numpy(), track_graph, edge_order=edges,
            edge_spacing=edge_spacing)
        pos_upsample['linear_position'] = linearized_df['linear_position'].values
    elif dim==2:
        pass
    else:
        raise Exception("Dimension must be 1D or 2D")
    if plot:
        _plot_movement(pos_upsample, dim)
    return pos_upsample


def _plot_movement(pos, dim=2):
    '''
    Helper function to plot movement data
    '''
    fig1, ax1 = plt.subplots(figsize=(20, 5))
    ax1.scatter(pos['x'], pos['y'], clip_on=False, s=1,
                color='black', alpha=0.005)
    sns.despine()
    plt.show()
    if dim==1:
        # make 2 subplots for linear position and velocity
        fig, axs = plt.subplots(2, 1, figsize=(20, 10), 
                                gridspec_kw={'height_ratios': [2, 1]},
                                sharex=True)
        # Plot linear position
        axs[0].scatter(pos.index.values, pos['linear_position'],
                        clip_on=False, s=1, color='black')
        axs[0].set_ylabel('Linear Position')
        sns.despine(ax=axs[0])
        # Plot velocity
        axs[1].scatter(pos.index.values, pos['vel'],
                        clip_on=False, s=1, color='red')
        axs[1].set_xlabel('Time')
        axs[1].set_ylabel('Velocity')
        sns.despine(ax=axs[1])
    plt.show()
    return