#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

import numpy as np
import pandas as pd
import pynapple as nap
from pycircstat2 import descriptive as circstats
from scipy import signal
from scipy.signal import butter, filtfilt, hilbert, lfilter, sosfiltfilt


def get_LFP_metadata(nwb_file, processed=True):
    lfp_meta = nwb_file.get_LFP_metadata()

    if processed:
        # remove group name column from lfp_meta
        lfp_meta = lfp_meta.drop(columns=['group', 'chID', 'hasLFP'])
        # add index as a column with name "lfp_index"
        lfp_meta['lfp_index'] = range(lfp_meta.shape[0])
    return lfp_meta


def butter_bandpass(lowcut, highcut, fs, order=5, output='ba'):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    if output == 'ba':
        b, a = butter(order, [low, high], btype='band')
        return b, a
    elif output == 'sos':
        sos = butter(order, [low, high], btype='band', output='sos')
        return sos


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5,
                           filter_type='filtfilt'):
    if filter_type == 'filtfilt':
        b, a = butter_bandpass(lowcut, highcut, fs, order=order)
        y = filtfilt(b, a, data)
    elif filter_type == 'sosfiltfilt':
        sos = butter_bandpass(lowcut, highcut, fs, order=order, output='sos')
        y = sosfiltfilt(sos, data)
    elif filter_type == 'lfilter':
        b, a = butter_bandpass(lowcut, highcut, fs, order=order)
        y = lfilter(b, a, data)
    else:
        print(filter_type)
    return y


def filter_and_save_lfp(rec_NWB, epochs_intSet, bands, save_file_name_format,
                        save_path, nwb_file, order=9,
                        filter_type='sosfiltfilt'):
    '''
    Filter LFP data.
    '''
    # Main loop
    for epoch_ind in epochs_intSet.index:
        this_epoch = epochs_intSet.loc[[epoch_ind]]
        lfps_df = None

        for band_name, limits in bands.items():
            min_fs = limits[0]
            max_fs = limits[1]
            this_file_name = os.path.join(
                save_path, nwb_file + save_file_name_format.format(
                    band_name, min_fs, max_fs, epoch_ind))
            if os.path.exists(this_file_name):
                # skip - already done.
                continue

            # waitin to load lfp until now - because it's slow.
            if lfps_df is None:
                lfps_df = rec_NWB.get_LFPs().restrict(this_epoch)
                ts = lfps_df.index.values
                fs = round(1 / np.median(np.diff(ts)))
            lfp_band = butter_bandpass_filter(
                lfps_df.as_array().T, min_fs, max_fs, fs,
                order=order, filter_type=filter_type).T
            pd.DataFrame(lfp_band, index=ts,
                         columns=lfps_df.columns).to_pickle(
                os.path.join(save_path, nwb_file + this_file_name))

    # with open(os.path.join(NWB_PATH, NWB_FILE + this_file_name), 'rb') as f:
    #     lfp_band = pickle.load(f)
    return None


def create_signal_tsdfs(lfp_band_df, fs):
    analytic_signal = hilbert(lfp_band_df, axis=0)
    amplitude_envelope = np.abs(analytic_signal)
    power = amplitude_envelope**2
    instantaneous_phase = np.angle(analytic_signal)
    instantaneous_frequency = np.diff(instantaneous_phase,
                                      append=np.nan, axis=0) / (2.0*np.pi) * fs

    return (nap.TsdFrame(t=lfp_band_df.index.values,
                         d=analytic_signal),
            nap.TsdFrame(t=lfp_band_df.index.values,
                         d=amplitude_envelope),
            nap.TsdFrame(t=lfp_band_df.index.values,
                         d=power),
            nap.TsdFrame(t=lfp_band_df.index.values,
                         d=instantaneous_phase),
            nap.TsdFrame(t=lfp_band_df.index.values,
                         d=instantaneous_frequency))


def phase_synchrony(lfp_phases, FS=1500):
    '''
    Phase synchrony - All combos - ~ 3 min
    Lachaux 2002 Phase Locking Value - Burgess 2014 used too
    Positive means the reference channel (first parameter) is leading
    '''
    # n_samples = lfp_phases.shape[0]
    n_lfps = lfp_phases.shape[1]
    phase_diff_theta_circ_mean = np.zeros((n_lfps, n_lfps))
    phase_diff_theta_circ_resultant = np.zeros((n_lfps, n_lfps))
    for i_lfp in range(n_lfps):
        phase_diff_theta = (lfp_phases - lfp_phases[:, [i_lfp]])
        for i_lfp2 in range(n_lfps):
            (phase_diff_theta_circ_mean[i_lfp, i_lfp2],
             phase_diff_theta_circ_resultant[i_lfp, i_lfp2]) = (
                circstats.circ_mean(phase_diff_theta[:, i_lfp2],
                                    return_r=True))
    # Shift the phase difference range from 0:2pi => -pi:pi
    phase_diff_theta_circ_mean = (
        (phase_diff_theta_circ_mean + np.pi) % (2 * np.pi) - np.pi)

    return phase_diff_theta_circ_mean, phase_diff_theta_circ_resultant


def amplitude_coupling(lfp_amp_envs, FS=1500):
    '''
    Amplitude Coupling - all combos - ~ 30 min.
    Why the norm? https://stackoverflow.com/questions/
    53436231/normalized-cross-correlation-in-python
    '''
    n_samples, n_lfps = lfp_amp_envs.shape
    amp_env_normed = (lfp_amp_envs
                      / np.linalg.norm(lfp_amp_envs, axis=0))
    amp_env_lags = np.zeros([n_lfps, n_lfps])
    amp_coupling = np.zeros([n_lfps, n_lfps])
    lags = signal.correlation_lags(n_samples, n_samples, mode="same")
    for i_lfp in range(n_lfps):
        amp_env_ref_mean_normed = amp_env_normed[:, i_lfp]
        amp_env_xcorrs = np.zeros_like(lfp_amp_envs)
        for i_lfp2 in range(n_lfps):
            amp_env_xcorrs[:, i_lfp2] = signal.correlate(
                amp_env_ref_mean_normed, amp_env_normed[:, i_lfp2],
                mode='same')
        amp_env_lags[i_lfp, :] = lags[np.argmax(amp_env_xcorrs, axis=0)].T/FS
        amp_coupling[i_lfp, :] = np.max(amp_env_xcorrs, axis=0).T

    return amp_coupling, amp_env_lags


def detect_ripples(lfp_band_rip, lfp_timestamps, spikes, vel,
                   SIGMA=30, VEL_THRESH=4, PEAK_THRESH=6,
                   PEAK_SEP=1, PEAK_WIDTH=10, PEAK_PROM=6):
    '''
    Michael Satchell's ripple detection algorithm, ported to python.
    '''

    '''
    Each tetrode's z-scored envelope measures, relatively, the power of the
    ripple band for that tetrode, and if we add all the zEnvs together we get
    the population relative zscore. Zscoring this sum shows the significance of
    power changes for the all tetrodes while maintaining the difference in
    mean ripple power for each tetrode.
    '''
    rip_band_envelope = np.abs(sp.signal.hilbert(lfp_band_rip, axis=0))
    rip_band_envelope_z = sp.stats.zscore(rip_band_envelope, axis=0)

    '''
    It is important to choose a good gaussian window width for ripple
    detection, because two things need to be considered: 1) cells in a ripple
    do not fire simultaneously, so the width cannot be too small or no
    overlap between the cell activity will be detected, and 2) cell bursting
    may be a factor to consider, and by using a large width the difference
    between a single spike and a small burst becomes small.
    What I think is important for using multiunit activity in ripple analysis
    is to incorporate the fact that ripples are special because they activate
    a wide distribution of neurons. High cell activity and bursting can
    happen outside of ripples, but the diversity in neuronal firing during the
    small window of a ripple is a somewhat signature trait, and should be
    taken advantage of for ripple detection. To this effect, it is important
    that for each neuron, gaussian curves do not sum when overlapping, as
    this would increase the importance of bursting activity in the signal.
    Instead, gaussians should be summed across different neurons so as to make
    the signal large when there are many different neurons coincidentally
    active. Unfortunately it is difficult for me to figure out how to convolve
    without summing overlapping parts, so I will do the next closest thing:
    cut off the peaks at an amplitude of 1. So the overlapping tails of the
    gaussians will sum, but the peaks will not.
    '''
    filtered_spikes = sp.ndimage.gaussian_filter1d(spikes, sigma=SIGMA,
                                                   axis=0, mode='constant')
    filtered_spikes[filtered_spikes > 1] = 1

    # Sum the gaussian traces from all neurons and zscore it.
    pop_spike_z = sp.stats.zscore(np.sum(filtered_spikes, axis=1))
    # Sum the rip band across electrodes and zscore it - already roi masked.
    pop_rip_band_z = sp.stats.zscore(np.sum(rip_band_envelope_z, axis=1))

    summay_stat_z = pop_spike_z + pop_rip_band_z

    # Find ripples
    peaks, peak_props = sp.signal.find_peaks(
        summay_stat_z, height=PEAK_THRESH, distance=PEAK_SEP,
        width=PEAK_WIDTH, prominence=PEAK_PROM)
    peak_props['peak_ind'] = peaks
    ripple_df = pd.DataFrame(peak_props)

    '''
    Apply velocity threshold
    Peaks that occur during outside of still periods are removed. These are
    mostly large amplitude noise events during behavior from the rat bumping
    into things.
    '''
    vel_mask = vel < VEL_THRESH
    ripple_df = ripple_df[vel_mask[ripple_df.peak_ind]]

    '''
    Now that the peaks have been found, it is time to determine the window
    edges for rippletimes. To do this, the times at which cumEnvG crosses
    below 0 will
    define the edges of the window for each peak. If multiple peaks occur
    within the same window, only one rippletime window will be recorded. The
    peak data should be saved separately.
    '''
    is_lt_zero = summay_stat_z < 0
    # find ripple window edges
    ripple_df['ripple_start_ind'] = [
        next(ind for ind, val in
             reversed(list(enumerate(is_lt_zero[:p_ind+1]))) if val)
        for p_ind in ripple_df.peak_ind]
    ripple_df['ripple_end_ind'] = [
        next(ind for ind, val in
             enumerate(is_lt_zero[p_ind:-1]) if val) + p_ind
        for p_ind in ripple_df.peak_ind]

    # Add timestamps to ripple_df
    ripple_df['ripple_peak_time'] = lfp_timestamps[
        ripple_df.peak_ind]
    ripple_df['ripple_start_time'] = lfp_timestamps[
        ripple_df.ripple_start_ind]
    ripple_df['ripple_end_time'] = lfp_timestamps[
        ripple_df.ripple_end_ind]
    ripple_df['ripple_duration'] = (
        ripple_df.ripple_end_time - ripple_df.ripple_start_time)

    # Remove duplicate rows for ripples with multiple peaks.
    ripple_df.drop(ripple_df.index.values[
        np.where(ripple_df.ripple_start_ind.duplicated())[0]])
    return ripple_df


def merge_ripples(ripple_df):

    ripple_df = ripple_df.sort_values('ripple_start_time')
    start_times = ripple_df.ripple_start_time.values
    end_times = ripple_df.ripple_end_time.values

    merge_list = []
    for ind in range(len(start_times[:-2])):
        if (start_times[ind+1] <= start_times[ind] <= end_times[ind+1] or
                start_times[ind+1] <= end_times[ind] <= end_times[ind+1] or
                start_times[ind] <= start_times[ind+1] <= end_times[ind] or
                start_times[ind] <= end_times[ind+1] <= end_times[ind]):
            merge_list.append(ind)

    if merge_list:
        ripple_df = _merge_ripples(ripple_df, merge_list)  # merge
        ripple_df = merge_ripples(ripple_df)  # repeat!
    return ripple_df


def _merge_ripples(ripple_df, merge_list):
    # peaks are just chose from first entry - so not valid over both ripples
    ripple_df_working = ripple_df.copy()
    new_row = dict()
    for ind in merge_list:
        new_row['ripple_start_time'] = min(ripple_df.ripple_start_time.iloc[ind],
                                           ripple_df.ripple_start_time.iloc[ind+1])
        new_row['ripple_end_time'] = max(ripple_df.ripple_end_time.iloc[ind],
                                         ripple_df.ripple_end_time.iloc[ind+1])
        new_row['ripple_start_ind'] = min(ripple_df.ripple_start_ind.iloc[ind],
                                          ripple_df.ripple_start_ind.iloc[ind+1])
        new_row['ripple_end_ind'] = max(ripple_df.ripple_end_ind.iloc[ind],
                                        ripple_df.ripple_end_ind.iloc[ind+1])
        new_row['ripple_duration'] = (new_row['ripple_end_time']
                                      - new_row['ripple_start_time'])
        new_row['peak_ind'] = ripple_df.peak_ind.iloc[ind]
        new_row['ripple_peak_time'] = ripple_df.ripple_peak_time.iloc[ind]
        if ripple_df.index.values[ind][1] == ripple_df.index.values[ind+1][1]:
            index = ripple_df.index.values[ind]
        else:
            index = (ripple_df.index.values[ind][0], 'union')
        ripple_df_working = pd.concat([ripple_df_working,
                                       pd.DataFrame(new_row, index=[index])],
                                      axis=0)
    merge_mask = np.array([True] * ripple_df_working.shape[0])
    merge_mask[merge_list] = False
    merge_mask[np.array(merge_list)+1] = False
    ripple_df_working = ripple_df_working.loc[merge_mask].sort_values(
        'ripple_start_time')
    return ripple_df_working
