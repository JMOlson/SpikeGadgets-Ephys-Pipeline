# Utilities for working with spiking data

import pynapple as nap
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# Own modules
from jlab_nwb import JLabNWBHandler
import analyze_one_nwb_settings.import_settings as analyze1_settings
import beh_core_settings.import_settings as beh_settings
import Utils.epoch_tools as ep_tools
import Beh.Tracking.tracking_preprocessor as tr_pre

# def create_MUA_binned(spikes, epoch, bin_size):
#     """
#     DON'T USE THIS FUNCTION. PYNAPPLE ALREADY HAS .COUNT() TO BIN ON SPIKES/TsGroup
    
#     Create a Multi-Unit Activity (MUA) signal from a spike train.
    
#     spikes: expected to by pynapple SpikeTrain object
    
#     bin size: bin size in seconds to sum spikes over
#     """
#     # round to nearest integet
#     n_Bins = np.round((epoch.end[0]-epoch.start[0]) / bin_size).astype(int)
    
#     spikes_binned = dict()
#     for i_unit in spikes:
#         spikes_binned[i_unit] = np.histogram(spikes[i_unit].times(),
#         bins=np.linspace(epoch.start[0], epoch.end[0], num=n_Bins))
        
#     spikes_binned = np.array([spikes_binned[i_unit][0] for i_unit in spikes_binned]).T
#     spikes_binned = np.sum(spikes_binned, axis=1)
    
#     epochs_binned_times = np.linspace(epoch.start[0], epoch.end[0], num=n_Bins)
    
#     return spikes_binned, epochs_binned_times

CHUNK_SIZE = 100000

def spikes_to_tsg(nwb_units_df, time_support=None):
    """
    Convert a dictionary of spike times to a pynapple TsGroup object
    """
    units_tsg = nap.TsGroup(
        {i_unit: nap.Ts(this_unit) for i_unit, this_unit
         in zip(nwb_units_df.index.values, nwb_units_df.spike_times)},
        time_support=time_support)
    units_tsg.set_info(nwb_units_df.iloc[:, :-1])
    return units_tsg

def bin_spikes(spikes_tsg, bin_centers, bin_size, chunk_size=CHUNK_SIZE):
    '''
    Bin spikes given centers.
    Uses pynapple's .count() method to bin spikes.
    Built using chunks to avoid memory issues.
    
    Parameters:
    - spikes_tsg (pynapple TsGroup):    units TsG with spike times
    - bin_centers (integer array):      array with bin centers
    - bin_size (integer):               size of bins
    - chunk_size (integer):             size of chunks to bin spikes
    
    Returns:
    - binned_spikes_all (pynapple TsdFrame):  binned spikes
    '''
    bin_intsets = nap.IntervalSet(start=bin_centers-bin_size/2,
                                  end=bin_centers+bin_size/2)
    n_chunks = int(np.ceil(len(bin_intsets)/chunk_size))
    for i_chunk in range(n_chunks):
        if i_chunk+1 > len(bin_intsets):
            chunk = bin_intsets[i_chunk*chunk_size:]
        else:
            chunk = bin_intsets[i_chunk*chunk_size:(i_chunk+1)*chunk_size]
        binned_spikes = spikes_tsg.count(ep=chunk)
        if i_chunk == 0:
            binned_spikes_all = binned_spikes
        else:
            binned_spikes_all = np.vstack((binned_spikes_all,binned_spikes))
    return binned_spikes_all