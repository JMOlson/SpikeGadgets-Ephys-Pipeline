#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import pynapple as nap


def compute_1d_tuning_curves(group, feature, nb_bins, ep=None, minmax=None,
                             return_occ=False):
    """
    Computes 1-dimensional tuning curves relative to a 1d feature.

    This is the pynapple fn, adding the ability to return NaN locations
    for the tuning curve.

    Parameters
    ----------
    group : TsGroup
        The group of Ts/Tsd for which the tuning curves will be computed
    feature : Tsd (or TsdFrame with 1 column only)
        The 1-dimensional target feature (e.g. head-direction)
    nb_bins : int
        Number of bins in the tuning curve
    ep : IntervalSet, optional
        The epoch on which tuning curves are computed.
        If None, the epoch is the time support of the feature.
    minmax : tuple or list, optional
        The min and max boundaries of the tuning curves.
        If None, the boundaries are inferred from the target feature

    Returns
    -------
    pandas.DataFrame
        DataFrame to hold the tuning curves

    Raises
    ------
    RuntimeError
        If group is not a TsGroup object.

    """
    assert isinstance(group, nap.TsGroup), "group should be a TsGroup."
    assert isinstance(
        feature, (nap.Tsd, nap.TsdFrame)
    ), "feature should be a Tsd (or TsdFrame with 1 column only)"
    if isinstance(feature, nap.TsdFrame):
        assert (
            feature.shape[1] == 1
        ), "feature should be a Tsd (or TsdFrame with 1 column only)"
    assert isinstance(nb_bins, int)

    if ep is None:
        ep = feature.time_support
    else:
        assert isinstance(ep, nap.IntervalSet), "ep should be an IntervalSet"

    if minmax is None:
        bins = np.linspace(np.min(feature), np.max(feature), nb_bins + 1)
    else:
        assert isinstance(
            minmax, tuple), "minmax should be a tuple of boundaries"
        bins = np.linspace(minmax[0], minmax[1], nb_bins + 1)

    idx = bins[0:-1] + np.diff(bins) / 2

    tuning_curves = pd.DataFrame(index=idx, columns=list(group.keys()))

    if isinstance(ep, nap.IntervalSet):
        group_value = group.value_from(feature, ep)
        occupancy, _ = np.histogram(feature.restrict(ep).values, bins)
    else:
        group_value = group.value_from(feature)
        occupancy, _ = np.histogram(feature.values, bins)

    for k in group_value:
        count, _ = np.histogram(group_value[k].values, bins)
        count = count / occupancy
        # count[np.isnan(count)] = 0.0
        tuning_curves[k] = count
        tuning_curves[k] = count * feature.rate

    return tuning_curves, occupancy


def create_bin_edges(bin_centers):
    # Compute bin edges
    bin_edges = np.zeros(len(bin_centers) + 1)
    bin_edges[1:-1] = (bin_centers[1:] + bin_centers[:-1]) / 2
    bin_edges[0] = 2 * bin_centers[0] - bin_edges[1]
    bin_edges[-1] = 2 * bin_centers[-1] - bin_edges[-2]

    return bin_edges
