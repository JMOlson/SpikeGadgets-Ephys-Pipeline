#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import Beh.beh_utilities as beh_utils
import numpy as np
import pandas as pd
import pynapple as nap
from StateScriptRecData import StateScriptRec as SSRec
import pickle

# import scipy as sp
# import matplotlib
# import matplotlib.pyplot as plt
# import yaml

# from jlab_nwb import JLabNWBHandler

# ### Access of base epochs from nwb file


def store_epoch_info(rec_nwb, ep_file, beh_settings,
                     rec_settings=None, sleep_tag=None):
    epochs_intSet, epochs_df = get_base_epochs(
        rec_nwb, beh_settings, settings_proj=rec_settings, sleep_tag=sleep_tag)
    with open(ep_file, 'wb') as f:
        pickle.dump((epochs_intSet, epochs_df), f)
    return epochs_intSet, epochs_df


def load_base_epochs(ep_file):
    with open(ep_file, 'rb') as f:
        epochs_intSet, epochs_df = pickle.load(f)
    return epochs_intSet, epochs_df


def get_base_epochs(rec_nwb, beh_settings=None,
                    settings_proj=None, sleep_tag=None):
    """
    Get epochs as an interval set and a dataframe from an nwb file.
    Makes sure to add sleep tag to epochs if available.
    """
    epochs_intSet = rec_nwb.get_epochs_for_pynapple()
    epochs_df = rec_nwb.get_epochs_as_dataframe()

    # Add sleep tag to epochs_df
    if sleep_tag is None:
        if settings_proj is not None and 'sleep_tag' in settings_proj:
            sleep_tag = settings_proj['sleep_tag']
    if sleep_tag is not None:
        is_sleep_epoch = [True if sleep_tag in epoch_info[0] else False
                          for epoch_info in epochs_df['tags']]
        epochs_df['is_sleep_epoch'] = is_sleep_epoch

    # Add
    epochs_meta_df = create_epoch_meta_df(rec_nwb, beh_settings,)
    epochs_df = epochs_df.join(epochs_meta_df)
    return epochs_intSet, epochs_df


def create_epoch_meta_df(day_rec, beh_settings=None,):
    '''
    Create a dataframe to store epoch metadata.
    '''
    exp_metadata = day_rec.get_exp_metadata()
    event_type_meta = beh_utils.get_ss_dio_types(exp_metadata)
    n_epochs = len(exp_metadata['epochs'])

    # Import StateScript data - Some variables affect behavior processing.
    if beh_settings is not None:
        statescript_logs = day_rec.get_statescript_logs()
        if len(statescript_logs) != n_epochs:
            raise ValueError(
                f"Number of statescript logs ({len(statescript_logs)})" +
                f"does not match number of epochs ({n_epochs})")
        ep_ss_log = [SSRec.from_string(log) for log in statescript_logs]
        if 'ss_var_list' in beh_settings:
            ep_ss_vars = [ep_ss_log[ep].get_variables(
                beh_settings['ss_var_list']) for ep in range(n_epochs)]
        else:
            ep_ss_vars = [None]*n_epochs
        if 'home_well_id' in beh_settings:
            ep_home_well_ind = [
                ep_ss_vars[ep][beh_settings['home_well_id']]
                if beh_settings['home_well_id'] in ep_ss_vars[ep] else None
                for ep in range(n_epochs)]
        else:
            ep_home_well_ind = [None]*n_epochs
    else:
        Warning.warn(
            'No behavior settings provided, so no beh settings imported.')
        statescript_logs = [None]*n_epochs
        ep_ss_vars = [None]*n_epochs
        ep_home_well_ind = [None]*n_epochs

    ep_camera_id = [None]*n_epochs
    ep_camera_meters_per_pixel = [None]*n_epochs
    ep_env_name = ['']*n_epochs
    ep_pump_douts = [None]*n_epochs
    ep_pump_ids = [None]*n_epochs
    ep_well_dins = [None]*n_epochs
    ep_well_ids = [None]*n_epochs
    for ep_ind, ep in enumerate(exp_metadata['epochs']):
        task_found_flag = False
        for task_ind, task in enumerate(exp_metadata['tasks']):
            if ep['task_name'] == task['task_name']:
                if ep['index'] in task['task_epochs']:
                    ep_camera_id[ep_ind] = task['camera_id']
                    ep_camera_meters_per_pixel[ep_ind] = (
                        exp_metadata['cameras'][ep_camera_id[ep_ind][0]]
                        ['meters_per_pixel']
                    )
                else:
                    raise ValueError(
                        f"Epoch {ep['id']} not found in task" +
                        "{task['task_name']}," +
                        "but epoch task name matches task name")
                task_found_flag = True
                break
        if task_found_flag is False:
            raise ValueError(
                f"Epoch {ep['id']} not found in any task")
        ep_env_name[ep_ind] = ep['env_name']
        for i_env in exp_metadata['environment']:
            if i_env['env_name'] == ep['env_name']:
                ep_env_events = i_env['event_list']
        if beh_settings is not None and ep_env_events is not None:
            events = pd.Series([' '.join([a, b]) for a, b
                                in zip(event_type_meta['use_type'],
                                       event_type_meta['use_type_id'])])
            is_in_event_list = events.isin(ep_env_events)
            is_well_key = (event_type_meta['use_type']
                           == beh_settings['reward_event_id'])
            short_list = event_type_meta[is_well_key & is_in_event_list]
            ep_pump_douts[ep_ind] = [' '.join([a, b]) for a, b in zip(
                short_list['io_type'], short_list['io_type_index'])]
            ep_pump_ids[ep_ind] = short_list['use_type_id'].values

            is_well_key = (event_type_meta['use_type']
                           == beh_settings['well_id'])
            short_list = event_type_meta[is_well_key & is_in_event_list]
            ep_well_dins[ep_ind] = [' '.join([a, b]) for a, b in zip(
                short_list['io_type'], short_list['io_type_index'])]
            ep_well_ids[ep_ind] = short_list['use_type_id'].values
        else:
            ep_pump_douts[ep_ind], ep_pump_ids[ep_ind] = None, None
            ep_well_dins[ep_ind], ep_well_ids[ep_ind] = None, None

    epoch_meta_dict = {
        'id': [ep['id'] for ep in exp_metadata['epochs']],
        'task_name': [ep['task_name'] for ep in exp_metadata['epochs']],
        'env_name': ep_env_name,
        'led_configuration': [ep['led_configuration']
                              for ep in exp_metadata['epochs']],
        'led_list': [ep['led_list'] for ep in exp_metadata['epochs']],
        'led_positions': [ep['led_positions']
                          for ep in exp_metadata['epochs']],
        'pump_dio_list': ep_pump_douts,
        'pump_id_list': ep_pump_ids,
        'well_dio_list': ep_well_dins,
        'well_id_list': ep_well_ids,
        'home_well_ind': ep_home_well_ind,
        'ss_log': statescript_logs,
        'ss_vars': ep_ss_vars,
        'camera_id': ep_camera_id,
        'camera_meters_per_pixel': ep_camera_meters_per_pixel,
    }
    epoch_meta_df = pd.DataFrame(epoch_meta_dict)

    return epoch_meta_df


# Epoch writing and manipulation

def split_into_timed_intervals(epochs, interval):
    '''
    Split an epoch into sub-epochs of a specified length.
    Return a pynapple interval set.
    '''
    # if epochs is a tuple or list of numbers, assume it is a start and stop
    if isinstance(epochs, tuple) or isinstance(epochs, list):
        if len(epochs) != 2:
            raise ValueError(
                'epochs must be a tuple or list of length 2, or a pynapple interval set')
        # drop the last interval, which will be shorter than the rest
        starts = epochs[0] + np.arange(0, epochs[1] - epochs[0], interval)[:-1]
        ends = starts + interval
        return nap.IntervalSet(start=starts, end=ends)
    elif isinstance(epochs, nap.IntervalSet):
        for i in range(len(epochs)):
            if i == 0:
                intSet_split = split_into_timed_intervals(
                    (epochs.start[i], epochs.end[i]), interval)
            else:
                intSet_split = intSet_split.union(split_into_timed_intervals(
                    (epochs.start[i], epochs.end[i]), interval))
        return intSet_split
    else:
        raise ValueError(
            'epochs must be a tuple or list of length 2, or a pynapple interval set')


def split_session_types(epochs_df, full_int_set, tags, invert_tags=False):
    '''
    Split epochs into sessions types based on flags.

    Tags is a list of strings that will be used to identify the session types.
    '''
    intSets = dict()

    for i_tag in tags:
        if invert_tags:
            default_value = True
            name = 'not_' + i_tag + '_sess'
        else:
            default_value = False
            name = i_tag + '_sess'
        is_tag_sess = pd.Series(
            [default_value] * epochs_df.shape[0], name=name)
        for i, i_epoch_info in enumerate(epochs_df['tags']):
            if i_tag in i_epoch_info[0]:
                is_tag_sess.iloc[i] = not default_value
        intSets[name] = full_int_set[is_tag_sess.to_list()]
        intSets[('not_'+name)] = full_int_set[(~is_tag_sess).to_list()]
        epochs_df[name] = is_tag_sess
    return intSets


def add_movement_epochs(intSets, movement_df, MOVEMENT_GAP_THRESH=0.5):
    diff_movement = movement_df['is_moving'].astype(int).diff()
    start_moving = diff_movement.index[diff_movement == 1]
    end_moving = diff_movement.index[diff_movement == -1]
    starts_still = end_moving[0] > start_moving[0]
    if starts_still:
        intSets['beh']['is_moving'] = nap.IntervalSet(
            start=start_moving,
            end=end_moving)
    else:
        intSets['beh']['is_moving'] = nap.IntervalSet(
            start=start_moving,
            end=end_moving[1:])
    intSets['beh']['is_moving'] = intSets['beh']['is_moving'].intersect(
        intSets['beh']['all_sess'])
    intSets['beh']['is_quiescent'] = intSets['beh']['all_sess'].set_diff(
        intSets['beh']['is_moving'])
    intSets['beh']['is_moving_merged'] = (
        intSets['beh']['is_moving'].merge_close_intervals(MOVEMENT_GAP_THRESH))
    intSets['beh']['is_quiescent_merged'] = (
        intSets['beh']['is_quiescent'].
        merge_close_intervals(MOVEMENT_GAP_THRESH))

    diff_movement = movement_df['is_moving_sleep'].astype(int).diff()
    start_moving = diff_movement.index[diff_movement == 1]
    end_moving = diff_movement.index[diff_movement == -1]
    starts_still = end_moving[0] > start_moving[0]
    if starts_still:
        intSets['beh']['is_moving_sleepThr'] = nap.IntervalSet(
            start=start_moving,
            end=end_moving)
    else:
        intSets['beh']['is_moving_sleepThr'] = nap.IntervalSet(
            start=start_moving,
            end=end_moving[1:])
    intSets['beh']['is_moving_sleepThr'] = (
        intSets['beh']['is_moving_sleepThr'].intersect(
            intSets['beh']['all_sess']))
    intSets['beh']['is_quiescent_sleepThr'] = (
        intSets['beh']['all_sess'].set_diff(
            intSets['beh']['is_moving_sleepThr'])
    )
    intSets['beh']['is_moving_sleepThr_merged'] = (
        intSets['beh']['is_moving_sleepThr'].
        merge_close_intervals(MOVEMENT_GAP_THRESH))
    intSets['beh']['is_quiescent_sleepThr_merged'] = (
        intSets['beh']['is_quiescent_sleepThr'].
        merge_close_intervals(MOVEMENT_GAP_THRESH))
    return None


def add_visits_epochs(intSets, visits_df):
    intSets['beh']['well_time'] = nap.IntervalSet(start=visits_df['start'],
                                                  end=visits_df['end'],)
    intSets['beh']['well_time_home'] = nap.IntervalSet(
        start=visits_df.loc[visits_df['is_home'], 'start'],
        end=visits_df.loc[visits_df['is_home'], 'end'],)
    intSets['beh']['well_time_away'] = nap.IntervalSet(
        start=visits_df.loc[~visits_df['is_home'], 'start'],
        end=visits_df.loc[~visits_df['is_home'], 'end'],)

    intSets['beh']['well_time_1'] = nap.IntervalSet(
        start=visits_df.loc[visits_df['id'] == 1, 'start'],
        end=visits_df.loc[visits_df['id'] == 1, 'end'],)
    intSets['beh']['well_time_2'] = nap.IntervalSet(
        start=visits_df.loc[visits_df['id'] == 2, 'start'],
        end=visits_df.loc[visits_df['id'] == 2, 'end'],)
    intSets['beh']['well_time_3'] = nap.IntervalSet(
        start=visits_df.loc[visits_df['id'] == 3, 'start'],
        end=visits_df.loc[visits_df['id'] == 3, 'end'],)
    intSets['beh']['well_time_4'] = nap.IntervalSet(
        start=visits_df.loc[visits_df['id'] == 4, 'start'],
        end=visits_df.loc[visits_df['id'] == 4, 'end'],)

    return None


def add_trajectory_epochs(intSets, traj_df):
    intSets['beh']['trajectories'] = nap.IntervalSet(
        start=traj_df['Start Time'],
        end=traj_df['End Time'],)
    is_outbound = traj_df['is_outbound']
    intSets['beh']['trajectories_outbound'] = nap.IntervalSet(
        start=traj_df.loc[is_outbound, 'Start Time'],
        end=traj_df.loc[is_outbound, 'End Time'],)
    intSets['beh']['trajectories_inbound'] = nap.IntervalSet(
        start=traj_df.loc[~is_outbound, 'Start Time'],
        end=traj_df.loc[~is_outbound, 'End Time'],)
    is_rewarded = traj_df['is_rewarded']
    intSets['beh']['trajectories_correct'] = nap.IntervalSet(
        start=traj_df.loc[is_rewarded, 'Start Time'],
        end=traj_df.loc[is_rewarded, 'End Time'],)
    intSets['beh']['trajectories_error'] = nap.IntervalSet(
        start=traj_df.loc[~is_rewarded, 'Start Time'],
        end=traj_df.loc[~is_rewarded, 'End Time'],)
    intSets['beh']['trajectories_outbound_correct'] = nap.IntervalSet(
        start=traj_df.loc[is_outbound & is_rewarded, 'Start Time'],
        end=traj_df.loc[is_outbound & is_rewarded, 'End Time'],)
    intSets['beh']['trajectories_outbound_error'] = nap.IntervalSet(
        start=traj_df.loc[is_outbound & ~is_rewarded, 'Start Time'],
        end=traj_df.loc[is_outbound & ~is_rewarded, 'End Time'],)
    intSets['beh']['trajectories_inbound_correct'] = nap.IntervalSet(
        start=traj_df.loc[~is_outbound & is_rewarded, 'Start Time'],
        end=traj_df.loc[~is_outbound & is_rewarded, 'End Time'],)
    intSets['beh']['trajectories_inbound_error'] = nap.IntervalSet(
        start=traj_df.loc[~is_outbound & ~is_rewarded, 'Start Time'],
        end=traj_df.loc[~is_outbound & ~is_rewarded, 'End Time'],)
    return None


def grab_traj_epochs(traj_df, start_well, end_well):
    '''
    Create an interval set containing each trajectory between start_well and
    end_well.
    '''
    trajs = ((traj_df['Start Well'] == start_well) &
             (traj_df['End Well'] == end_well))
    return nap.IntervalSet(
        start=traj_df.loc[trajs, 'Start Time'],
        end=traj_df.loc[trajs, 'End Time'],)


def trim_epochs(intSets, traj_df):
    '''
    Trim epochs to start at first statescript event
    and end at last statescript event.
    '''
    all_sess = intSets['all_sess']
    for i_sess in intSets:
        intSets[i_sess] = intSets[i_sess].intersect(all_sess)
    epochs_df['all_sess'] = all_sess
    return None
