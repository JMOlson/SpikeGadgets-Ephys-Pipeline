#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pynapple as nap


def import_spikes(day_rec, epochs_intSet=[]):
    """
    """
    # Import spikes Data as df and TsGroup
    nwb_units_df = day_rec.get_spikes()
    units_tsg = nap.TsGroup({i_unit: nap.Ts(this_unit)
                             for i_unit, this_unit in
                             zip(nwb_units_df.index.values,
                                 nwb_units_df.spike_times)},
                            time_support=epochs_intSet)
    units_tsg.set_info(nwb_units_df.iloc[:, :-1])
    return nwb_units_df, units_tsg


# Write your main code logic
# if __name__ == "__main__":
    # Call your functions or perform any operations
