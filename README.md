# Analysis Instructions 
Instructions for basic usage of this repo.

# Statescipt Warning - CHECK VALIDITY OF ALL SS LOG FILES
Statescript Behavior and Concern: Statescript records the currently loaded 
code when the rec starts, but then not does not log any code run after the
rec has started. It does change to using that code though so the code shown
may not match the code running.

If you always run your statescipt code before starting your rec, you should
not have issues. This is the recommended process.

# ############ INITIAL EXPORT OF DATA FROM SPIKEGADGETS ############
First, create a settings yml file for your experiment in you experiment data
folder, if you haven't already.

# To write initial NWB from preprocessed data:

**Run jlab_nwb.py**

Select the nwb file by setting the correct values in
import_one_rec_settings.py and analysis_paths.yml in
jlab-settings/anallyze_one_rec_settings

# Trajectory, Visit, and Reward Database Creation
**Script: build_basic_beh_dfs.py** 

Creates: _rewards, _traj, _visits in Beh folder
## _rewards (df)
- pump (int, Din id), start (float, sec), stop (float, sec), duration (float, sec), epoch_ind (int), and is_home (bool)
## _traj (df)
- traj_id (obj, start -> end), epoch_ind (int), start_well(obj), end_well (obj), start_time (float, sec), end_time (float, sec), is_rewarded (bool), is_outbound (bool), is_inbound (bool)
## _visits (df)
- id (int, well Din), start (float, sec), end (float, sec), epoch_ind (int), is_home (bool), duration (float, sec), ss_licks (obj, licks within lick criteria), ss_nonlicks (obj, licks too short or too long), is_rewarded (bool)


# Build movement and movement_smooth_dfs in 2D
**Script: build_movement_df.py**

Creates: _movement_df and movement_smooth_df in Beh folder
## _movement_df (df)
- index (float, sec), x (float, cm), y (float, cm), hd(float, radians), vel (float, cm/s), heading (float, ?), acc(float, ?), acc_heading (float, radians), angvel (float, radian), is_moving (bool), is_moving_sleep (bool)
## _movement_smooth_df (df)
Identical to _movement_df but calculated from smoothed tracking. More missing values as a result

# Create track graphs based on linearization and update previous data with linearization
**Fn: create_track_map.py**

Creates: _track_graph, track_graph_trajs in Beh folder

Modifies: _traj, _movement_df, movement_smooth_df in Beh folder

## _track_graph (obj, needed by edeno linearization)
- track_graph (graph, edges, spacing)

## _track_graph_trajs (df)
- traj_start (int, well ID start), traj_end (int, well ID end), route_ind (int, ?), track_graph (obj, graph), edges(obj, edge list, pairs of nodes)

## _traj (df)
- Adds: traj_template_ind (int, traj ID), traj_route_ind (int, ?), traj_rmse_cm (float, cm, root mean square error of 2D to assigned 1D traj)

## _movement_df (df)
- Adds: linear_position (float), track_segment_id (float), projected_x_position (float, ?), projected_y_position (float, ?), traj_linear_position (float, ?), traj_track_segment_id (float, ?), traj_projected_x_position (float, ?), traj_projected_y_position (float, ?) 
## _movement_smooth_df (df)
- same as _movement_df but on smoothed tracking data

## Usage Insructions create_track_map
### Trajectories
- Plots will pop up of each possibe trajectory between reward wells
- Left click to place nodes along the animal's trajectory 
    - Edges will be drawn between nodes and these edges will serve as the linearzation coordinates
    - X,Y coordinates will be placed on the edge they are closest to
- When done, simply close the plot window (do not File -> Save)

- A trajectory may be presented multiple times, for example, you may see "Trajectory ('1','5')" plotted twice in a row. This feature is present for the possibility that rats can take multiple trajectories between points of interest. If your rats only have one route, mark the nodes on the first plot and simply close the duplicate plots without making any nodes.
- Similarly, you may see plots of trajectories between wells that your rat never ran. Also close these plots without making any nodes.

### Full track map
- After all reward well trajecotories are made, a final plot of all tracking data will be shown
- As with trajectories, mark nodes on the maze how you wish to linearize the entire maze
- When done, close the plot window

<!-- # Epoch creation
 epoch_creation.ipynb # in utils folder -->

### These 7 files in Beh folder serve as the basis for all further analyses.

### Naming conventions
## Time

Time should always be in seconds

The beginning of an epoch of time should always be called "start_time" and the ending of an epoch should always be called "stop_time" 

Convention from pynapple




